/* eslint-disable import/no-extraneous-dependencies */
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
/* eslint-enable import/no-extraneous-dependencies */
const getConfig = require('./webpack.common.js');
const Config = require('./src/app/Config.json');

const devExtractTextPluginInstance = new ExtractTextPlugin('static/bundle.css');
const devHtmlWebpackPluginInstance = new HtmlWebpackPlugin({
  url: Config.url,
  title: Config.title,
  keywords: Config.keywords,
  description: Config.description,
  logoUrl: Config.logoUrl,
  coverUrl: Config.coverUrl,
  copyright: Config.copyright,
  author: Config.author,
  twitterAccount: Config.twitterAccount,
  googleAnalyticsID: '',
  facebookPixelID: '',
  template: './node_modules/reactjs-beepeers-framework/index.ejs',
  filename: './index.html',
  showErrors: true,
});
const devCopyWebpackPluginInstance = new CopyWebpackPlugin(
  [
    { from: './static', to: './static' },
  ],
  {
    copyUnmodified: false,
    debug: 'debug',
  }
);
const devFaviconsWebpackPluginInstance = new FaviconsWebpackPlugin({
  logo: Config.faviconImg,
  title: Config.title,
  prefix: 'static/icons-[hash]/',
});

module.exports = getConfig(
  devExtractTextPluginInstance,
  [
    devFaviconsWebpackPluginInstance,
    devHtmlWebpackPluginInstance,
    devCopyWebpackPluginInstance,
  ]
);
