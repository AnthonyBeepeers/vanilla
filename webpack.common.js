const path = require('path');
const fs = require('fs');
/* eslint-disable import/no-extraneous-dependencies */
const combineLoaders = require('webpack-combine-loaders/combineLoaders');
/* eslint-enable import/no-extraneous-dependencies */

const includePaths = [
  fs.realpathSync(`${__dirname}/src`),
  fs.realpathSync(`${__dirname}/node_modules/reactjs-beepeers-framework/`),
];

const resolvePaths = [
  fs.realpathSync(`${__dirname}/node_modules/`),
];

module.exports = (extractTextPluginInstance, otherPluginInstances) => (
  {
    devServer: {
      inline: true,
      historyApiFallback: true,
      port: 9002,
    },
    entry: './src/index.jsx',
    output: {
      path: path.join(__dirname, 'dist'),
      publicPath: '/',
      filename: 'bundle.js',
    },
    resolve: {
      extensions: ['', '.js', '.jsx'],
      root: resolvePaths,
      fallback: resolvePaths,
    },
    resolveLoader: {
      root: resolvePaths,
      fallback: resolvePaths,
    },
    eslint: {
      configFile: './.eslintrc',
    },
    module: {
      loaders: [
        {
          test: /\.js(x)?$/,
          loader: combineLoaders([
            {
              loader: 'babel-loader',
              query: {
                babelrc: false,
                presets: [
                  'es2015',
                  'react',
                ].map(dep => require.resolve(`babel-preset-${dep}`)),
                plugins: [
                  'transform-object-rest-spread',
                ].map(dep => require.resolve(`babel-plugin-${dep}`)),
              },
            },
            {
              loader: 'eslint-loader',
            },
          ]),
          include: includePaths,
          exclude: /node_modules/,
        },
        {
          test: /\.css$/,
          exclude: /node_modules/,
          include: includePaths,
          loader: extractTextPluginInstance.extract(
            'style-loader',
            combineLoaders([
              {
                loader: 'css-loader',
                query: {
                  modules: true,
                  localIdentName: '[name]__[local]___[hash:base64:5]',
                },
              },
            ]),
            'postcss-loader'
          ),
        },
        {
          test: /\.css$/,
          exclude: path.resolve(__dirname, 'src'),
          include: path.resolve(__dirname, 'node_modules'),
          loader: extractTextPluginInstance.extract(
            'style-loader',
            combineLoaders([
              {
                loader: 'css-loader',
                query: {
                  sourceMap: true,
                },
              },
            ]),
            'postcss-loader'
          ),
        },
        {
          test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
          loader: combineLoaders([
            {
              loader: 'url-loader',
              query: {
                limit: 10000,
                mimetype: 'application/font-woff',
                name: 'static/[hash].[ext]',
              },
            },
          ]),
        },
        {
          test: /\.(ttf|eot|svg)((\?v=[0-9]\.[0-9]\.[0-9])|(\?#iefix))?$/,
          loader: 'file-loader',
          query: {
            name: 'static/[hash].[ext]',
          },
        },
        {
          test: /\.(jpe?g|png|gif|ico)$/,
          loader: 'file-loader',
          query: {
            name: 'static/[hash].[ext]',
          },
        },
        {
          test: /\.(mp4)$/,
          loader: 'file-loader',
          query: {
            name: 'static/[hash].[ext]',
          },
        },
        {
          test: /\.json$/,
          loader: 'json',
        },
      ],
    },
    plugins: [
      extractTextPluginInstance,
      ...otherPluginInstances,
    ],
  }
);
