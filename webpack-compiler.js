/* eslint-disable import/no-extraneous-dependencies */
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const fse = require('fs-extra');
/* eslint-enable import/no-extraneous-dependencies */
const getConfig = require('./webpack.common.js');
const Config = require('./src/app/Config.json');

const prodExtractTextPluginInstance = new ExtractTextPlugin('static/bundle.[hash].css');
const prodHtmlWebpackPluginInstance = new HtmlWebpackPlugin({
  url: Config.url,
  title: Config.title,
  keywords: Config.keywords,
  description: Config.description,
  logoUrl: Config.logoUrl,
  coverUrl: Config.coverUrl,
  copyright: Config.copyright,
  author: Config.author,
  twitterAccount: Config.twitterAccount,
  googleAnalyticsID: Config.googleAnalyticsID,
  facebookPixelID: Config.facebookPixelID,
  template: './node_modules/reactjs-beepeers-framework/index.ejs',
  filename: './index.html',
  showErrors: false,
});
const prodCopyWebpackPluginInstance = new CopyWebpackPlugin(
  [
    { from: './static', to: './static' },
  ],
  {
    copyUnmodified: true,
    debug: 'warning',
  }
);
const prodFaviconsWebpackPluginInstance = new FaviconsWebpackPlugin({
  logo: Config.faviconImg,
  title: Config.title,
  prefix: 'static/icons-[hash]/',
});

const webpackConfig = getConfig(
  prodExtractTextPluginInstance,
  [
    prodFaviconsWebpackPluginInstance,
    prodHtmlWebpackPluginInstance,
    prodCopyWebpackPluginInstance,
  ]
);

const buildPath = webpackConfig.output.path;

if (buildPath !== __dirname) {
  fse.removeSync(buildPath);
}

webpackConfig.output.filename = 'bundle.[hash].js';

webpackConfig.plugins.push(
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify('production'),
    },
  }),
  new webpack.optimize.OccurrenceOrderPlugin(),
  new webpack.optimize.DedupePlugin(),
  new webpack.optimize.UglifyJsPlugin({
    compress: {
      unused: true,
      dead_code: true,
      warnings: false,
    },
  })
);

const compiler = webpack(webpackConfig);

compiler.run((err, stats) => {
  if (err) {
    console.error('Webpack compiler encountered a fatal error.', err);
    return;
  }

  const jsonStats = stats.toJson();
  console.info('Webpack compile completed.');
  console.log(stats.toString({
    chunks: false,
    colors: true,
  }));

  if (jsonStats.errors.length > 0) {
    console.error('Webpack compiler encountered errors.');
    console.error(jsonStats.errors.join('\n'));
    return;
  } else if (jsonStats.warnings.length > 0) {
    console.warn('Webpack compiler encountered warnings.');
    console.warn(jsonStats.warnings.join('\n'));
  } else {
    console.info('No errors or warnings encountered.');

    console.info(`Webpack Compiler : Compilation duration -> ${stats.endTime - stats.startTime} ms.`);
  }
  return;
});
