export const WHITE = '#FFFFFF';
export const DARK = '#000000';

export const PURPLE = 'var(--primary-color)';
export const ORANGE = 'var(--segundary-color)';

export const LPE_NORMAL_FIELD_STYLE = {
  underlineFocusStyle: {
    borderColor: PURPLE,
  },

  floatingLabelStyle: {
    color: PURPLE,
  },

  floatingLabelFocusStyle: {
    color: PURPLE,
  },
};


export const BLACK = 'var(--primary-color)';

export const RED = 'var(--segundary-color)';

export const CABARET_NORMAL_FIELD_STYLE = {
  underlineFocusStyle: {
    borderColor: RED,
  },

  floatingLabelStyle: {
    color: RED,
  },

  floatingLabelFocusStyle: {
    color: RED,
  },
};
