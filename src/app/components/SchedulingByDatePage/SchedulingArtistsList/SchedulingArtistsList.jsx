import React from 'react/lib/React';

import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
import Grid from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Grid/Grid';

import SchedulingCarousel from '../SchedulingCarousel/SchedulingCarousel';
import SchedulingLinks from '../SchedulingLinks/SchedulingLinks';
import SchedulingArtists from '../SchedulingArtists/SchedulingArtists';

import styles from './SchedulingArtistsList.css';

const SchedulingArtistsList = () => (
  <div className={styles.wrapper} >
    <FullRow className={styles.content}>
      <SchedulingCarousel />
      <SchedulingLinks />
      <Grid className={styles.grid}>
        <SchedulingArtists />
      </Grid>
    </FullRow>
  </div>
);

export default SchedulingArtistsList;
