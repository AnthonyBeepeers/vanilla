import React, { PropTypes } from 'react/lib/React';

import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
import Grid from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Grid/Grid';
import SchedulingArtistsByDate from '../SchedulingArtistsByDate/SchedulingArtistsByDate';
import SchedulingCarousel from '../SchedulingCarousel/SchedulingCarousel';
import SchedulingLinks from '../SchedulingLinks/SchedulingLinks';

import styles from './SchedulingByDate.css';

const SchedulingByDate = ({ match }) => (
  <div className={styles.wrapper} >
    <FullRow className={styles.content}>
      <SchedulingCarousel />
      <SchedulingLinks />
      <Grid className={styles.grid}>
        <SchedulingArtistsByDate match={match} />
      </Grid>
    </FullRow>
  </div>
);

SchedulingByDate.propTypes = {
  match: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default SchedulingByDate;
