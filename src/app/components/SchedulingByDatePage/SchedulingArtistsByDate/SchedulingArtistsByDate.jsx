import React, { Component, PropTypes } from 'react/lib/React';
import { connect } from 'react-redux';

import $ from 'jquery/dist/jquery.min';

import moment from 'moment';

import { Tabs, Tab } from 'material-ui/Tabs';

import { FormattedMessage, injectIntl } from 'react-intl';

import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
import ResponsiveGallery from 'reactjs-beepeers-framework/components/Layout/Web/ResponsiveGallery/ResponsiveGallery';
import EventsFetcher from 'reactjs-beepeers-framework/components/Events/Fetchers/EventsFetcher/EventsFetcher';
import FullLineEventsListMoreButton from 'reactjs-beepeers-framework/components/Events/Renderers/Web/FullLineEventsListMoreButton/FullLineEventsListMoreButton';

import { DATE_FORMAT } from 'reactjs-beepeers-framework/constants/beepeersConfig';
import FullLineTitle from 'reactjs-beepeers-framework/components/General/Web/FullLineTitle/FullLineTitle';

import { EVENT_ACTOR_TYPE } from '../../../constants/actors';

import { WHITE, PURPLE, ORANGE } from '../../../constants/colors';

import morePictureUrl from '../../../assets/images/more_btn.png';

import styles from './SchedulingArtistsByDate.css';
import LinkedEvent from '../../General/LinkedEvent/LinkedEvent';

class SchedulingArtistsByDate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      width: 0,
    };
  }

  componentWillMount() {
    this.updateDimensionsHandler = null;
    this.updateDimensions();
  }

  componentDidMount() {
    this.updateDimensionsHandler = () => this.updateDimensions();
    $(window).resize(this.updateDimensionsHandler);
  }

  componentWillUnmount() {
    if (typeof (this.updateDimensionsHandler) !== 'undefined' && this.updateDimensionsHandler !== null) {
      $(window).off('resize', this.updateDimensionsHandler);
    }
  }

  updateDimensions() {
    this.setState({
      width: $(window).width(),
    });
  }

  render() {
    const { match, appConfig, locales, history, intl } = this.props;

    const { lang } = locales;
    const langWithoutRegionCode = lang.toLowerCase().split(/[_-]+/)[0];

    const { formatMessage } = intl;

    const allTitleString = formatMessage({ id: 'SCHEDULING.ALL_ARTIST' });

    const goToAll = () => {
      history.push('/scheduling');
    };

    const goToDate = (date) => {
      const dateURI = encodeURIComponent(date.format(DATE_FORMAT).replace(/\./g, '&#46;'));
      history.push(`/scheduling/bydate/${dateURI}`);
    };

    const { params = {} } = match;

    const currentDate = moment(decodeURIComponent(params.date).replace(/&#46;/g, '.')).utcOffset(0).format(DATE_FORMAT);

    const renderEventFunc = item => (
      <LinkedEvent event={item} />
    );

    const renderEventsItems = items => (
      <ResponsiveGallery
        list={items}
        renderItemFunc={renderEventFunc}
        nbPerRows={{ xs: 1, sm: 2, md: 3 }}
        strict
      />
    );

    const renderFunc = (eventsSections, isFetching) => {
      if (!isFetching && eventsSections.length > 0) {
        const eventsNodes = eventsSections.map((eventsSection, i) => (
          <div key={i} className={styles.sectionWrapper}>
            <FullLineTitle title={eventsSection.name} intlActivated={false} className={styles.sectionTitle} />
            {renderEventsItems(eventsSection.items)}
          </div>
        ));

        return (
          <FullRow>
            {eventsNodes}
          </FullRow>
        );
      }

      if (!isFetching) {
        return (
          <FullRow className={styles.emptyWrapper}>
            <FormattedMessage id={'SCHEDULING.NO_EVENT'} />
          </FullRow>
        );
      }

      return null;
    };

    const tabsNodes = [];

    const search = {
      profileTypeKey: EVENT_ACTOR_TYPE,
      day: currentDate,
      displaySections: true,
      nbResult: 50,
      sectionMode: 'LOCATION',
    };

    tabsNodes.push((
      <Tab
        key="All"
        style={{ color: WHITE }}
        label={allTitleString}
        className={styles.tabBtn}
        onActive={() => goToAll()}
        value="All"
      >
        <span>&nbsp;</span>
      </Tab>
  ));

    let validDateSelected = false;

    let dateFormat = 'DD MMM';
    if (langWithoutRegionCode === 'en') {
      dateFormat = 'MMM DD';
    }

    if (this.state.width > 768) {
      dateFormat = 'ddd DD MMM';
      if (langWithoutRegionCode === 'en') {
        dateFormat = 'ddd, MMM DD';
      }
    }

    appConfig.schedulingDates.forEach((date) => {
      const dateMoment = moment(date).utcOffset(0);
      const formattedDateMoment = dateMoment.format(DATE_FORMAT);

      if (formattedDateMoment === currentDate) {
        validDateSelected = true;
        tabsNodes.push((
          <Tab
            key={date}
            style={{ color: 'blue' }}
            label={dateMoment.format(dateFormat)}
            className={styles.tabBtn}
            onActive={() => goToDate(dateMoment)}
            value={formattedDateMoment}
          >
            <EventsFetcher
              renderFunc={renderFunc}
              search={search}
            />
            <FullLineEventsListMoreButton
              search={search}
              loadingColor={ORANGE}
              pictureUrl={morePictureUrl}
              className={styles.moreBtnWrapper}
            />
          </Tab>
      ));
      } else {
        tabsNodes.push((
          <Tab
            key={date}
            style={{ color: WHITE }}
            label={dateMoment.format(dateFormat)}
            className={styles.tabBtn}
            onActive={() => goToDate(dateMoment)}
            value={formattedDateMoment}
          >
            <span>&nbsp;</span>
          </Tab>
      ));
      }
    });

    if (!validDateSelected) {
      goToAll();
    }

    return (
      <Tabs
        inkBarStyle={{ backgroundColor: ORANGE, height: '6px', marginTop: '-6px' }}
        tabItemContainerStyle={{ backgroundColor: PURPLE }}
        value={currentDate}
      >
        {tabsNodes}
      </Tabs>
    );
  }
}

SchedulingArtistsByDate.propTypes = {
  appConfig: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.string,
    PropTypes.array,
    PropTypes.object,
    PropTypes.number,
  ])).isRequired,
  history: PropTypes.objectOf(PropTypes.any).isRequired,
  match: PropTypes.objectOf(PropTypes.any).isRequired,
  locales: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ])).isRequired,
  intl: PropTypes.objectOf(PropTypes.any).isRequired,
};

const SchedulingArtistsByDateIntl = injectIntl(SchedulingArtistsByDate);

const mapStateToProps = state => ({
  appConfig: state.appConfig,
  history: state.history,
  locales: state.locales,
});

const SchedulingArtistsByDateIntlConnected = connect(
  mapStateToProps
)(SchedulingArtistsByDateIntl);

export default SchedulingArtistsByDateIntlConnected;
