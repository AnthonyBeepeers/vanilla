import React from 'react/lib/React';

import ActorCarouselByKey from 'reactjs-beepeers-framework/components/Actors/Renderers/Web/ActorCarouselByKey/ActorCarouselByKey';

import { SCHEDULING_HEADER_KEY } from '../../../constants/actors';

const SchedulingCarousel = () => (
  <ActorCarouselByKey actorKey={SCHEDULING_HEADER_KEY} />
);

export default SchedulingCarousel;
