import React, { PropTypes } from 'react/lib/React';
import { injectIntl } from 'react-intl';
import Col from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Col/Col';
import Row from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Row/Row';

import styles from './SchedulingLinks.css';

const pdfLinks = [
  { url: 'http://www.panda-events.com/telechargement/pe_2018/PlanPE2018.pdf', key: 'SCHEDULING.FRIDAY' },
  { url: 'http://www.panda-events.com/telechargement/pe_2018/PlanPE2018.pdf', key: 'SCHEDULING.SATURDAY' },
  { url: 'http://www.panda-events.com/telechargement/pe_2018/PlanPE2018.pdf', key: 'SCHEDULING.SUNDAY' },
];

const SchedulingLinks = ({ intl }) => {
  const { formatMessage } = intl;

  /*  titleContent = formatMessage({
      id: title,
      defaultMessage: title,
    });*/
  const tmp = (
    <Row center="xs" className={styles.linksWrapper}>
      <Col xs={12} sm={8} md={6} lg={5}>
        <Row>
          {pdfLinks.map((link) => {
            const linkTitle = formatMessage({
              id: link.key,
              defaultMessage: link.key,
            });
            return (
              <Col className={styles.links} xs={4} >
                <a key={link.key} href={link.url} target="_blank" rel="noopener noreferrer">
                  <div>
                    <i className="fa fa-arrow-circle-o-down" />
                    {linkTitle}
                  </div>
                </a>
              </Col>
            );
          })}
        </Row>
      </Col>
    </Row>
  );
  console.log(tmp);
  return (
    <div />
  );
};

SchedulingLinks.propTypes = {
  /* eslint-disable react/no-unused-prop-types */
  intl: PropTypes.shape({
    formatMessage: PropTypes.func,
  }).isRequired,
  /* eslint-enable react/no-unused-prop-types */
};

const SchedulingLinksIntl = injectIntl(SchedulingLinks);

export default SchedulingLinksIntl;
