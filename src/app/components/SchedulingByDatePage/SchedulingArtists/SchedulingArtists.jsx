import React, { Component, PropTypes } from 'react/lib/React';
import { connect } from 'react-redux';

import $ from 'jquery/dist/jquery.min';

import moment from 'moment';

import { Tabs, Tab } from 'material-ui/Tabs';

import { FormattedMessage, injectIntl } from 'react-intl';

import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
import ResponsiveGallery from 'reactjs-beepeers-framework/components/Layout/Web/ResponsiveGallery/ResponsiveGallery';
import ActorsListFetcher from 'reactjs-beepeers-framework/components/Actors/Fetchers/ActorsListFetcher/ActorsListFetcher';
import FullLineTypedActorsListByKeyMoreButton from 'reactjs-beepeers-framework/components/Actors/Renderers/Web/FullLineTypedActorsListByKeyMoreButton/FullLineTypedActorsListByKeyMoreButton';

import { DATE_FORMAT } from 'reactjs-beepeers-framework/constants/beepeersConfig';

import morePictureUrl from '../../../assets/images/more_btn.png';
import ArtistDefaultPicture from '../../../assets/images/artist_default.png';

import LinkedArtist from '../../General/LinkedArtist/LinkedArtist';

import { ARTISTS_LIST_KEY, EVENT_ACTOR_TYPE } from '../../../constants/actors';

import { WHITE, PURPLE, ORANGE } from '../../../constants/colors';
import styles from './SchedulingArtists.css';

class SchedulingArtists extends Component {
  constructor(props) {
    super(props);
    this.state = {
      width: 0,
    };
  }

  componentWillMount() {
    this.updateDimensionsHandler = null;
    this.updateDimensions();
  }

  componentDidMount() {
    this.updateDimensionsHandler = () => this.updateDimensions();
    $(window).resize(this.updateDimensionsHandler);
  }

  componentWillUnmount() {
    if (typeof (this.updateDimensionsHandler) !== 'undefined' && this.updateDimensionsHandler !== null) {
      $(window).off('resize', this.updateDimensionsHandler);
    }
  }

  updateDimensions() {
    this.setState({
      width: $(window).width(),
    });
  }

  render() {
    const { appConfig, history, locales, intl } = this.props;

    const { lang } = locales;
    const langWithoutRegionCode = lang.toLowerCase().split(/[_-]+/)[0];

    const { formatMessage } = intl;

    const allTitleString = formatMessage({ id: 'SCHEDULING.ALL_ARTIST' });

    const goToDate = (date) => {
      const dateURI = encodeURIComponent(date.format(DATE_FORMAT).replace(/\./g, '&#46;'));
      history.push(`/scheduling/bydate/${dateURI}`);
    };

    const renderFunc = (events, isFetching) => {
      const renderEventFunc = (event) => {
        if (typeof (event.thumbnailUrl) !== 'undefined') {
          return (
            <LinkedArtist
              pictureUrl={`${event.originalUrl}&width=250&height=250`}
              artist={event}
            />
          );
        }

        if (typeof (event.coverUrl) !== 'undefined') {
          return (
            <LinkedArtist
              pictureUrl={`${event.coverUrl}&width=250&height=250`}
              artist={event}
            />
          );
        }

        return (
          <LinkedArtist
            pictureUrl={ArtistDefaultPicture}
            artist={event}
          />
        );
      };

      let eventsNodes = null;
      if (events.length > 0) {
        eventsNodes = (
          <ResponsiveGallery
            list={events}
            renderItemFunc={renderEventFunc}
            nbPerRows={{ xs: 3, sm: 6 }}
            strict
          />
        );
      }

      if (eventsNodes !== null) {
        return eventsNodes;
      }

      if (!isFetching) {
        return (
          <FullRow className={styles.emptyWrapper}>
            <FormattedMessage id={'SCHEDULING.NO_ARTIST'} />
          </FullRow>
        );
      }

      return null;
    };

    const tabsNodes = [];
    const param = {
      nbResult: 50,
      displaySections: false,
      headerProfileListKey: ARTISTS_LIST_KEY,
    };
    tabsNodes.push((
      <Tab
        key="All"
        style={{ color: WHITE }}
        label={allTitleString}
        className={styles.tabBtn}
      >
        <ActorsListFetcher
          actorTypeKey={EVENT_ACTOR_TYPE}
          actorsListKey={ARTISTS_LIST_KEY}
          flattenSections
          renderFunc={renderFunc}
        />
        <FullLineTypedActorsListByKeyMoreButton
          actorTypeKey={EVENT_ACTOR_TYPE}
          params={param}
          loadingColor={ORANGE}
          pictureUrl={morePictureUrl}
          className={styles.moreBtnWrapper}
        />
      </Tab>
    ));

    let dateFormat = 'DD MMM';
    if (langWithoutRegionCode === 'en') {
      dateFormat = 'MMM DD';
    }

    if (this.state.width > 768) {
      dateFormat = 'ddd DD MMM';
      if (langWithoutRegionCode === 'en') {
        dateFormat = 'ddd, MMM DD';
      }
    }

    appConfig.schedulingDates.forEach((date) => {
      const dateMoment = moment(date).utcOffset(0);

      tabsNodes.push((
        <Tab
          key={date}
          style={{ color: WHITE }}
          label={dateMoment.format(dateFormat)}
          className={styles.tabBtn}
          onActive={() => goToDate(dateMoment)}
        >
          <span>&nbsp;</span>
        </Tab>
      ));
    });
    return (
      <Tabs
        inkBarStyle={{ backgroundColor: ORANGE, height: '6px', marginTop: '-6px' }}
        tabItemContainerStyle={{ backgroundColor: PURPLE }}
      >
        {tabsNodes}
      </Tabs>
    );
  }
}

SchedulingArtists.propTypes = {
  appConfig: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.string,
    PropTypes.array,
    PropTypes.object,
    PropTypes.number,
  ])).isRequired,
  history: PropTypes.objectOf(PropTypes.any).isRequired,
  locales: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ])).isRequired,
  intl: PropTypes.objectOf(PropTypes.any).isRequired,
};

const SchedulingArtistsIntl = injectIntl(SchedulingArtists);

const mapStateToProps = state => ({
  appConfig: state.appConfig,
  history: state.history,
  locales: state.locales,
});

const SchedulingArtistsIntlConnected = connect(
  mapStateToProps
)(SchedulingArtistsIntl);

export default SchedulingArtistsIntlConnected;
