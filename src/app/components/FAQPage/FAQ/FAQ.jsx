import React from 'react/lib/React';

import Grid from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Grid/Grid';
import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
import Row from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Row/Row';

import FAQContent from 'reactjs-beepeers-framework/components/Actors/Renderers/Web/FAQ/FAQ';

import { FAQ_LIST_KEY } from '../../../constants/actors';

import styles from './FAQ.css';
import HoloColoredTitle from '../../General/HoloColoredTitle/HoloColoredTitle';

const FAQ = () => (
  <FullRow>
    <Grid className={styles.wrapper}>
      <Row>
        <HoloColoredTitle
          title="FAQ.FAQ_TITLE"
          enableGrid={false}
          className={styles.wrapper}
        >
          <FAQContent
            actorsListKey={FAQ_LIST_KEY}
            sidenavClassName={styles.sidenav}
            sectionClassName={styles.section}
            faqItemClassName={styles.faqItem}
          />
        </HoloColoredTitle>
      </Row>
    </Grid>
  </FullRow>
);

export default FAQ;
