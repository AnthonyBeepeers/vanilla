import React, { PropTypes } from 'react/lib/React';
import { connect } from 'react-redux';

import Grid from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Grid/Grid';
import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
import FullLineTitle from 'reactjs-beepeers-framework/components/General/Web/FullLineTitle/FullLineTitle';
import AccountForgotPwd from 'reactjs-beepeers-framework/components/Account/Renderers/Web/AccountForgotPwd/AccountForgotPwd';

import { PURPLE, LPE_NORMAL_FIELD_STYLE } from '../../../constants/colors';

import styles from './AccountManageForgotPwd.css';

const AccountManageForgotPwd = ({ history }) => {
  const afterAskPwdFunc = () => {
    history.push('/myaccount/sign');
  };

  return (
    <FullRow className={styles.mainContent}>
      <Grid className={styles.contentWrapper}>
        <FullLineTitle title="ACCOUNT.SIGN.ASK_NEW_PWD.TITLE" className={styles.title} />
        <FullRow>
          <AccountForgotPwd
            formColors={{ background: PURPLE, label: 'white' }}
            afterAskPwdFunc={afterAskPwdFunc}
            formNormalFieldStyle={LPE_NORMAL_FIELD_STYLE}
          />
        </FullRow>
      </Grid>
    </FullRow>
  );
};

AccountManageForgotPwd.propTypes = {
  history: PropTypes.objectOf(PropTypes.any).isRequired,
};

const mapStateToProps = state => ({
  history: state.history,
});

const AccountManageForgotPwdConnected = connect(
  mapStateToProps
)(AccountManageForgotPwd);

export default AccountManageForgotPwdConnected;
