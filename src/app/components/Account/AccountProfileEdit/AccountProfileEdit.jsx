import React, { PropTypes } from 'react/lib/React';
import { connect } from 'react-redux';

import Grid from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Grid/Grid';
import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
import AccountEdition from 'reactjs-beepeers-framework/components/Account/Renderers/Web/AccountEdition/AccountEdition';
import NotPendingAccount from 'reactjs-beepeers-framework/components/Account/Renderers/Web/NotPendingAccount/NotPendingAccount';
import OwnedActorsFetcher from 'reactjs-beepeers-framework/components/Actors/Fetchers/OwnedActorsFetcher/OwnedActorsFetcher';

import { PURPLE, LPE_NORMAL_FIELD_STYLE } from '../../../constants/colors';
import { CONTRIBUTOR_KEY } from '../../../constants/actors';

import AccountHeader from '../AccountHeader/AccountHeader';
import AccountMenu from '../AccountMenu/AccountMenu';

import styles from './AccountProfileEdit.css';

const AccountProfileEdit = ({ history, dispatch }) => {
  const renderAccount = (accountProfile) => {
    if (accountProfile !== null) {
      const header = () => {
        const renderFunc = (ownedExhibitors) => {
          /* eslint-disable new-cap */
          let profileMenu = AccountMenu(dispatch);
          /* eslint-enable new-cap */

          if (ownedExhibitors.length > 0) {
            const firstExhibitor = ownedExhibitors[0];
            /* eslint-disable new-cap */
            profileMenu = AccountMenu(dispatch, firstExhibitor.profileId);
            /* eslint-enable new-cap */
          }

          return (<AccountHeader accountProfile={accountProfile} ownedExhibitors={ownedExhibitors} className={styles.header} profileMenu={profileMenu} />);
        };

        return (<OwnedActorsFetcher
          actorId={accountProfile.profileId}
          profileTypeKey={CONTRIBUTOR_KEY}
          nbResult={10}
          flattenSections
          renderFunc={renderFunc}
          forceFetch
        />);
      };

      const afterAccountEditionFunc = () => {
        history.push('/myaccount/manage');
      };

      return (
        <FullRow>
          { header() }
          <AccountEdition
            formColors={{ background: PURPLE, label: 'white' }}
            afterAccountEditionFunc={afterAccountEditionFunc}
            formNormalFieldStyle={LPE_NORMAL_FIELD_STYLE}
          />
        </FullRow>
      );
    }

    return null;
  };

  return (
    <FullRow className={styles.mainContent}>
      <Grid className={styles.contentWrapper}>
        <NotPendingAccount
          renderAccount={renderAccount}
          className={styles.message}
        />
      </Grid>
    </FullRow>
  );
};

AccountProfileEdit.propTypes = {
  history: PropTypes.objectOf(PropTypes.any).isRequired,
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  history: state.history,
});

const AccountProfileEditConnected = connect(
  mapStateToProps
)(AccountProfileEdit);

export default AccountProfileEditConnected;
