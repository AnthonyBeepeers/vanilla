import React, { PropTypes } from 'react/lib/React';
import { connect } from 'react-redux';

import NotPendingAccount from 'reactjs-beepeers-framework/components/Account/Renderers/Web/NotPendingAccount/NotPendingAccount';
import OwnedActorsFetcher from 'reactjs-beepeers-framework/components/Actors/Fetchers/OwnedActorsFetcher/OwnedActorsFetcher';

import AccountMenu from '../AccountMenu/AccountMenu';

import { CONTRIBUTOR_KEY } from '../../../constants/actors';
import AccountHeaderElem from '../AccountHeaderElem/AccountHeaderElem';

import styles from './AccountHeader.css';

const AccountHeader = ({ dispatch, accountProfile: receivedAccountProfile = null, ownedExhibitors: receivedOwnedExhibitors = null }) => {
  const renderAccount = (accountProfile) => {
    if (accountProfile !== null) {
      const ownedExhibitorsRenderFunc = (ownedExhibitors) => {
        /* eslint-disable new-cap */
        let profileMenu = AccountMenu(dispatch);
        /* eslint-enable new-cap */

        if (ownedExhibitors.length > 0) {
          const firstExhibitor = ownedExhibitors[0];
          /* eslint-disable new-cap */
          profileMenu = AccountMenu(dispatch, firstExhibitor.profileId);
          /* eslint-enable new-cap */
        }

        return (
          <AccountHeaderElem className={styles.headerContent} profileMenu={profileMenu} />
        );
      };

      if (receivedOwnedExhibitors !== null) {
        return ownedExhibitorsRenderFunc(receivedOwnedExhibitors);
      }

      return (
        <OwnedActorsFetcher
          actorId={accountProfile.profileId}
          profileTypeKey={CONTRIBUTOR_KEY}
          nbResult={10}
          flattenSections
          renderFunc={ownedExhibitorsRenderFunc}
          forceFetch
        />
      );
    }
    return null;
  };

  if (receivedAccountProfile !== null) {
    return renderAccount(receivedAccountProfile);
  }

  return (
    <NotPendingAccount
      renderAccount={renderAccount}
      className={styles.message}
    />
  );
};

AccountHeader.propTypes = {
  dispatch: PropTypes.func.isRequired,
  accountProfile: PropTypes.objectOf(PropTypes.any),
  ownedExhibitors: PropTypes.arrayOf(PropTypes.any),
};

const AccountHeaderConnected = connect()(AccountHeader);

export default AccountHeaderConnected;
