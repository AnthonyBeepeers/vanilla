import React, { PropTypes } from 'react/lib/React';
import { connect } from 'react-redux';

import { FormattedMessage } from 'react-intl';

import { Link } from 'react-router-dom';

import { logOut } from 'reactjs-beepeers-framework/actions/auth';

import Grid from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Grid/Grid';
import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
import NotPendingAccount from 'reactjs-beepeers-framework/components/Account/Renderers/Web/NotPendingAccount/NotPendingAccount';
import OwnedActorsFetcher from 'reactjs-beepeers-framework/components/Actors/Fetchers/OwnedActorsFetcher/OwnedActorsFetcher';
import AccountInfos from 'reactjs-beepeers-framework/components/Account/Renderers/Web/AccountInfos/AccountInfos';

import { CONTRIBUTOR_KEY } from '../../../constants/actors';

import AccountMenu from '../AccountMenu/AccountMenu';

import styles from './AccountProfile.css';
import AccountHeader from '../AccountHeader/AccountHeader';

const AccountProfile = ({ dispatch }) => {
  const logOutMenuItemNode = (
    <Link
      to="/myaccount/logout"
      onClick={() => dispatch(logOut())}
    >
      <FormattedMessage id={'ACCOUNT.MENU.LOG_OUT'} />
    </Link>
  );

  const renderAccount = (accountProfile) => {
    if (accountProfile !== null) {
      const ownedExhibitorsRenderFunc = (ownedExhibitors) => {
        /* eslint-disable new-cap */
        let profileMenu = AccountMenu(dispatch);
        /* eslint-enable new-cap */

        if (ownedExhibitors.length > 0) {
          const firstExhibitor = ownedExhibitors[0];
          /* eslint-disable new-cap */
          profileMenu = AccountMenu(dispatch, firstExhibitor.profileId);
          /* eslint-enable new-cap */
        }

        return (
          <FullRow>
            <AccountHeader accountProfile={accountProfile} ownedExhibitors={ownedExhibitors} className={styles.header} profileMenu={profileMenu} />
            <AccountInfos className={styles.infos} />
          </FullRow>
        );
      };

      return (
        <OwnedActorsFetcher
          actorId={accountProfile.profileId}
          profileTypeKey={CONTRIBUTOR_KEY}
          nbResult={10}
          flattenSections
          renderFunc={ownedExhibitorsRenderFunc}
          forceFetch
        />
      );
    }

    return null;
  };

  const notPendingLogOutNode = (
    <FullRow className={styles.notPendingLogOut}>
      { logOutMenuItemNode }
    </FullRow>
  );

  return (
    <FullRow className={styles.mainContent}>
      <Grid className={styles.contentWrapper}>
        <NotPendingAccount
          renderAccount={renderAccount}
          className={styles.message}
          logOutNode={notPendingLogOutNode}
          forceFetch
        />
      </Grid>
    </FullRow>
  );
};

AccountProfile.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const AccountProfileConnected = connect()(AccountProfile);

export default AccountProfileConnected;
