import React from 'react/lib/React';

import Grid from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Grid/Grid';
import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
import NotPendingAccount from 'reactjs-beepeers-framework/components/Account/Renderers/Web/NotPendingAccount/NotPendingAccount';
import AccountFavorites from 'reactjs-beepeers-framework/components/Account/Renderers/Web/AccountFavorites/AccountFavorites';
import { FormattedMessage } from 'react-intl';

import AccountHeader from '../AccountHeader/AccountHeader';

import morePicture from '../../../assets/images/more_btn.png';
import EventDefaultPicture from '../../../assets/images/news_default.png';
import LinkedEvent from '../../General/LinkedEvent/LinkedEvent';

import styles from './AccountProgram.css';
import { EVENT_ACTOR_TYPE } from '../../../constants/actors';
import { PURPLE } from '../../../constants/colors';

const AccountProgram = () => {
  const renderAccount = (accountProfile) => {
    if (accountProfile !== null) {
      const renderEventFunc = eventFest => (
        <LinkedEvent event={eventFest} />
      );
      const emptyEventNode = (
        <div className={styles.empty}>
          <FormattedMessage id={'ACCOUNT.FAV.PROGRAM.EMPTY'} />
        </div>
      );
      const favoritesTypes = [
        {
          name: EVENT_ACTOR_TYPE,
          title: 'ACCOUNT.MY_FAVORITES.EVENTS_LIST',
          defaultFavoritePictureUrl: EventDefaultPicture,
          morePictureUrl: morePicture,
          loadingColor: PURPLE,
          nbResult: 10,
          renderFavoriteFunc: renderEventFunc,
          responsiveParams: {
            xs: 12,
            sm: 6,
            md: 6,
            lg: 6,
          },
          flattenSections: false,
          emptyNode: emptyEventNode,
        },
      ];
      return (
        <FullRow className={styles.contentWrapper}>
          <AccountHeader accountProfile={accountProfile} />
          <AccountFavorites
            className={styles.accountProgram}
            types={favoritesTypes}
          />
        </FullRow>
      );
    }

    return null;
  };

  return (
    <FullRow>
      <Grid className={styles.mainContent}>
        <NotPendingAccount
          renderAccount={renderAccount}
          className={styles.message}
        />
      </Grid>
    </FullRow>
  );
};

export default AccountProgram;
