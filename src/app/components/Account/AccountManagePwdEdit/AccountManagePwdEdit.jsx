import React, { PropTypes } from 'react/lib/React';
import { connect } from 'react-redux';

import Grid from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Grid/Grid';
import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
import NotPendingAccount from 'reactjs-beepeers-framework/components/Account/Renderers/Web/NotPendingAccount/NotPendingAccount';
import AccountPwdEdition from 'reactjs-beepeers-framework/components/Account/Renderers/Web/AccountPwdEdition/AccountPwdEdition';
import OwnedActorsFetcher from 'reactjs-beepeers-framework/components/Actors/Fetchers/OwnedActorsFetcher/OwnedActorsFetcher';

import { PURPLE, LPE_NORMAL_FIELD_STYLE } from '../../../constants/colors';

import styles from './AccountManagePwdEdit.css';
import AccountMenu from '../AccountMenu/AccountMenu';
import AccountHeader from '../AccountHeader/AccountHeader';
import { CONTRIBUTOR_KEY } from '../../../constants/actors';

const AccountManagePwdEdit = ({ history, dispatch }) => {
  const renderAccount = (accountProfile) => {
    if (accountProfile !== null) {
      const header = () => {
        const renderFunc = (ownedExhibitors) => {
          /* eslint-disable new-cap */
          let profileMenu = AccountMenu(dispatch);
          /* eslint-enable new-cap */

          if (ownedExhibitors.length > 0) {
            const firstExhibitor = ownedExhibitors[0];
            /* eslint-disable new-cap */
            profileMenu = AccountMenu(dispatch, firstExhibitor.profileId);
            /* eslint-enable new-cap */
          }

          return (<AccountHeader accountProfile={accountProfile} ownedExhibitors={ownedExhibitors} className={styles.header} profileMenu={profileMenu} />);
        };

        return (<OwnedActorsFetcher
          actorId={accountProfile.profileId}
          profileTypeKey={CONTRIBUTOR_KEY}
          nbResult={10}
          flattenSections
          renderFunc={renderFunc}
          forceFetch
        />);
      };

      const afterAccountPwdEditionFunc = () => {
        history.push('/myaccount/manage');
      };

      return (
        <FullRow>
          { header() }
          <AccountPwdEdition
            formColors={{ background: PURPLE, label: 'white' }}
            afterAccountPwdEditionFunc={afterAccountPwdEditionFunc}
            formNormalFieldStyle={LPE_NORMAL_FIELD_STYLE}
          />
        </FullRow>
      );
    }

    return null;
  };

  return (
    <FullRow className={styles.mainContent}>
      <Grid className={styles.contentWrapper}>
        <NotPendingAccount
          renderAccount={renderAccount}
          className={styles.message}
        />
      </Grid>
    </FullRow>
  );
};

AccountManagePwdEdit.propTypes = {
  history: PropTypes.objectOf(PropTypes.any).isRequired,
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  history: state.history,
});

const AccountManagePwdEditConnected = connect(
  mapStateToProps
)(AccountManagePwdEdit);

export default AccountManagePwdEditConnected;
