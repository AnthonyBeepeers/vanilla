import React, { PropTypes } from 'react/lib/React';

import { FormattedMessage } from 'react-intl';

import { Link } from 'react-router-dom';

import { logOut } from 'reactjs-beepeers-framework/actions/auth';

import styles from './AccountMenu.css';

const AccountMenu = (dispatch) => {
  const logOutMenuItemNode = (
    <Link
      to="/myaccount/logout"
      onClick={() => dispatch(logOut())}
    >
      <FormattedMessage id={'ACCOUNT.MENU.LOG_OUT'} />
    </Link>
  );
  const myFavoritesMenuItemNode = (
    <Link to="/myaccount/manage">
      <FormattedMessage id={'ACCOUNT.MENU.MY_PROFILE'} />
    </Link>
  );
  const editProfileMenuItemNode = (
    <Link to="/myaccount/profile/edit">
      <FormattedMessage id={'ACCOUNT.MENU.EDIT_PROFILE'} />
    </Link>
  );
  const passwordEditItemNode = (
    <Link to="/myaccount/manage/pwdedit">
      <FormattedMessage id={'MENU.ACCOUNT.MANAGE.EDIT_MY_ACCOUNT.EDIT_PWD'} />
    </Link>
  );

  const editProfileMenuItem = {
    menuItemClassName: styles.menuItem,
    node: editProfileMenuItemNode,
  };
  const myFavoritesMenuItem = {
    menuItemClassName: styles.menuItem,
    node: myFavoritesMenuItemNode,
  };
  const logOutMenuItem = {
    menuItemClassName: styles.menuItem,
    node: logOutMenuItemNode,
  };

  const passwordEditItem = {
    menuItemClassName: styles.menuItem,
    node: passwordEditItemNode,
  };

  const menu = {
    items: [
      myFavoritesMenuItem,
      editProfileMenuItem,
      passwordEditItem,
      logOutMenuItem,
    ],
  };

  return menu;
};

AccountMenu.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

export default AccountMenu;
