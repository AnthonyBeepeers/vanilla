import React, { PropTypes } from 'react/lib/React';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';

import NotPendingAccount from 'reactjs-beepeers-framework/components/Account/Renderers/Web/NotPendingAccount/NotPendingAccount';
import CoverPicture from 'reactjs-beepeers-framework/components/General/Web/CoverPicture/CoverPicture';
import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
import Row from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Row/Row';
import Col from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Col/Col';

import defaultCoverPicture from '../../../assets/images/default_cover_picture.jpg';
import defaultPersonPicture from '../../../assets/images/default_user_picture.jpg';

import styles from './AccountHeaderElem.css';

const AccountHeaderElem = ({ profileMenu = null, className = '', history }) => {
  const renderAccount = (accountProfile) => {
    if (accountProfile !== null) {
      let nameNode = null;
      if (typeof (accountProfile.displayName) !== 'undefined') {
        nameNode = (
          <div className={styles.profileName}>
            {accountProfile.displayName}
          </div>
        );
      }

      let coverPictureNode = (
        <CoverPicture pictureUrl={defaultCoverPicture} className={styles.cover} />
      );
      if (typeof (accountProfile.coverUrl) !== 'undefined' && accountProfile.coverUrl !== '') {
        coverPictureNode = (
          <CoverPicture pictureUrl={accountProfile.coverUrl} className={styles.cover} />
        );
      }

      let profilePictureNode = (
        <CoverPicture pictureUrl={defaultPersonPicture} className={styles.cover} />
      );
      if (typeof (accountProfile.thumbnailUrl) !== 'undefined' && accountProfile.thumbnailUrl !== '') {
        profilePictureNode = (
          <CoverPicture pictureUrl={accountProfile.thumbnailUrl} className={styles.cover} />
        );
      }

      let profileMenuNode = null;
      if (profileMenu !== null) {
        const { menuClassName = '', items = [] } = profileMenu;
        const menuItemsNodes = items.map(({ menuItemClassName = '', node }, i) => (
          <Col xs={6} md={12} key={i}>
            <div className={`${styles.profileMenuItem} ${menuItemClassName}`}>
              {node}
            </div>
          </Col>
        ));

        if (menuItemsNodes.length > 0) {
          profileMenuNode = (
            <div className={`${styles.profileMenu} ${menuClassName}`}>
              <Row center="xs">
                { menuItemsNodes }
              </Row>
            </div>
          );
        }
      }

      const headerContent = (
        <div className={styles.headerWrapper}>
          <div className={styles.headerContent}>
            { coverPictureNode }
            <div className={styles.profileWrapper}>
              <div className={styles.profilePictureWrapper}>
                { profilePictureNode }
              </div>
              <div className={styles.profileNameWrapper}>
                { nameNode }
              </div>
            </div>
          </div>
        </div>
      );

      const header = (
        <FullRow>
          { headerContent }
          { profileMenuNode }
        </FullRow>
      );
//
      let manageClassNames = styles.links;
      if (history.location.pathname === '/myaccount/manage') {
        manageClassNames = `${manageClassNames} ${styles.selectedLink}`;
      }

      let programClassNames = styles.links;
      if (history.location.pathname === '/myaccount/myprogram') {
        programClassNames = `${programClassNames} ${styles.selectedLink}`;
      }

      return (
        <FullRow className={className}>
          { header }
          <Row className={styles.favoritesMenuItemsWrapper}>
            <Col xs={6} className={styles.linksContainer}>
              <Link to="/myaccount/manage" className={manageClassNames}>
                <i className="fa fa-user" />
                <FormattedMessage id={'ACCOUNT.MENU.MY_PROFILE'} />
              </Link>
            </Col>
            <Col xs={6} className={styles.linksContainer}>
              <Link to="/myaccount/myprogram" className={programClassNames}>
                <i className="fa fa-calendar" />
                <FormattedMessage id={'ACCOUNT.MENU.MY_PROGRAM'} />
              </Link>
            </Col>
          </Row>
        </FullRow>
      );
    }

    return null;
  };

  return (
    <NotPendingAccount
      renderAccount={renderAccount}
      forceFetch
    />
  );
};

AccountHeaderElem.propTypes = {
  className: PropTypes.string,
  /* eslint-disable react/no-unused-prop-types */
  profileMenu: PropTypes.shape({
    menuClassName: PropTypes.string,
    items: PropTypes.arrayOf(PropTypes.shape({
      menuItemClassName: PropTypes.string,
      node: PropTypes.node.isRequired,
    })).isRequired,
  }),
  /* eslint-enable react/no-unused-prop-types */
  history: PropTypes.objectOf(PropTypes.any).isRequired,
};


const mapStateToProps = state => ({
  history: state.history,
});

const AccountHeaderElemConnected = connect(
  mapStateToProps
)(AccountHeaderElem);

export default AccountHeaderElemConnected;
