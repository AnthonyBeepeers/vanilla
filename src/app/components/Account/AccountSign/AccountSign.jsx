import React, { PropTypes } from 'react/lib/React';

import { FormattedMessage } from 'react-intl';

import Grid from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Grid/Grid';
import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
import Row from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Row/Row';
import Col from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Col/Col';
import AccountSignInForm from 'reactjs-beepeers-framework/components/Account/Renderers/Web/AccountSignInForm/AccountSignInForm';
import AccountSignUpForm from 'reactjs-beepeers-framework/components/Account/Renderers/Web/AccountSignUpForm/AccountSignUpForm';
import AccountTwitterSignIn from 'reactjs-beepeers-framework/components/Account/Renderers/Web/AccountTwitterSignIn/AccountTwitterSignIn';
import AccountFacebookSignIn from 'reactjs-beepeers-framework/components/Account/Renderers/Web/AccountFacebookSignIn/AccountFacebookSignIn';

import { extractQuery } from 'reactjs-beepeers-framework/constants/helpers';

import { PURPLE, ORANGE, WHITE, LPE_NORMAL_FIELD_STYLE } from '../../../constants/colors';

import styles from './AccountSign.css';

const AccountSign = ({ location }) => {
  const params = extractQuery(location.search);

  let hintNode = (
    <div className={styles.containerHint}>
      <div className={styles.hint}>
        <FormattedMessage id={'MENU.ACCOUNT.SIGN.HINT'} />
      </div>
    </div>
  );
  let accountSignUpFormProps = {};
  let accountSignInFormProps = {};
  let afterSocialSignInPath = '';
  if (params !== null && typeof (params.source) !== 'undefined' && params.source === 'exhibitorcreate') {
    hintNode = (
      <div className={`${styles.containerHint} ${styles.exhibitorcreateHint}`}>
        <div className={styles.hint}>
          <FormattedMessage id={'MENU.ACCOUNT.SIGN.EXHIBITOR_CREATE_HINT'} />
        </div>
      </div>
    );
    accountSignUpFormProps = {
      afterSignUpPath: '/myaccount/exhibitor/create',
    };
    accountSignInFormProps = {
      afterLogInPath: '/myaccount/exhibitor/create',
    };
    afterSocialSignInPath = '/myaccount/exhibitor/create';
  }

  return (
    <Grid className={styles.wrapper}>
      <FullRow>
        { hintNode }
      </FullRow>
      <Row >
        <Col xs={12} sm={6} className={styles.signContainers}>
          <AccountSignUpForm
            formColor={PURPLE}
            formNormalFieldStyle={LPE_NORMAL_FIELD_STYLE}
            {...accountSignUpFormProps}
          />
        </Col>
        <Col xs={12} sm={6} className={styles.signContainers}>
          <Row>
            <Col xs={6}>
              <AccountFacebookSignIn loadingColor={ORANGE} afterSignInPath={afterSocialSignInPath} iconHoverColor={WHITE} />
            </Col>
            <Col xs={6}>
              <AccountTwitterSignIn loadingColor={ORANGE} afterSignInPath={afterSocialSignInPath} iconHoverColor={WHITE} />
            </Col>
          </Row>
          <AccountSignInForm
            formColor={PURPLE}
            formNormalFieldStyle={LPE_NORMAL_FIELD_STYLE}
            {...accountSignInFormProps}
          />
        </Col>
      </Row>
    </Grid>
  );
};

AccountSign.propTypes = {
  location: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default AccountSign;
