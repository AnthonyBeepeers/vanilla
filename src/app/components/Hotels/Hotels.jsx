import React, { PropTypes } from 'react/lib/React';
import { connect } from 'react-redux';

import Grid from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Grid/Grid';
import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
import ActorsListFetcher from 'reactjs-beepeers-framework/components/Actors/Fetchers/ActorsListFetcher/ActorsListFetcher';
import FullLineTitle from 'reactjs-beepeers-framework/components/General/Web/FullLineTitle/FullLineTitle';
import Map from '../General/Map/LeafletMap';

import LinkedTicket from '../General/LinkedTicket/LinkedTicket';

import { SUBS } from '../../constants/actors';

import styles from './Hotels.css';
// import HoloColoredTitle from '../HoloColoredTitle/HoloColoredTitle';

const Partners = ({ className = '', locales }) => {
  const { lang } = locales;
  const langWithoutRegionCode = lang.toLowerCase().split(/[_-]+/)[0];

  let button = 'click';
  if (langWithoutRegionCode === 'fr') {
    button = 'click';
  }
  const renderFunc = (partnersSections) => {
    if (partnersSections[0] == null) {
      console.log('IS NULL');
      return null;
    }
    const foo = partnersSections[0].items;

    const renderPartnerFunc = (partner) => {
      console.log('partner');
      console.log(partner);
      if (typeof (partner.originalUrl) !== 'undefined' && typeof (partner.website) !== 'undefined') {
        return (<LinkedTicket partner={partner} pictureUrl={`${partner.originalUrl}&type=png&width=800&height=800`} />);
      }

      return null;
    };

    const partnersNodes = foo.map((partner, i) => {
      console.log('partnersNodes');
      console.log(partner);
      return (
        <div key={i}>
          <FullLineTitle title={partner.name} intlActivated={false} className={styles.sectionTitle} />
          { renderPartnerFunc(partner) }
        </div>
      ); });
    console.log(partnersNodes);

    return (
      <FullRow>
        <div className={className}>
          {partnersNodes}
        </div>
      </FullRow>
    );
  };
  /* eslint-disable */
  const link = 'https://secure.accorhotels.com/store/index.html#/fr/hotels?dateIn=2018-12-18&adults=1&origin=accorhotels&domainId=https:%2F%2Fwww.accorhotels.com&q=1544,6017,3043&children=&nights=1&tksToHighlight=ALT18';

  return (
    <Grid className={styles.wrapper}>
      <FullRow>
        <Map />
      </FullRow>
      <ActorsListFetcher
        actorsListKey={SUBS}
        renderFunc={renderFunc}
      />
      <div className={styles.container}>
       <a href={link} target="_blank" rel="noopener noreferrer" className={styles.button}> {button} </a>
      </div>
    </Grid>
  );
};

Partners.propTypes = {
  className: PropTypes.string,
 locales: PropTypes.objectOf(PropTypes.oneOfType([
  PropTypes.string,
  PropTypes.object,
 ])).isRequired,
};

const mapStateToProps = state => ({
  locales: state.locales,
});

const PartnersConnected = connect(
 mapStateToProps
)(Partners);

export default PartnersConnected;
