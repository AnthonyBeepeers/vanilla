import React, { PropTypes } from 'react/lib/React';
import { connect } from 'react-redux';

import Link from 'reactjs-beepeers-framework/components/Routing/LinkWithLang/LinkWithLang';

import ContainPicture from 'reactjs-beepeers-framework/components/General/Web/ContainPicture/ContainPicture';

import { selectLinkId } from 'reactjs-beepeers-framework/actions/menu';

import Logo from '../../../../assets/images/header.png';
import Date from '../../../../assets/images/date.png';

import styles from './LogoHeader.css';

const LogoHeader = ({ dispatch, ...remainProps }) =>
  (
    <div className={styles.logoHeader}>
      <Link to="/" onClick={() => dispatch(selectLinkId(null))} {...remainProps} className={styles.container}>
        <ContainPicture pictureUrl={Logo} style={{ height: '60%' }} />
        <ContainPicture pictureUrl={Date} />
      </Link>
    </div>
  );

LogoHeader.propTypes = {
  dispatch: PropTypes.func.isRequired,
  mini: PropTypes.bool,
};

const LogoHeaderConnected = connect()(LogoHeader);

export default LogoHeaderConnected;
