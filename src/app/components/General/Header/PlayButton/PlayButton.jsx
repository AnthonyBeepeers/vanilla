import React from 'react/lib/React';
import spotify from '../../../../assets/images/Spotify_Logo_White.png';
import styles from './PlayButton.css';
/* eslint-disable */
const PlayButton = () =>
 (
   <div className={styles.container}>
     <a href="https://open.spotify.com/user/vpsqu33al2k6bdc60q5b9ejwy/playlist/3oGgrAmjdefaJ5TCBvJ7bx?si=6bSThZ0XRzO0hzwSDrVYpg" className={styles.cardButtonLink} target="_blank" rel="noopener noreferrer">
       <img className={styles.cardButtonLink} src={spotify} role="presentation" />
     </a>
   </div>
 );

export default PlayButton;
