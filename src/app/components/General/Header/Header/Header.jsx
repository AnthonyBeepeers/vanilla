import React, { Component, PropTypes } from 'react/lib/React';

import { withRouter } from 'react-router-dom';

import $ from 'jquery/dist/jquery.min';

import Grid from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Grid/Grid';
import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
import Row from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Row/Row';
import Col from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Col/Col';
import FrEnUrlSwitcher from '../../FrEnUrlSwitcher/FrEnUrlSwitcher';

import Routes from '../../../../Routes';

import DrawerMenu from '../../DrawerMenu/DrawerMenu';
import AccountProfileButton from '../AccountProfileButton/AccountProfileButton';
import LogoHeader from '../LogoHeader/LogoHeader';
import PlayButton from '../PlayButton/PlayButton';
import Menu from '../../Menu/Menu/Menu';

import styles from './Header.css';
import MoreMenu from '../../Menu/MoreMenu/MoreMenu';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menu: null,
    };
  }


  componentWillMount() {
    this.onScrollHandler = null;
  }

  componentDidMount() {
    // if (location.pathname !== '/' && this.onScrollHandler === null) {
    this.onScrollHandler = event => this.onScroll(event);
    $(window)
      .scroll(this.onScrollHandler);
    // }
  }

  componentWillUnmount() {
    this.offScroll();
  }

  onScroll() {
    const updatedState = {
      scrollTop: $(window)
        .scrollTop(),
      menu: {
        width: $(this.menuNode)
          .width(),
      },
    };
    if (this.state.menu === null) {
      updatedState.menu = {
        top: $(this.menuNode)
          .offset().top,
        left: $(this.menuNode)
          .offset().left,
        width: $(this.menuNode)
          .width(),
        height: $(this.menuNode)
          .height(),
      };
    } else {
      updatedState.menu = {
        ...this.state.menu,
        width: updatedState.menu.width,
      };
    }
    this.setState(updatedState);
  }

  offScroll() {
    if (typeof (this.onScrollHandler) !== 'undefined' && this.onScrollHandler !== null) {
      $(window)
        .off('scroll', this.onScrollHandler);
    }
  }

  render() {
    const { scrollTop = null, menu = null } = this.state;
    const upMenu = (
      <Row center="sm">
        <Col xs={0} sm={12}>
          <Row>
            <Col xs={0} sm={4} className={styles.logoMini} />
            <Col xs={0} sm={3} >
              <Row center="sm">
                <Col smOffset={3} xs={0} sm={3} className={styles.logoMini}>
                  <PlayButton />
                </Col>
                <Col xs={0} sm={3} className={styles.logoMini}>
                  <AccountProfileButton />
                </Col>
                <Col xs={0} sm={3} className={styles.logoMini}>
                  <FrEnUrlSwitcher className={styles.langSwitcher} verticalMode />
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>
    );
    const downMenu = (
      <Row center="sm">
        <Col xs={0} sm={12}>
          <Row>
            <Col xs={11}>
              <Menu routes={Routes} />
            </Col>
            <Col xs={1}>
              <MoreMenu routes={Routes} />
            </Col>
          </Row>
        </Col>
      </Row>
   );
    let menuNode = (
      <FullRow>
        {upMenu}
        {downMenu}
      </FullRow>
    );

    menuNode = (
      <FullRow>
        <Row>
          <Col sm={4} className={styles.logo}>
            <LogoHeader />
          </Col>
          <Col sm={8} className={styles.logo}>
            <Row>
              <Col smOffset={9} sm={3} className={styles.logoMini} style={{ paddingTop: '2%' }}>
                <FrEnUrlSwitcher className={styles.langSwitcher} />
              </Col>
            </Row>
            <Row>
              <Col sm={12} className={styles.logoMini}>
                <Row>
                  <Menu routes={Routes} sm={12} />
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>
      </FullRow>);

    let menuClassName = '';
    if (scrollTop !== null && menu !== null && scrollTop > (menu.top + menu.height)) {
      menuClassName = styles.stickyMenuContainer;
      menuNode = (
        <Grid>
          {menuNode}
        </Grid>
      );
    }
    return (
      <FullRow className={styles.mainContent}>
        <Grid>
          <FullRow>
            <Row>
              <Col xs={2} sm={0}>
                <DrawerMenu routes={Routes} />
              </Col>
              <Col xs={10} sm={0} className={styles.logoHeaderXS}>
                gfgff
              </Col>
              <Col xs={0} sm={12}>
                <div
                  ref={(node) => {
                    this.menuNode = node;
                  }}
                  className={menuClassName}
                >
                  {menuNode}
                </div>
              </Col>
            </Row>
          </FullRow>
        </Grid>
      </FullRow>
    );
  }
}

Header.propTypes = {
  location: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ])).isRequired,
};

const HeaderWithRouter = withRouter(Header);

export default HeaderWithRouter;
