import React, { PropTypes } from 'react/lib/React';
import { connect } from 'react-redux';

import Link from 'reactjs-beepeers-framework/components/Routing/LinkWithLang/LinkWithLang';

import ContainPicture from 'reactjs-beepeers-framework/components/General/Web/ContainPicture/ContainPicture';

import { selectLinkId } from 'reactjs-beepeers-framework/actions/menu';

import Date from '../../../../assets/images/date.png';

import styles from './DateHeader.css';

const DateHeader = ({ dispatch, ...remainProps }) =>
  (
    <div className={styles.DateHeader}>
      <Link to="/" onClick={() => dispatch(selectLinkId(null))} {...remainProps}>
        <ContainPicture pictureUrl={Date} />
      </Link>
    </div>
  );

DateHeader.propTypes = {
  dispatch: PropTypes.func.isRequired,
  mini: PropTypes.bool,
};

const DateHeaderConnected = connect()(DateHeader);

export default DateHeaderConnected;
