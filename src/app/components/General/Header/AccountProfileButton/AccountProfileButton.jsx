import React, { PropTypes } from 'react/lib/React';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import ContainPicture from 'reactjs-beepeers-framework/components/General/Web/ContainPicture/ContainPicture';

import AccountProfileDefault from '../../../../assets/images/accountProfile.png';

import styles from './AccountProfileButton.css';

const AccountProfileBtn = ({ auth, onClick = null }) => {
  let onClickParam = {};
  if (onClick !== null) {
    onClickParam = {
      onClick,
    };
  }

  let finalPicture = AccountProfileDefault;

  if (
    (
      typeof (auth.accountIsFetching) === 'undefined'
      || !auth.accountIsFetching
    )
    && typeof (auth.account) !== 'undefined'
    && auth.account !== null
    && typeof (auth.account.accountId) !== 'undefined'
  ) {
    const accountProfile = auth.account.profiles[0];
    if (typeof (accountProfile.thumbnailUrl) !== 'undefined' && accountProfile.thumbnailUrl !== null && accountProfile.thumbnailUrl !== '') {
      finalPicture = accountProfile.thumbnailUrl;
    }
  }

  return (
    <div className={styles.wrapper}>
      <Link to="/myaccount/sign" className={styles.linkWrapper} {...onClickParam}>
        <div className={styles.pictureWrapper}>
          <div className={styles.pictureContent}>
            <ContainPicture pictureUrl={finalPicture} />
          </div>
        </div>
      </Link>
    </div>
  );
};

AccountProfileBtn.propTypes = {
  auth: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
    PropTypes.object,
  ])).isRequired,
  onClick: PropTypes.func,
};

const mapStateToProps = state => ({
  auth: state.auth,
});

const AccountProfileBtnConnected = connect(
  mapStateToProps
)(AccountProfileBtn);

export default AccountProfileBtnConnected;

