import React from 'react/lib/React';

import styles from './CardButton.css';

import cardPicture from '../../../../assets/images/shopping-cart.png';

const CardButton = () =>
  (
    <div className={styles.container}>
      <a href="https://www.youtube.com/watch?v=jBX3xK0MdlE" className={styles.cardButtonLink} target="_blank" rel="noopener noreferrer">
        <img src={cardPicture} alt="" />
      </a>
    </div>
  );

export default CardButton;
