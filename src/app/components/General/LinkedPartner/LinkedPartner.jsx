import React, { PropTypes } from 'react/lib/React';

import ContainPictureLink from 'reactjs-beepeers-framework/components/General/Web/ContainPictureLink/ContainPictureLink';
import ResponsiveContainer from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/ResponsiveContainer/ResponsiveContainer';

import styles from './LinkPartner.css';

const LinkedPartner = ({ partner, pictureUrl }) => (
  <ResponsiveContainer
    className={styles.partnerWrapper}
    xs={{ height: '25vw', width: '25vw' }}
    sm={{ height: '141px', width: '141px' }}
    md={{ height: '155px', width: '155px' }}
    lg={{ height: '162px', width: '162px' }}
  >
    <ContainPictureLink
      pictureUrl={pictureUrl}
      linkPath={partner.website}
      linkName=""
    />
  </ResponsiveContainer>
  );

LinkedPartner.propTypes = {
  partner: PropTypes.objectOf(PropTypes.any).isRequired,
  pictureUrl: PropTypes.string.isRequired,
};

export default LinkedPartner;
