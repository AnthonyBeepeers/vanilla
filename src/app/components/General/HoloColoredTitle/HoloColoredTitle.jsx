import React, { PropTypes } from 'react/lib/React';

import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
import Grid from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Grid/Grid';

import ColoredTitle from '../ColoredTitle/ColoredTitle';

import styles from './HoloColoredTitle.css';

const HoloColoredTitle = ({ title, children, intlActivated = true, className = '', gridClassName = '', enableGrid = true, ...remainProps }) => {
  let contentNode = (
    <Grid className={`${styles.grid} ${gridClassName}`}>
      { children }
    </Grid>
  );

  if (!enableGrid) {
    contentNode = children;
  }

  return (
    <FullRow className={`${styles.wrapper} ${className}`} {...remainProps}>
      <ColoredTitle
        title={title}
        intlActivated={intlActivated}
        className={styles.title}
      >
        { contentNode }
      </ColoredTitle>
    </FullRow>
  );
};

HoloColoredTitle.propTypes = {
  className: PropTypes.string,
  gridClassName: PropTypes.string,
  title: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ]).isRequired,
  children: PropTypes.node.isRequired,
  intlActivated: PropTypes.bool,
  enableGrid: PropTypes.bool,
};

export default HoloColoredTitle;
