import React, { Component, PropTypes } from 'react/lib/React';
import { connect } from 'react-redux';

import { withRouter } from 'react-router-dom';

import Row from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Row/Row';
import Col from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Col/Col';
import ResponsiveContainer from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/ResponsiveContainer/ResponsiveContainer';
import Link from 'react-router-dom/Link';
// import Link from 'reactjs-beepeers-framework/components/Routing/LinkWithLang/LinkWithLang';
// import ContainPicture from 'reactjs-beepeers-framework/components/General/Web/ContainPicture/ContainPicture';

import { displaySubmenu } from 'reactjs-beepeers-framework/actions/menu';
import { USER } from 'reactjs-beepeers-framework/constants/auth';

import { injectIntl } from 'react-intl';

import styles from './MainMenu.css';

class MainMenu extends Component {

  render() {
    const { dispatch, routes, menu, auth, intl, location, className = '' /* , ...remainProps */ } = this.props;
    const { submenuDisplayed, linkIdSelected, pattern, pathname } = menu;
    const { formatMessage } = intl;

    let menuOpened = false;

    const menuItems = routes.map((route, i) => {
      if (typeof (route.onlyLogIn) !== 'undefined') {
        if (route.onlyLogIn) {
          if (!auth.isFetching && auth.accountType !== USER) {
            return null;
          }
        } else if (!auth.isFetching && auth.accountType === USER) {
          return null;
        }
      }
      if (typeof (route.name) !== 'undefined' && typeof (route.routes) !== 'undefined') {
        route.routes.forEach((subRoute, indexSubRoute) => {
          if (typeof (subRoute.name) !== 'undefined' && subRoute.name !== '' && typeof (subRoute.pattern) !== 'undefined' && subRoute.pattern !== '') {
            if (submenuDisplayed !== null) {
              if (submenuDisplayed === indexSubRoute) {
                menuOpened = true;
              }
            } else {
              const linkId = `${i}_${indexSubRoute}`;
              if (linkId === linkIdSelected || subRoute.pattern === pattern || subRoute.pattern === pathname) {
                menuOpened = true;
              }
            }
          }
        });

        const routeName = formatMessage({ id: route.name });

        let routeStyle = {};
        if (typeof (route.textColor) !== 'undefined') {
          routeStyle = {
            color: route.textColor,
          };
        }

        return (
          <Col
            key={i}
            xs
            style={{ backgroundColor: route.color }}
          >
            <button
              className={styles.button}
              onClick={() => dispatch(displaySubmenu(i))}
              onMouseEnter={() => dispatch(displaySubmenu(i))}
            >
              <ResponsiveContainer
                sm={{ fontSize: '1.2rem' }}
                md={{ fontSize: '1.4rem' }}
                lg={{ fontSize: '1.8rem' }}
                className={styles.buttonLabel}
                style={routeStyle}
              >
                { routeName }
              </ResponsiveContainer>
            </button>
          </Col>
        );
      }

      if (typeof (route.name) !== 'undefined' && typeof (route.pattern) !== 'undefined' && route.pattern !== '') {
        const routeName = formatMessage({ id: route.name });

        let routeBackgroundStyle = {};
        if (typeof (route.color) !== 'undefined') {
          routeBackgroundStyle = {
            backgroundColor: route.color,
          };
        }

        let routeStyle = {};
        if (typeof (route.textColor) !== 'undefined') {
          routeStyle = {
            color: route.textColor,
          };
        }

        let allMenuClasses = styles.button;
        let selected = false;

        if (typeof (route.menuSelectedPatterns) !== 'undefined' && route.menuSelectedPatterns.length > 0) {
          route.menuSelectedPatterns.forEach((selPattern) => {
            if (location.pathname.startsWith(selPattern) || location.pathname.substring(3, location.pathname.length).startsWith(selPattern)) {
              selected = true;
            }
          });
        }
        if (!selected) {
          if (location.pathname === '/' || route.pattern === '/' || location.pathname.substring(3, location.pathname.length) === '/') {
            if (location.pathname === route.pattern || location.pathname.substring(3, location.pathname.length) === route.pattern) {
              selected = true;
            }
          } else if (location.pathname.startsWith(route.pattern) || location.pathname.substring(3, location.pathname.length).startsWith(route.pattern)) {
            selected = true;
          }
        }

        if (selected) {
          allMenuClasses = `${allMenuClasses} ${styles.menuItemSelected}`;
        }

        const menuItemContent = (
          <ResponsiveContainer
            sm={{ fontSize: '1.2rem' }}
            md={{ fontSize: '1.4rem' }}
            lg={{ fontSize: '1.8rem' }}
            className={styles.buttonLabel}
            style={routeStyle}
          >
            { routeName }
          </ResponsiveContainer>
        );
        let menuItemLink = (
          <Link className={allMenuClasses} to={route.pattern}>
            { menuItemContent }
          </Link>
        );
        if (typeof (route.external) !== 'undefined' && route.external) {
          allMenuClasses = `${allMenuClasses} ${styles.menuItemExternal}`;
          menuItemLink = (
            <a className={allMenuClasses} href={route.pattern} target="_blank" rel="noopener noreferrer">
              { menuItemContent }
            </a>
          );
        }
        return (
          <Col xs key={i} style={routeBackgroundStyle} className={styles.menuItem}>
            { menuItemLink }
          </Col>
        );
      }

      return null;
    });

    let allClasses = `${styles.main} ${className}`;
    if (menuOpened) {
      allClasses = `${allClasses} ${styles.menuMainOpen}`;
    }

    return (
      <Row className={allClasses} /* {...remainProps} */>
        {
          menuItems
        }
      </Row>
    );
  }
}

MainMenu.propTypes = {
  dispatch: PropTypes.func.isRequired,
  /* eslint-disable react/no-unused-prop-types */
  intl: PropTypes.shape({
    formatMessage: PropTypes.func,
  }).isRequired,
  menu: PropTypes.shape({
    submenuDisplayed: PropTypes.number,
    linkIdSelected: PropTypes.string,
    pattern: PropTypes.string,
    pathname: PropTypes.string,
  }).isRequired,
  routes: PropTypes.arrayOf(PropTypes.shape({
    routes: PropTypes.arrayOf(PropTypes.shape({
      name: PropTypes.string,
      pattern: PropTypes.string,
    })),
    name: PropTypes.string,
    pattern: PropTypes.string,
    color: PropTypes.string,
  })).isRequired,
  auth: PropTypes.shape({
    accountType: PropTypes.string,
    isFetching: PropTypes.bool,
  }).isRequired,
/* eslint-enable react/no-unused-prop-types */
  className: PropTypes.string,
  location: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ])).isRequired,
};

const mapStateToProps = state => (
  {
    menu: state.menu,
    auth: state.auth,
  }
);

const SubMenuIntl = injectIntl(MainMenu);

const SubMenuIntlConnected = connect(
  mapStateToProps
)(SubMenuIntl);

const SubMenuIntlConnectedWithRouter = withRouter(SubMenuIntlConnected);

export default SubMenuIntlConnectedWithRouter;
