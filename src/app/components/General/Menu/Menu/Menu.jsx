import React, { Component, PropTypes } from 'react/lib/React';

import MainMenu from '../MainMenu/MainMenu';

import styles from './Menu.css';

class Menu extends Component {

  render() {
    const { routes, className = '' } = this.props;

    return (
      <div
        ref={(node) => { this.menuNode = node; }}
        className={`${styles.menu} ${className}`}
      >
        <MainMenu routes={routes} className={styles.main} />
      </div>
    );
  }
}

Menu.propTypes = {
  routes: PropTypes.arrayOf(PropTypes.object).isRequired,
  className: PropTypes.string,
};

export default Menu;
