import React, { PropTypes } from 'react/lib/React';
import { connect } from 'react-redux';

import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';

import { injectIntl } from 'react-intl';

import Link from 'reactjs-beepeers-framework/components/Routing/LinkWithLang/LinkWithLang';

import { WHITE, ORANGE } from '../../../../constants/colors';

import styles from './MoreMenu.css';

const MoreMenu = ({ routes, intl }) => {
  const { formatMessage } = intl;

  const moreMenuItems = [];
  routes.forEach((route) => {
    if (route.moreMenu != null && route.moreMenu && route.routes != null) {
      route.routes.forEach((subRoute) => {
        if (subRoute.name != null && subRoute.pattern != null) {
          const subRouteName = formatMessage({ id: subRoute.name });
          let linkNode = (
            <Link
              to={subRoute.pattern}
              className={styles.link}
            >
              { subRouteName }
            </Link>
          );
          if (subRoute.external != null && subRoute.external) {
            linkNode = (
              <a href={subRoute.pattern} className={styles.link} target="_blank" rel="noopener noreferrer">
                { subRouteName }
              </a>
            );
          }
          moreMenuItems.push((
            <MenuItem key={subRoute.pattern}>
              { linkNode }
            </MenuItem>
          ));
        }
      });
    }
  });

  return (
    <IconMenu
      iconButtonElement={<IconButton><FontIcon color={WHITE} hoverColor={ORANGE} className={`fa fa-bars ${styles.moreMenuIcon}`} /></IconButton>}
      anchorOrigin={{ horizontal: 'left', vertical: 'top' }}
      targetOrigin={{ horizontal: 'left', vertical: 'top' }}
    >
      { moreMenuItems }
    </IconMenu>
  );
};

MoreMenu.propTypes = {
  intl: PropTypes.objectOf(PropTypes.any).isRequired,
  routes: PropTypes.arrayOf(PropTypes.object).isRequired,
};

const MoreMenuIntl = injectIntl(MoreMenu);

const mapStateToProps = state => (
  {
    appConfig: state.appConfig,
    locales: state.locales,
  }
);

const MoreMenuIntlConnected = connect(
  mapStateToProps
)(MoreMenuIntl);

export default MoreMenuIntlConnected;
