import React, { PropTypes } from 'react/lib/React';

import ActorFetcherById from 'reactjs-beepeers-framework/components/Actors/Fetchers/ActorFetcherById/ActorFetcherById';
import ActorCarouselByKey from '../../../General/SliderWithLinks/ActorCarouselByKey/ActorCarouselByKey';

import SchedulingContainer from '../../../SchedulingPage/SchedulingContainer/SchedulingContainer';
import EventConvDescription from '../EventConvDescription/EventConvDescription';

import EventConvDefaultPicture from '../../../../assets/images/event_default.png';
import VideoPlayerDefault from '../../../../assets/images/video_player_default.png';

import styles from './EventConv.css';
import { HOME_HEADERS } from '../../../../constants/actors';

const EventConv = ({ match }) => {
  const { params = {} } = match;
  const renderFunc = (actor) => {
    if (typeof (actor.profileId) !== 'undefined') {
      return (
        <EventConvDescription
          actor={actor}
          defaultPeoplePictureUrl={EventConvDefaultPicture}
          className={styles.eventConvDesc}
          modalClassNameForMedias={styles.eventConvModal}
          enableVideoPlayer
          videoPlayerThumbnail={VideoPlayerDefault}
          videoPlayerEnablePlayPicture={false}
        />
      );
    }

    return null;
  };

  return (
    <SchedulingContainer className={styles.wrapper}>
      <ActorCarouselByKey actorKey={HOME_HEADERS} />
      <ActorFetcherById
        actorId={params.eventId}
        renderFunc={renderFunc}
      />
    </SchedulingContainer>
  );
};

EventConv.propTypes = {
  match: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default EventConv;
