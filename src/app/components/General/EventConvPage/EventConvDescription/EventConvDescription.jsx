import React, { PropTypes } from 'react/lib/React';

import moment from 'moment';

import { injectIntl } from 'react-intl';

import BeeMetadata from 'reactjs-beepeers-framework/components/General/Web/BeeMetadata/BeeMetadata';
import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
import Row from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Row/Row';
import Col from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Col/Col';
import CenterContainer from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/CenterContainer/CenterContainer';
import IconLabel from 'reactjs-beepeers-framework/components/General/Web/IconLabel/IconLabel';
import IconBlock from 'reactjs-beepeers-framework/components/General/Web/IconBlock/IconBlock';
import CoverPicture from 'reactjs-beepeers-framework/components/General/Web/CoverPicture/CoverPicture';
import ContainPicture from 'reactjs-beepeers-framework/components/General/Web/ContainPicture/ContainPicture';
import ResponsiveMediaSlider from 'reactjs-beepeers-framework/components/General/Web/ResponsiveMediaSlider/ResponsiveMediaSlider';
import ResponsiveMediaGallery from 'reactjs-beepeers-framework/components/General/Web/ResponsiveMediaGallery/ResponsiveMediaGallery';
import VideoPlayer from 'reactjs-beepeers-framework/components/General/Web/VideoPlayer/VideoPlayer';
import ActorFetcherById from 'reactjs-beepeers-framework/components/Actors/Fetchers/ActorFetcherById/ActorFetcherById';
import SubscriptionToggleButton from 'reactjs-beepeers-framework/components/Actors/Renderers/Web/SubscriptionToggleButton/SubscriptionToggleButton';
import GoBackButton from 'reactjs-beepeers-framework/components/General/Web/GoBackButton/GoBackButton';

import DefaultVideoPlayerThumbnail from 'reactjs-beepeers-framework/assets/images/video_player_default.jpg';

import {
  HTML_CONTENT,
  PHONE, FAX, EMAIL, WEBSITE,
  FACEBOOK, TWITTER, INSTAGRAM, YOUTUBE,
  SOUNDCLOUD,
  LINKEDIN, OTHER,
} from 'reactjs-beepeers-framework/constants/iconLabelTypes';

import BlueColoredTitle from '../../../General/BlackColoredTitle/BlackColoredTitle';

import flagFR from '../../../../assets/images/flags/fr.gif';
import flagEN from '../../../../assets/images/flags/en.gif';

import styles from './EventConvDescription.css';

const EventConvDescription = (
    {
      actor,
      defaultPeoplePictureUrl,
      galleryRenderForMedias = false,
      className = '',
      modalClassNameForMedias = '',
      enableTitleOnDetailsLinks = false,
      intl,
      enableVideoPlayer = false,
      videoPlayerThumbnail = DefaultVideoPlayerThumbnail,
      videoPlayerEnablePlayPicture = false,
    }
  ) => {
  const {
    profileId,

    originalUrl,
    thumbnailUrl,
    coverUrl,
    displayName,
    name = null,
    description = null,
    description2 = null,

    startDate = null,
    endDate = null,

    locationProfileId = null,

    parentId = null,
    parent2Id = null,

    email = null,
    website = null,
    blog = null,
    phone = null,
    fax = null,

    facebookUrl = null,
    twitterUrl = null,
    instagramUrl = null,
    youtubeUrl = null,

    functionList = null,
  } = actor;

  const { formatMessage } = intl;

  const renderActorDisplayName = (actorToDisplay) => {
    if (typeof (actorToDisplay.profileId) !== 'undefined') {
      return (
        <span>
          { actorToDisplay.displayName }
        </span>
      );
    }
    return null;
  };

  const renderScheduledTime = () => {
    let finalDateString = null;
    if (startDate !== null) {
      const startDateMoment = moment(startDate).utcOffset(0);
      finalDateString = `${startDateMoment.format('ll')} ${startDateMoment.format('LT')}`;
    }
    if (endDate !== null) {
      finalDateString = `${finalDateString} - ${moment(endDate).utcOffset(0).format('LT')}`;
    }
    if (finalDateString !== null) {
      return (
        <span>
          { finalDateString }
        </span>
      );
    }
    return null;
  };

  const renderLanguage = () => {
    const languages = name.split('-');
    const languagesNodes = [];
    languages.forEach((lang) => {
      switch (lang.toUpperCase()) {
        case 'FR':
          languagesNodes.push((
            <div key="FR" className={styles.languageWrapper}>
              <ContainPicture pictureUrl={flagFR} />
            </div>
          ));
          break;
        case 'EN':
          languagesNodes.push((
            <div key="EN" className={styles.languageWrapper}>
              <ContainPicture pictureUrl={flagEN} />
            </div>
          ));
          break;
        default:
          // Nothing to do.
      }
    });
    return languagesNodes;
  };

  const renderRoute = (actorForRoute) => {
    if (typeof (actorForRoute.profileId) !== 'undefined') {
      const routeTitleString = formatMessage({ id: 'CONVENTION.SCHEDULING.EVENT.ROUTE.TITLE' });
      return (
        <div className={styles.route}>
          {routeTitleString} <div className={styles.routeColor} style={{ backgroundColor: actorForRoute.color }} /> {actorForRoute.displayName}
        </div>
      );
    }
    return null;
  };

  const nameNode = (
    <div className={styles.profileName}>
      {displayName}
    </div>
  );

  const datesNode = (
    <div className={styles.dates}>
      <i className="fa fa-clock-o" /> { renderScheduledTime() }
    </div>
  );

  let locationNode = null;
  if (locationProfileId !== null) {
    locationNode = (
      <div className={styles.location}>
        <i className="fa fa-map-marker" /> <ActorFetcherById actorId={locationProfileId} renderFunc={renderActorDisplayName} />
      </div>
    );
  }

  let catNode = null;
  if (parentId !== null) {
    catNode = (
      <div className={styles.cat}>
        <i className="fa fa-tags" /> <ActorFetcherById actorId={parentId} renderFunc={renderActorDisplayName} />
      </div>
    );
  }

  let routeNode = null;
  if (parent2Id !== null) {
    routeNode = (
      <ActorFetcherById actorId={parent2Id} renderFunc={renderRoute} />
    );
  }

  let languageNode = null;
  if (name !== null) {
    languageNode = (
      <div className={styles.language}>
        {renderLanguage()}
      </div>
    );
  }

  let profilePictureNode = (
    <CoverPicture pictureUrl={defaultPeoplePictureUrl} className={styles.profilePicture} />
  );
  if ((typeof (originalUrl) !== 'undefined' && originalUrl !== '')) {
    profilePictureNode = (
      <ContainPicture pictureUrl={`${originalUrl}&size=200&type=png`} className={styles.profilePicture} />
    );
  } else if ((typeof (thumbnailUrl) !== 'undefined' && thumbnailUrl !== '')) {
    profilePictureNode = (
      <ContainPicture pictureUrl={thumbnailUrl} className={styles.profilePicture} />
    );
  }

  const subscriptionToggleButton = (
    <SubscriptionToggleButton
      actorId={profileId}
      className={styles.subscriptionBtn}
    />
  );

  let liveAndReplayNode = null;
  if (website !== null && website.trim() !== '') {
    const liveAndReplayString = formatMessage({ id: 'CONVENTION.SCHEDULING.EVENT.LIVE_AND_REPLAY' });
    liveAndReplayNode = (
      <div className={styles.liveAndReplayWrapper}>
        <a className={styles.liveAndReplayLink} href={website} target="_blank" rel="noopener noreferrer">
          <div className={styles.liveAndReplayContent}>
            <i className="fa fa-play-circle" /> { liveAndReplayString }
          </div>
        </a>
      </div>
    );
  }

  const header = (
    <div className={styles.profileWrapper}>
      <div className={styles.profilePictureWrapper}>
        { profilePictureNode }
      </div>
      <div className={styles.profileNameWrapper}>
        { nameNode }
        { datesNode }
        { locationNode }
        { catNode }
        { routeNode }
        { languageNode }
        { liveAndReplayNode }
      </div>
      { subscriptionToggleButton }
    </div>
  );

  let abstractNode = null;
  if (description !== null && description.trim() !== '') {
    abstractNode = (
      <IconLabel
        className={styles.abstract}
        type={HTML_CONTENT}
        label={description}
      />
    );
  }

  let descriptionNode = null;
  if (description2 !== null && description2.trim() !== '') {
    descriptionNode = (
      <IconLabel
        type={HTML_CONTENT}
        label={description2}
      />
    );
  }

  let phoneNode = null;
  if (phone !== null && phone.trim() !== '') {
    let otherParams = {};
    if (enableTitleOnDetailsLinks) {
      otherParams = {
        title: formatMessage({ id: 'STATIC_CONTENT_VERTICAL.PHONE' }),
      };
    }
    phoneNode = (
      <IconLabel
        type={PHONE}
        label={phone}
        {...otherParams}
      />
    );
  }

  let faxNode = null;
  if (fax !== null && fax.trim() !== '') {
    let otherParams = {};
    if (enableTitleOnDetailsLinks) {
      otherParams = {
        title: formatMessage({ id: 'STATIC_CONTENT_VERTICAL.FAX' }),
      };
    }
    faxNode = (
      <IconLabel
        type={FAX}
        label={fax}
        {...otherParams}
      />
    );
  }

  let emailNode = null;
  if (email !== null && email.trim() !== '') {
    let otherParams = {};
    if (enableTitleOnDetailsLinks) {
      otherParams = {
        title: formatMessage({ id: 'STATIC_CONTENT_VERTICAL.EMAIL' }),
      };
    }
    emailNode = (
      <IconLabel
        type={EMAIL}
        label={email}
        {...otherParams}
      />
    );
  }

  let blogNode = null;
  if (blog !== null && blog.trim() !== '') {
    let otherParams = {};
    if (enableTitleOnDetailsLinks) {
      otherParams = {
        title: formatMessage({ id: 'STATIC_CONTENT_VERTICAL.BLOG' }),
      };
    }
    blogNode = (
      <IconLabel
        type={WEBSITE}
        label={blog}
        {...otherParams}
      />
    );
  }

  let facebookNode = null;
  if (facebookUrl !== null && facebookUrl.trim() !== '') {
    facebookNode = (
      <Col xs={2} sm={1} className={styles.iconBlock}>
        <IconBlock
          type={FACEBOOK}
          label={facebookUrl}
        />
      </Col>
    );
  }

  let twitterNode = null;
  if (twitterUrl !== null && twitterUrl.trim() !== '') {
    twitterNode = (
      <Col xs={2} sm={1} className={styles.iconBlock}>
        <IconBlock
          type={TWITTER}
          label={twitterUrl}
        />
      </Col>
    );
  }

  let instagramNode = null;
  if (instagramUrl !== null && instagramUrl.trim() !== '') {
    instagramNode = (
      <Col xs={2} sm={1} className={styles.iconBlock}>
        <IconBlock
          type={INSTAGRAM}
          label={instagramUrl}
        />
      </Col>
    );
  }

  let youtubeNode = null;
  if (!enableVideoPlayer && youtubeUrl !== null && youtubeUrl.trim() !== '') {
    youtubeNode = (
      <Col xs={2} sm={1} className={styles.iconBlock}>
        <IconBlock
          type={YOUTUBE}
          label={youtubeUrl}
        />
      </Col>
    );
  }

  let youtubeVideoPlayerNode = null;
  let youtubeVideoFunction = false;
  let ytChannelNode = null;
  let soundCloudPlaylistPlayerNode = null;
  const knownFunctions = [];
  const otherFunctions = [];
  if (functionList !== null && functionList.length > 0) {
    functionList.forEach((functionItem) => {
      switch (functionItem.identifier) {
        case 'FB_PAGE':
        case 'TWITTER':
        case 'INSTAGRAM':
          break;
        case 'YOUTUBE_VIDEO': {
          youtubeVideoFunction = true;
          if (enableVideoPlayer) {
            let youtubeVideoUrl = null;
            if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
              functionItem.parameterList.forEach((parameterItem) => {
                if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                  youtubeVideoUrl = parameterItem.value;
                }
              });
            }
            if (youtubeVideoUrl !== null) {
              youtubeVideoPlayerNode = (
                <Row center="xs">
                  <Col xs={10} sm={8}>
                    <VideoPlayer
                      videoThumbnailUrl={videoPlayerThumbnail}
                      disablePlayPictureUrl={!videoPlayerEnablePlayPicture}
                      videoUrl={youtubeVideoUrl}
                    />
                  </Col>
                </Row>
              );
            }
          }
          break;
        }
        case 'SOUNDCLOUD': {
          let soundcloudUrl = null;
          if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
            functionItem.parameterList.forEach((parameterItem) => {
              if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                soundcloudUrl = parameterItem.value;
              }
            });
          }
          if (soundcloudUrl !== null) {
            const soundcloudNode = (
              <Col key="SOUNDCLOUD" xs={2} sm={1} className={styles.iconBlock}>
                <IconBlock
                  type={SOUNDCLOUD}
                  label={soundcloudUrl}
                />
              </Col>
            );
            knownFunctions.push(soundcloudNode);
          }
          break;
        }
        case 'SOUNDCLOUD_PLAYLIST': {
          let soundcloudPlaylistUrl = null;
          if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
            functionItem.parameterList.forEach((parameterItem) => {
              if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                soundcloudPlaylistUrl = parameterItem.value;
              }
            });
          }
          if (soundcloudPlaylistUrl !== null) {
            soundCloudPlaylistPlayerNode = (
              <Row center="xs">
                <Col xs={12} sm={9}>
                  <div className={styles.soundcloudPlaylistPlayerWrapper}>
                    <div className={styles.soundcloudPlaylistPlayerContent}>
                      <iframe
                        frameBorder="0"
                        scrolling="no"
                        marginHeight="0"
                        marginWidth="0"
                        src={soundcloudPlaylistUrl}
                        className={styles.soundcloudPlaylistPlayerIFrameWrapper}
                      />
                    </div>
                  </div>
                </Col>
              </Row>
            );
          }
          break;
        }
        case 'LINKEDIN_PROFILE': {
          let linkedInUrl = null;
          if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
            functionItem.parameterList.forEach((parameterItem) => {
              if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                linkedInUrl = parameterItem.value;
              }
            });
          }
          if (linkedInUrl !== null) {
            const linkedInNode = (
              <Col key="LINKEDIN_PROFILE" xs={2} sm={1} className={styles.iconBlock}>
                <IconBlock
                  type={LINKEDIN}
                  label={linkedInUrl}
                />
              </Col>
            );
            knownFunctions.push(linkedInNode);
          }
          break;
        }
        case 'LINKEDIN_PAGE': {
          let linkedInUrl = null;
          if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
            functionItem.parameterList.forEach((parameterItem) => {
              if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                linkedInUrl = parameterItem.value;
              }
            });
          }
          if (linkedInUrl !== null) {
            const linkedInNode = (
              <Col key="LINKEDIN_PAGE" xs={2} sm={1} className={styles.iconBlock}>
                <IconBlock
                  type={LINKEDIN}
                  label={linkedInUrl}
                />
              </Col>
            );
            knownFunctions.push(linkedInNode);
          }
          break;
        }
        case 'YOUTUBE_CHANNEL': {
          let ytChannelUrl = null;
          if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
            functionItem.parameterList.forEach((parameterItem) => {
              if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                ytChannelUrl = parameterItem.value;
              }
            });
          }
          if (ytChannelUrl !== null) {
            ytChannelNode = (
              <Col key="YOUTUBE_CHANNEL" xs={2} sm={1} className={styles.iconBlock}>
                <IconBlock
                  type={YOUTUBE}
                  label={ytChannelUrl}
                />
              </Col>
            );
            knownFunctions.push(ytChannelNode);
          }
          break;
        }
        case 'WEB': {
          let webUrl = null;
          if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
            functionItem.parameterList.forEach((parameterItem) => {
              if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                webUrl = parameterItem.value;
              }
            });
          }
          if (webUrl !== null) {
            const webNode = (
              <Col key="WEB" xs={2} sm={1} className={styles.iconBlock}>
                <IconBlock
                  type={WEBSITE}
                  label={webUrl}
                />
              </Col>
            );
            knownFunctions.push(webNode);
          }
          break;
        }
        default: {
          if (typeof (functionItem.pictureUrl) !== 'undefined' && functionItem.pictureUrl.trim() !== '') {
            let otherUrl = null;
            if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
              functionItem.parameterList.forEach((parameterItem) => {
                if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                  otherUrl = parameterItem.value;
                }
              });
            }
            if (otherUrl !== null) {
              const otherNode = (
                <Col key={functionItem.identifier} xs={2} sm={1} className={styles.iconBlock}>
                  <IconBlock
                    type={OTHER}
                    label={otherUrl}
                    typePicture={functionItem.pictureUrl}
                  />
                </Col>
              );
              otherFunctions.push(otherNode);
            }
          }
        }
      }
    });
  }
  if (youtubeNode !== null && ytChannelNode !== null && !youtubeVideoFunction) {
    youtubeNode = null;
  }

  let mediasNode = null;
  if (typeof (actor.medias) !== 'undefined') {
    mediasNode = (
      <ResponsiveMediaSlider
        className={styles.sliderWrapper}
        sliderClassName={styles.slider}
        modalClassName={`${styles.modalMedia} ${modalClassNameForMedias}`}
        medias={actor.medias}
        nbToShow={4}
        sliderId={`peopleMediaSlider_${actor.profileId}`}
        xs={{ height: '25vw' }}
        sm={{ height: '89px' }}
        md={{ height: '124px' }}
        lg={{ height: '157px' }}
      />
    );
    if (galleryRenderForMedias) {
      mediasNode = (
        <ResponsiveMediaGallery
          className={styles.mediaGallery}
          modalClassName={`${styles.modalMedia} ${modalClassNameForMedias}`}
          galleryId={`peopleMediaGallery_${actor.profileId}`}
          medias={actor.medias}
          nbPerRows={4}
          xs={{ height: '25vw' }}
          sm={{ height: '89px' }}
          md={{ height: '124px' }}
          lg={{ height: '157px' }}
        />
      );
    }
  }

  let peopleImgUrl = null;
  if ((typeof (coverUrl) !== 'undefined' && coverUrl !== '')) {
    peopleImgUrl = coverUrl;
  } else if ((typeof (thumbnailUrl) !== 'undefined' && thumbnailUrl !== '')) {
    peopleImgUrl = thumbnailUrl;
  }

  const detailsLinks = (
    <div className={styles.details}>
      { emailNode }
      { blogNode }
      { phoneNode }
      { faxNode }
    </div>
  );

  const detailsBlocks = (
    <FullRow>
      <Row center="xs">
        { facebookNode }
        { twitterNode }
        { instagramNode }
        { youtubeNode }
        { knownFunctions }
      </Row>
      <Row center="xs">
        { otherFunctions }
      </Row>
    </FullRow>
  );

  const detailsNode = (
    <FullRow>
      { detailsLinks }
      { youtubeVideoPlayerNode }
      { soundCloudPlaylistPlayerNode }
      { detailsBlocks }
    </FullRow>
  );

  let organizeBy = null;
  let contributorsList = null;
  let artistsShowcaseList = null;

  const organizers = [];
  const contributors = [];
  const artistsShowcase = [];

  if (organizers.length > 0) {
    organizeBy = (
      <BlueColoredTitle
        title="CONVENTION.SCHEDULING.EVENT.ORGANIZE_BY.TITLE"
        enableGrid={false}
      >
        { organizers }
      </BlueColoredTitle>
    );
  }

  if (contributors.length > 0) {
    contributorsList = (
      <BlueColoredTitle
        title="CONVENTION.SCHEDULING.EVENT.CONTRIBUTORS.TITLE"
        enableGrid={false}
      >
        { contributors }
      </BlueColoredTitle>
    );
  }

  if (artistsShowcase.length > 0) {
    artistsShowcaseList = (
      <BlueColoredTitle
        title="CONVENTION.SCHEDULING.EVENT.ARTISTS.TITLE"
        enableGrid={false}
      >
        { artistsShowcase }
      </BlueColoredTitle>
    );
  }

  const backString = formatMessage({ id: 'SHARED.NEWS.BACK' });

  return (
    <div className={`${styles.wrapper} ${className}`}>
      <div className={styles.backLink}>
        <GoBackButton>
          <i className="fa fa-arrow-circle-left" /> { backString }
        </GoBackButton>
      </div>
      <BeeMetadata title={displayName} path={`/profile/${actor.profileId}`} imgUrl={peopleImgUrl} />
      { header }
      <div className={styles.infos}>
        { abstractNode }
        { descriptionNode }
      </div>
      { contributorsList }
      { artistsShowcaseList }
      { organizeBy }
      <CenterContainer className={styles.detailsContainer} width={10}>
        { detailsNode }
        { mediasNode }
      </CenterContainer>
    </div>
  );
};

EventConvDescription.propTypes = {
  actor: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.bool,
    PropTypes.number,
    PropTypes.object,
    PropTypes.string,
  ])).isRequired,
  defaultPeoplePictureUrl: PropTypes.string.isRequired,
  className: PropTypes.string,
  galleryRenderForMedias: PropTypes.bool,
  modalClassNameForMedias: PropTypes.string,
  enableTitleOnDetailsLinks: PropTypes.bool,
  intl: PropTypes.objectOf(PropTypes.any).isRequired,
  enableVideoPlayer: PropTypes.bool,
  videoPlayerThumbnail: PropTypes.string,
  videoPlayerEnablePlayPicture: PropTypes.bool,
};

const PeopleDescriptionIntl = injectIntl(EventConvDescription);

export default PeopleDescriptionIntl;
