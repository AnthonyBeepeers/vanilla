import React, { PropTypes } from 'react/lib/React';

// import ContainPicture from 'reactjs-beepeers-framework/components/General/Web/ContainPictureCover/ContainPictureCover';
// import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
// import FullLineTitle from 'reactjs-beepeers-framework/components/General/Web/FullLineTitle/FullLineTitle';
import ReactHtmlParser from 'react-html-parser';

import styles from './LinkTicket.css';

/* eslint-disable new-cap*/

const LinkedTicket = ({ partner }) => {
  console.log(partner);
  return (
    <div className={styles.whole}>
      <div className={styles.container}>
        <div
          style={{ backgroundImage: `url(${partner.originalUrl})` }}
          className={styles.img}
        />
        <div className={styles.text}>
          {ReactHtmlParser(partner.description)}
        </div>

      </div>
    </div>
  ); };

LinkedTicket.propTypes = {
  partner: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default LinkedTicket;
