import React, { PropTypes } from 'react/lib/React';


import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
import ActorFetcherById from 'reactjs-beepeers-framework/components/Actors/Fetchers/ActorFetcherById/ActorFetcherById';
import ContainPicture from 'reactjs-beepeers-framework/components/General/Web/ContainPicture/ContainPicture';
// import SubscriptionToggleButton from 'reactjs-beepeers-framework/components/Actors/Renderers/Web/SubscriptionToggleButton/SubscriptionToggleButton';
// import SubscriptionContainer from 'reactjs-beepeers-framework/components/General/Web/SubscriptionContainer/SubscriptionContainer';

import Link from 'reactjs-beepeers-framework/components/Routing/LinkWithLang/LinkWithLang';

import DefaultEventPicture from '../../../assets/images/event_default.png';

import styles from './LinkedEvent.css';

const LinkedEvent = ({ event }) => {
  const renderActorDisplayName = (actor) => {
    console.log('0 messages dans nos propos');
    console.log(actor);
    if (typeof (actor.profileId) !== 'undefined') {
      return (
        <span>
          { actor.displayName }
        </span>
      );
    }
    return null;
  };

  let pictureNode = (
    <img
      className={styles.contain}
      role={'presentation'}
      src={DefaultEventPicture}
    />
  );
  if (typeof (event.originalUrl) !== 'undefined' && event.originalUrl.trim() !== '') {
    pictureNode = (
      <ContainPicture
        pictureUrl={`${event.originalUrl}&size=250&type=png`}
      />
    );
  } else if (typeof (event.thumbnailUrl) !== 'undefined' && event.thumbnailUrl.trim() !== '') {
    const toDelete = '&format=profile&device=web';
    const originalUrl = event.thumbnailUrl.substring(0, event.thumbnailUrl.length - toDelete.length);
    pictureNode = (
      <ContainPicture
        pictureUrl={`${originalUrl}&size=250&type=png`}
      />
    );
  } else if (typeof (event.coverUrl) !== 'undefined' && event.coverUrl.trim() !== '') {
    pictureNode = (
      <ContainPicture
        pictureUrl={`${event.coverUrl}&size=250&type=png`}
      />
    );
  }

  const eventName = encodeURIComponent(event.displayName.replace(/\./g, '&#46;'));

  const linkedEventNode = (
    <Link to={`/event/${event.profileId}/${eventName}`} className={styles.wrapper}>
      <div className={styles.flexContainer}>
        <div className={styles.eventPicture}>
          { pictureNode }
        </div>
        <div className={styles.eventDescription}>
          <div className={styles.mobileEventWrapper}>
            <FullRow className={styles.mobileEventName}>
              {event.displayName}
            </FullRow>
            {
              typeof (event.locationProfileId) !== 'undefined' ? (
                <FullRow>
                  <div className={styles.eventDetail}>
                    <i className="fa fa-map-marker" /> <ActorFetcherById actorId={event.locationProfileId} renderFunc={renderActorDisplayName} />
                  </div>
                </FullRow>
              ) : null
            }
            {
              typeof (event.parentId) !== 'undefined' ? (
                <FullRow>
                  <div className={styles.eventDetail}>
                    <ActorFetcherById actorId={event.parentId} renderFunc={renderActorDisplayName} />
                  </div>
                </FullRow>
                ) : null
            }
          </div>
        </div>
      </div>
      <div className={styles.hoverContainer}>
        &nbsp;
      </div>
    </Link>
  );

  return linkedEventNode;
/*
  const subscriptionToggleButton = (
    <SubscriptionToggleButton
      actorId={event.profileId}
      className={styles.subscriptionBtn}
    />
  );

  return (
    <SubscriptionContainer
      subscriptionToggleButton={subscriptionToggleButton}
      className={styles.subscriptionContainer}
    >
      { linkedEventNode }
    </SubscriptionContainer>
  );
  */
};

LinkedEvent.propTypes = {
  event: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default LinkedEvent;
