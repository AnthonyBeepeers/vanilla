import React from 'react/lib/React';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';

import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';

import ActorCarouselByKey from 'reactjs-beepeers-framework/components/Actors/Renderers/Web/ActorCarouselByKey/ActorCarouselByKey';

import HoloColoredTitle from '../../General/HoloColoredTitle/HoloColoredTitle';

import { HOME_HEADER_KEY } from '../../../constants/actors';

import styles from './NotFound.css';

const NotFound = () => (
  <FullRow className={styles.wrapper} >
    <ActorCarouselByKey actorKey={HOME_HEADER_KEY} />
    <HoloColoredTitle
      title="404.message"
      enableGrid
      className={styles.wrapper}
    >
      <div className={styles.downloadlinkWrapper}>
        <Link className={styles.downloadlinkLink} to={'/'}>
          <div className={styles.downloadlinkContent}>
            <i className="fa fa-arrow-circle-o-left" /> <FormattedMessage id="404.return" />
          </div>
        </Link>
      </div>
    </HoloColoredTitle>
  </FullRow>
);

export default NotFound;
