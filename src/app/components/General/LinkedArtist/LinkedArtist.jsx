import React, { PropTypes } from 'react/lib/React';

import { FormattedMessage } from 'react-intl';

import ContainPicture from 'reactjs-beepeers-framework/components/General/Web/ContainPicture/ContainPicture';

import Link from 'reactjs-beepeers-framework/components/Routing/LinkWithLang/LinkWithLang';
import SubscriptionToggleButton from 'reactjs-beepeers-framework/components/Actors/Renderers/Web/SubscriptionToggleButton/SubscriptionToggleButton';
import SubscriptionContainer from 'reactjs-beepeers-framework/components/General/Web/SubscriptionContainer/SubscriptionContainer';

import styles from './LinkedArtist.css';

const LinkedArtist = ({ artist, pictureUrl }) => {
  let artistNameUrl = artist.displayName.replace(/ /g, '-');
  artistNameUrl = encodeURIComponent(artistNameUrl.replace(/\./g, '&#46;'));

  const linkedArtistNode = (
    <Link to={`/artist/${artist.profileId}/${artistNameUrl}`} className={styles.wrapper}>
      <div className={styles.pictureWrapper}>
        <div className={styles.pictureContent}>
          <ContainPicture pictureUrl={pictureUrl} />
          <div className={styles.moreInfo}>
            <FormattedMessage id={'BUTTON.MORE_INFOS'} />
          </div>
        </div>
      </div>
      <div className={styles.contentWrapper}>
        <div className={styles.artistName}>
          { artist.displayName }
        </div>
        <div className={styles.artistActivity}>
          { artist.name }
        </div>
      </div>
    </Link>
  );

  const subscriptionToggleButton = (
    <SubscriptionToggleButton
      actorId={artist.profileId}
      className={styles.subscriptionBtn}
    />
  );

  return (
    <SubscriptionContainer
      subscriptionToggleButton={subscriptionToggleButton}
      className={styles.subscriptionContainer}
    >
      { linkedArtistNode }
    </SubscriptionContainer>
  );
};

LinkedArtist.propTypes = {
  artist: PropTypes.objectOf(PropTypes.any).isRequired,
  pictureUrl: PropTypes.string.isRequired,
};

export default LinkedArtist;
