import React, { PropTypes } from 'react/lib/React';
import { connect } from 'react-redux';

import Grid from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Grid/Grid';
import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
import ActorsListFetcher from 'reactjs-beepeers-framework/components/Actors/Fetchers/ActorsListFetcher/ActorsListFetcher';
import FullLineTitle from 'reactjs-beepeers-framework/components/General/Web/FullLineTitle/FullLineTitle';
import ResponsiveContainer from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/ResponsiveContainer/ResponsiveContainer';
import ActorsSlider from 'reactjs-beepeers-framework/components/Actors/Renderers/Web/ActorsSlider/ActorsSlider';

import LinkedTicket from '../../General/LinkedTicket/LinkedTicket';

import { LINKS } from '../../../constants/actors';

import styles from './Tmp.css';
// import HoloColoredTitle from '../HoloColoredTitle/HoloColoredTitle';

const Partners = ({ className = '' }) => {
  const renderFunc = (partnersSections) => {
    const renderPartnerFunc = (partner) => {
      if (typeof (partner.originalUrl) !== 'undefined' && typeof (partner.website) !== 'undefined') {
        return (<LinkedTicket partner={partner} pictureUrl={`${partner.originalUrl}&type=png&width=800&height=800`} />);
      }

      return null;
    };

    const responsivePartnersSliderSettings = {
      xs: {
        slidesToShow: 4,
        slidesToScroll: 4,
        displayArrows: false,
        autoplay: true,
        displayDots: false,
        infinite: true,
        variableWidth: false,
        autoplaySpeed: 7000,
      },
      sm: {
        slidesToShow: 5,
        slidesToScroll: 5,
        displayArrows: false,
        autoplay: true,
        displayDots: false,
        infinite: true,
        variableWidth: false,
        autoplaySpeed: 7000,
      },
      md: {
        slidesToShow: 6,
        slidesToScroll: 6,
        displayArrows: false,
        autoplay: true,
        displayDots: false,
        infinite: true,
        variableWidth: false,
        autoplaySpeed: 7000,
      },
      lg: {
        slidesToShow: 7,
        slidesToScroll: 7,
        displayArrows: false,
        autoplay: true,
        displayDots: false,
        infinite: true,
        variableWidth: false,
        autoplaySpeed: 7000,
      },
    };

    const partnersNodes = partnersSections.map((partnersSection, i) => (
      <div key={i}>
        <FullLineTitle title={partnersSection.name} intlActivated={false} className={styles.sectionTitle} />
        <ResponsiveContainer
          xs={{ height: '25vw' }}
          sm={{ height: '180px' }}
          md={{ height: '235px' }}
          lg={{ height: '835px' }}
        >
          <ActorsSlider
            actors={partnersSection.items}
            renderFunc={renderPartnerFunc}
            responsiveSettings={responsivePartnersSliderSettings}
          />
        </ResponsiveContainer>
      </div>
      ));

    return (
      <FullRow>
        <div className={className}>
          {partnersNodes}
        </div>
      </FullRow>
    );
  };

  return (
    <Grid className={styles.wrapper}>
      <ActorsListFetcher
        actorsListKey={LINKS}
        renderFunc={renderFunc}
      />
    </Grid>
  );
};

Partners.propTypes = {
  className: PropTypes.string,
};

const mapStateToProps = state => ({
  locales: state.locales,
});

const PartnersConnected = connect(
  mapStateToProps
)(Partners);

export default PartnersConnected;
