import React, { Component, PropTypes } from 'react/lib/React';
import { connect } from 'react-redux';

import { injectIntl } from 'react-intl';

import { withRouter } from 'react-router-dom';

import RaisedButton from 'material-ui/RaisedButton';
import FontIcon from 'material-ui/FontIcon';
import Drawer from 'material-ui/Drawer';
import { List, ListItem } from 'material-ui/List';

import Row from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Row/Row';
import Col from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Col/Col';
// import FrEnUrlSwitcher from 'reactjs-beepeers-framework/components/General/Web/FrEnUrlSwitcher/FrEnUrlSwitcher';

import { openDrawerMenu, closeDrawerMenu } from 'reactjs-beepeers-framework/actions/menu';

import Link from 'reactjs-beepeers-framework/components/Routing/LinkWithLang/LinkWithLang';
import FrEnUrlSwitcher from 'reactjs-beepeers-framework/components/General/Web/FrEnUrlSwitcher/FrEnUrlSwitcher';

import LogoHeader from '../Header/LogoHeader/LogoHeader';
import PlayButton from '../Header/PlayButton/PlayButton';
import AccountProfileButton from '../Header/AccountProfileButton/AccountProfileButton';

import * as COLORS from '../../../constants/colors';

import styles from './DrawerMenu.css';

class DrawerMenu extends Component {
  render() {
    const { dispatch, menu, intl, routes = [], location, className = '' } = this.props;
    const { formatMessage } = intl;
    const { drawerMenuOpen } = menu;

    const switchDrawerMenu = (open) => {
      if (open) {
        dispatch(openDrawerMenu());
      } else {
        dispatch(closeDrawerMenu());
      }
    };

    let menuItems = null;
    if (routes.length > 0) {
      menuItems = routes.map((route, routeId) => {
        if (typeof (route.name) !== 'undefined' && typeof (route.pattern) !== 'undefined' && route.pattern !== '') {
          const routeName = formatMessage({ id: route.name });

          let routeBackgroundStyle = {};
          if (typeof (route.color) !== 'undefined') {
            routeBackgroundStyle = {
              backgroundColor: route.color,
            };
          }

          let routeStyle = {};
          if (typeof (route.textColor) !== 'undefined') {
            routeStyle = {
              color: route.textColor,
            };
          }

          let linkClassName = styles.submenuLink;
          let selected = false;

          if (typeof (route.menuSelectedPatterns) !== 'undefined' && route.menuSelectedPatterns.length > 0) {
            route.menuSelectedPatterns.forEach((selPattern) => {
              if (location.pathname.startsWith(selPattern) || location.pathname.substring(3, location.pathname.length).startsWith(selPattern)) {
                selected = true;
              }
            });
          }
          if (!selected) {
            if (location.pathname === '/' || route.pattern === '/' || location.pathname.substring(3, location.pathname.length) === '/') {
              if (location.pathname === route.pattern || location.pathname.substring(3, location.pathname.length) === route.pattern) {
                selected = true;
              }
            } else if (location.pathname.startsWith(route.pattern) || location.pathname.substring(3, location.pathname.length).startsWith(route.pattern)) {
              selected = true;
            }
          }

          if (selected) {
            linkClassName = `${linkClassName} ${styles.submenuLinkSelected}`;
          }

          let linkNode = (
            <Link
              to={route.pattern}
              className={linkClassName}
              onClick={() => switchDrawerMenu(false)}
            >
              <div className={styles.submenuLinkLabel}>
                { routeName }
              </div>
            </Link>
          );
          if (typeof (route.external) !== 'undefined' && route.external) {
            linkNode = (
              <a
                href={route.pattern}
                className={linkClassName}
                onClick={() => switchDrawerMenu(false)}
                target="_blank" rel="noopener noreferrer"
              >
                <div className={styles.submenuLinkLabel}>
                  { routeName }
                </div>
              </a>
            );
          }

          return (
            <ListItem
              key={routeId}
              style={routeBackgroundStyle}
              innerDivStyle={routeStyle}
              className={styles.menuItem}
            >
              <Row className={styles.menuItemContainer}>
                <Col xs={12}>
                  { linkNode }
                </Col>
              </Row>
            </ListItem>
          );
        }

        if (typeof (route.moreMenu) !== 'undefined' && route.moreMenu && typeof (route.routes) !== 'undefined' && route.routes.length > 0) {
          const menuMoreRoutes = [];
          route.routes.forEach((subroute, subRouteIndex) => {
            if (typeof (subroute.name) !== 'undefined' && typeof (subroute.pattern) !== 'undefined' && subroute.pattern !== '' && subroute.pattern !== '/') {
              const routeName = formatMessage({ id: subroute.name });

              let routeBackgroundStyle = {};
              if (typeof (subroute.color) !== 'undefined') {
                routeBackgroundStyle = {
                  backgroundColor: subroute.color,
                };
              }

              let routeStyle = {};
              if (typeof (subroute.textColor) !== 'undefined') {
                routeStyle = {
                  color: subroute.textColor,
                };
              }

              let linkClassName = styles.submenuLink;
              let selected = false;

              if (typeof (route.menuSelectedPatterns) !== 'undefined' && route.menuSelectedPatterns.length > 0) {
                route.menuSelectedPatterns.forEach((selPattern) => {
                  if (location.pathname.startsWith(selPattern) || location.pathname.substring(3, location.pathname.length).startsWith(selPattern)) {
                    selected = true;
                  }
                });
              }
              if (!selected) {
                if (location.pathname === '/' || subroute.pattern === '/' || location.pathname.substring(3, location.pathname.length) === '/') {
                  if (location.pathname === subroute.pattern || location.pathname.substring(3, location.pathname.length) === subroute.pattern) {
                    selected = true;
                  }
                } else if (location.pathname.startsWith(subroute.pattern) || location.pathname.substring(3, location.pathname.length).startsWith(subroute.pattern)) {
                  selected = true;
                }
              }

              if (selected) {
                linkClassName = `${linkClassName} ${styles.submenuLinkSelected}`;
              }

              let linkNode = (
                <Link
                  to={subroute.pattern}
                  className={linkClassName}
                  onClick={() => switchDrawerMenu(false)}
                >
                  <div className={styles.submenuLinkLabel}>
                    { routeName }
                  </div>
                </Link>
              );
              if (typeof (subroute.external) !== 'undefined' && subroute.external) {
                linkNode = (
                  <a
                    href={subroute.pattern}
                    className={linkClassName}
                    onClick={() => switchDrawerMenu(false)}
                    target="_blank" rel="noopener noreferrer"
                  >
                    <div className={styles.submenuLinkLabel}>
                      { routeName }
                    </div>
                  </a>
                );
              }

              menuMoreRoutes.push((
                <ListItem
                  key={`${routeId}_${subRouteIndex}`}
                  style={routeBackgroundStyle}
                  innerDivStyle={routeStyle}
                  className={styles.menuItem}
                >
                  <Row className={styles.menuItemContainer}>
                    <Col xs={12}>
                      { linkNode }
                    </Col>
                  </Row>
                </ListItem>
              ));
            }
          });
          if (menuMoreRoutes.length > 0) {
            return menuMoreRoutes;
          }
          return null;
        }

        return null;
      });
    }

    return (
      <div className={`${styles.wrapper} ${className}`}>
        <RaisedButton
          onTouchTap={() => switchDrawerMenu(true)}
          icon={<FontIcon color={COLORS.BLACK} className="fa fa-bars" />}
          style={{ minWidth: '100%', height: '100%' }}
        />
        <Drawer
          docked={false}
          width={250}
          open={drawerMenuOpen}
          onRequestChange={open => switchDrawerMenu(open)}
          containerClassName={styles.drawerMenu}
        >
          <div className={styles.logo}>
            <LogoHeader onTouchTap={() => switchDrawerMenu(false)} />
          </div>
          {/* <Row>
            <Col xs={12}>
              <FrEnUrlSwitcher className={styles.langSwitcher} />
            </Col>
          </Row>*/}
          {/*          <FullRow className={styles.subSitesLinks}>
            <SubSitesLinks onClick={() => switchDrawerMenu(false)} />
          </FullRow>*/}
          <List>
            { menuItems }
          </List>
          <Row>
            <Col xs={6} className={styles.logoMini}>
              <PlayButton />
            </Col>
            <Col xs={6} className={styles.logoMini} >
              <AccountProfileButton onClick={() => switchDrawerMenu(false)} />
            </Col>
          </Row>
          <Row>
            <Col xs={12} className={styles.logoMini}>
              <FrEnUrlSwitcher className={styles.langSwitcher} />
            </Col>
          </Row>
        </Drawer>
      </div>
    );
  }
}

DrawerMenu.propTypes = {
  dispatch: PropTypes.func.isRequired,
/* eslint-disable react/no-unused-prop-types */
  intl: PropTypes.shape({
    formatMessage: PropTypes.func,
  }).isRequired,
  menu: PropTypes.shape({
    drawerMenuOpen: PropTypes.bool,
  }).isRequired,
/* eslint-enable react/no-unused-prop-types */
  routes: PropTypes.arrayOf(PropTypes.object),
  className: PropTypes.string,
  location: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ])).isRequired,
};

const DrawerMenuIntl = injectIntl(DrawerMenu);

const mapStateToProps = state => (
  {
    menu: state.menu,
  }
);

const DrawerMenuIntlConnected = connect(
  mapStateToProps
)(DrawerMenuIntl);

const DrawerMenuIntlConnectedWithRouter = withRouter(DrawerMenuIntlConnected);

export default DrawerMenuIntlConnectedWithRouter;
