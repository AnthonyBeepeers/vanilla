import React, { PropTypes } from 'react/lib/React';
import { connect } from 'react-redux';

import ActorFetcherByKey from 'reactjs-beepeers-framework/components/Actors/Fetchers/ActorFetcherByKey/ActorFetcherByKey';
import Col from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Col/Col';
import Row from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Row/Row';
// import FullLineTitle from 'reactjs-beepeers-framework/components/General/Web/FullLineTitle/FullLineTitle';
import Grid from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Grid/Grid';
// import { FormattedMessage } from 'react-intl';

import { CONTACT_INFOS_KEY } from '../../../constants/actors';

// import DownloadMobileLogo from '../../../assets/images/mobileAppDownloadButtons/appDownload.png';

import styles from './Footer.css';

const Footer = ({ appConfig, locales }) => {
  const { lang } = locales;
  const langWithoutRegionCode = lang.toLowerCase().split(/[_-]+/)[0];

  let sentence = 'If you have any query, please contact : ';
  let may = 'or';
  if (langWithoutRegionCode === 'fr') {
    sentence = 'Pour toutes demandes, contactez : ';
    may = 'ou';
  }
  const renderFunc = (actor) => {
    const { email = null, location = null, phone = null } = actor;
    const addressNodes = [];
    const contactNodes = [];

    if (location !== null) {
      const { address = null, city = null, postalCode = null } = location;
      if (address !== null && address !== '') {
        addressNodes.push(
          <div className={styles.contactItem} key="FOOTER_ADDRESS">
            {address}
          </div>);
      }
      if (city !== null && city !== '' && postalCode !== null && postalCode !== '') {
        addressNodes.push(<div className={styles.contactItem} key="FOOTER_CITY">{` ${postalCode} ${city}`}</div>);
      } else {
        if (city !== null && city !== '') {
          addressNodes.push(<div className={styles.contactItem} key="FOOTER_CITY">{city}</div>);
        }
        if (postalCode !== null && postalCode !== '') {
          addressNodes.push(<div className={styles.contactItem} key="FOOTER_POSTAL_CODE">{postalCode}</div>);
        }
      }
    }
    if (phone !== null && phone !== '') {
      contactNodes.push(<div className={styles.contactItem} key="FOOTER_PHONE"><a href={`tel:${phone}`}>{phone}</a></div>);
    }
    if (email !== null && email !== '') {
      contactNodes.push(<div className={styles.contactItem} key="FOOTER_EMAIL"><a href={`mailto:${email}`}>{email}</a></div>);
    }

    // let applicationZoneNode = null;

    if (typeof (appConfig.applicationKey) !== 'undefined' && appConfig.applicationKey !== '') {
      /* applicationZoneNode = (
        <div>
          <FullLineTitle className={styles.blockTitle} title="FOOTER.MOBILE_DOWNLOAD_TITLE" intlActivated />
          <div className={styles.linkContainer}>
            <a
              href={`http://bprs.it/apps/${appConfig.applicationKey}`}
              className={styles.downloadLink}
              target="_blank"
              rel="noopener noreferrer"
            >
              <img src={DownloadMobileLogo} alt="Download mobile" />
            </a>
          </div>
        </div>
      ); */
    }

    return (
      <div className={styles.wrapper}>
        <Grid >
          <Row className={styles.footerContent}>
            <Col xs={12} sm={12} className={styles.contactZone}>
              {sentence}
              <div className={styles.contactItem} key="FOOTER_EMAIL1"><a href={'mailto:emeallares@altrad.com'}>{'emeallares@altrad.com'}</a></div>
              {may}
              <div className={styles.contactItem} key="FOOTER_EMAIL2"><a href={'mailto:antoine.martinez@prezioso-linjebygg.com'}>{'antoine.martinez@prezioso-linjebygg.com'}</a></div>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  };

  return (<ActorFetcherByKey actorKey={CONTACT_INFOS_KEY} renderFunc={renderFunc} />);
};

Footer.propTypes = {
  appConfig: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.string,
    PropTypes.array,
    PropTypes.object,
    PropTypes.number,
  ])).isRequired,
  locales: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ])).isRequired,
};

const mapStateToProps = state => ({
  appConfig: state.appConfig,
  locales: state.locales,
});

const FooterListConnected = connect(
  mapStateToProps
)(Footer);

export default FooterListConnected;
