import React, { PropTypes } from 'react/lib/React';

import ResponsiveContainer from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/ResponsiveContainer/ResponsiveContainer';
// import ActorFetcherByKey from 'reactjs-beepeers-framework/components/Actors/Fetchers/ActorFetcherByKey/ActorFetcherByKey';

import CoverPicture from 'reactjs-beepeers-framework/components/General/Web/CoverPicture/CoverPicture';
import FullLineCarousel from 'reactjs-beepeers-framework/components/General/Web/FullLineCarousel/FullLineCarousel';
import { COVER_RATIO } from 'reactjs-beepeers-framework/constants/beepeersConfig';
import ActorsListFetcher from 'reactjs-beepeers-framework/components/Actors/Fetchers/ActorsListFetcher/ActorsListFetcher';
// import ActorMediasCarousel from '../../General/ActorMediasCarousel/ActorMediasCarousel';

import styles from './ActorCarouselByKey.css';
/* eslint-disable no-else-return */
/* eslint-disable no-unreachable */
const ActorCarouselByKey = ({ actorKey, fullWidth = true }) => {
  const renderFunc = (actor) => {
    console.log('ACTORR');
    console.log(actor);
    if (typeof actor !== 'undefined' && typeof actor[0] !== 'undefined' && typeof actor[0].items !== 'undefined' && actor[0].items.length > 0) {
      const pictures = actor[0].items.map((media, index) => {
        if (typeof (media.originalUrl) !== 'undefined' && media.originalUrl !== '' && media.website !== '') {
          return (
            <a href={media.website} target="_blank" rel="noopener noreferrer">
              <CoverPicture key={index} pictureUrl={media.originalUrl} />
            </a>
          );
        } else {
          return (
            <CoverPicture key={index} pictureUrl={media.originalUrl} />
          );
        }
        return null;
      });
      if (fullWidth) {
        const fullWidthResp = { height: `${85 / COVER_RATIO}vw` };
        return (
          <ResponsiveContainer
            className={styles.slider}
            xs={fullWidthResp}
            sm={fullWidthResp}
            md={fullWidthResp}
            lg={fullWidthResp}
          >
            <FullLineCarousel >
              { pictures }
            </FullLineCarousel>
          </ResponsiveContainer>
        );
      }
      return (
        <ResponsiveContainer
          className={styles.slider}
          xs={{ height: '100vw' }}
          sm={{ height: `${768 / COVER_RATIO}px` }}
          md={{ height: `${992 / COVER_RATIO}px` }}
          lg={{ height: `${1200 / COVER_RATIO}px` }}
        >
          <FullLineCarousel>
            { pictures }
          </FullLineCarousel>
        </ResponsiveContainer>
      );
    }
    return null;
  };

  return (
    <ActorsListFetcher
      actorsListKey={actorKey}
      renderFunc={renderFunc}
    />
  );
};

ActorCarouselByKey.propTypes = {
  actorKey: PropTypes.string.isRequired,
  fullWidth: PropTypes.bool,
};

export default ActorCarouselByKey;
