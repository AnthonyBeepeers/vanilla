import React, { PropTypes } from 'react/lib/React';

import CoverPicture from 'reactjs-beepeers-framework/components/General/Web/CoverPicture/CoverPicture';
import FullLineCarousel from 'reactjs-beepeers-framework/components/General/Web/FullLineCarousel/FullLineCarousel';

const ActorMediasCarousel = ({ actor, className = '' }) => {
  if (typeof (actor.medias) !== 'undefined' && actor.medias.length > 0) {
    if (actor.medias.length > 1) {
      const pictures = actor.medias.map((media, index) => {
        if (typeof (media.url) !== 'undefined' && media.url !== '') {
          return (
            <CoverPicture key={index} pictureUrl={media.url} />
          );
        }
        return null;
      });

      return (
        <FullLineCarousel className={className}>
          { pictures }
        </FullLineCarousel>
      );
    }

    return (
      <CoverPicture pictureUrl={actor.medias[0].url} wrapperClassName={className} />
    );
  }

  return null;
};

ActorMediasCarousel.propTypes = {
  actor: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.bool,
    PropTypes.number,
    PropTypes.object,
    PropTypes.string,
  ])).isRequired,
  className: PropTypes.string,
};

export default ActorMediasCarousel;
