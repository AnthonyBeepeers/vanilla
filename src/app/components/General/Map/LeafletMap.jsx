import React, { PropTypes } from 'react/lib/React';

import { Map, Marker, TileLayer, Tooltip } from 'react-leaflet';
import Leaflet from 'leaflet';
import 'leaflet/dist/leaflet.css';
import styles from './LeafletMap.css';

const LeafletMap = ({ zoom = 15, showMarker = true, className = '', markerClassName = '' }) => {
  const position = [43.609121, 3.894113];
  const position2 = [43.609408, 3.885490];
  const position3 = [43.609854, 3.894618];
  const position4 = [43.612724, 3.88205];

  let markerNode = null;
  let markerNode2 = null;
  let markerNode3 = null;
  let markerNode4 = null;

  if (showMarker) {
    const markerIcon = Leaflet.divIcon({ className: `fa fa-map-marker ${styles.marker} ${markerClassName}` });
    markerNode = (
      <Marker position={position} icon={markerIcon} >
        <Tooltip className={styles.tooltip} permanent direction={'right'}>
          <div>
        Hotel Mercure Centre Antigone
          </div>
        </Tooltip>
      </Marker>
  );
    markerNode3 = (
      <Marker position={position3} icon={markerIcon} >
        <Tooltip className={styles.tooltip} permanent direction={'right'}>
          <div>
           Hôtel Novotel Suites Montpellier
          </div>
        </Tooltip>
      </Marker>
  );

    markerNode4 = (
      <Marker position={position4} icon={markerIcon} >
        <Tooltip className={styles.tooltip} permanent direction={'right'}>
          <div>
       Le Corum
          </div>
        </Tooltip>
      </Marker>
   );


    markerNode2 = (
      <Marker position={position2} icon={markerIcon} >
        <Tooltip className={styles.tooltip} permanent direction={'left'}>
          <div>
       Hotel Mercure Centre Comédie
          </div>
        </Tooltip>
      </Marker>
   );
  }

  return (
    <div className={`${styles.wrapper} ${className}`}>
      <div className={styles.mapWrapper}>
        <div className={styles.mapContent}>
          <Map scrollWheelZoom={false} center={position} zoom={zoom} className={styles.map}>
            <TileLayer
              attribution="&copy; <a href='http://osm.org/copyright'>OpenStreetMap</a> contributors"
              url="https://external-cdg2-1.xx.fbcdn.net/map_tile.php?v=28&amp;osm_provider=3&amp;x={x}&amp;y={y}&amp;z={z}&amp;language=fr_FR"
            />
            { markerNode }
            { markerNode2 }
            { markerNode3 }
            { markerNode4 }
          </Map>
        </div>
      </div>
    </div>
  );
};

LeafletMap.propTypes = {
  className: PropTypes.string,
  zoom: PropTypes.number,
  showMarker: PropTypes.bool,
  markerClassName: PropTypes.string,
};

export default LeafletMap;
