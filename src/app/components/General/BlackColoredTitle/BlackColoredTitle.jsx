import React, { PropTypes } from 'react/lib/React';

import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
import Grid from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Grid/Grid';

import ColoredTitle from '../ColoredTitle/ColoredTitle';

import Background from '../../../assets/images/Shared/background.jpg';

import styles from './BlackColoredTitle.css';

const BlackColoredTitle = ({ title, children, intlActivated = true, className = '', gridClassName = '', disableGrid = false, h1 = false }) => {
  if (disableGrid) {
    return (
      <FullRow className={`${styles.wrapper} ${className}`} style={{ backgroundImage: `url(${Background})` }}>
        <ColoredTitle
          title={title}
          intlActivated={intlActivated}
          className={styles.title}
          h1={h1}
        >
          <div className={`${styles.grid} ${gridClassName}`}>
            { children }
          </div>
        </ColoredTitle>
      </FullRow>
    );
  }

  return (
    <FullRow className={`${styles.wrapper} ${className}`} style={{ backgroundImage: `url(${Background})` }}>
      <ColoredTitle
        title={title}
        intlActivated={intlActivated}
        className={styles.title}
        h1={h1}
      >
        <Grid className={`${styles.grid} ${gridClassName}`}>
          { children }
        </Grid>
      </ColoredTitle>
    </FullRow>
  );
};

BlackColoredTitle.propTypes = {
  className: PropTypes.string,
  gridClassName: PropTypes.string,
  title: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ]).isRequired,
  children: PropTypes.node.isRequired,
  intlActivated: PropTypes.bool,
  disableGrid: PropTypes.bool,
  h1: PropTypes.bool,
};

export default BlackColoredTitle;
