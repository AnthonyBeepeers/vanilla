import React, { PropTypes } from 'react/lib/React';
import Col from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Col/Col';
import IconBlock from 'reactjs-beepeers-framework/components/General/Web/IconBlock/IconBlock';
import Row from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Row/Row';

import {
  WEBSITE, SNAPCHAT,
  FACEBOOK, TWITTER, INSTAGRAM, YOUTUBE,
  SOUNDCLOUD, SPOTIFY,
  LINKEDIN, OTHER,
} from 'reactjs-beepeers-framework/constants/iconLabelTypes';

import styles from './SocialIcons.css';

const SocialIcons = ({ functionList = null }) => {
  const knownFunctions = [];
  const otherFunctions = [];
  if (functionList !== null && functionList.length > 0) {
    functionList.forEach((functionItem) => {
      switch (functionItem.identifier) {
        case 'FB_PAGE': {
          let fbPageUrl = null;
          if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
            functionItem.parameterList.forEach((parameterItem) => {
              if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                fbPageUrl = parameterItem.value;
              }
            });
          }
          if (fbPageUrl !== null) {
            const fbPageNode = (
              <Col key="FB_PAGE" xs={2} className={styles.iconBlock}>
                <IconBlock
                  type={FACEBOOK}
                  label={fbPageUrl}
                />
              </Col>
            );
            knownFunctions.push(fbPageNode);
          }
          break;
        }
        case 'TWITTER': {
          let twitterUrl = null;
          if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
            functionItem.parameterList.forEach((parameterItem) => {
              if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                twitterUrl = parameterItem.value;
              }
            });
          }
          if (twitterUrl !== null) {
            const twitterNode = (
              <Col key="TWITTER" xs={2} className={styles.iconBlock}>
                <IconBlock
                  type={TWITTER}
                  label={twitterUrl}
                />
              </Col>
            );
            knownFunctions.push(twitterNode);
          }
          break;
        }
        case 'INSTAGRAM': {
          let instagramUrl = null;
          if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
            functionItem.parameterList.forEach((parameterItem) => {
              if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                instagramUrl = parameterItem.value;
              }
            });
          }
          if (instagramUrl !== null) {
            const instagramNode = (
              <Col key="INSTAGRAM" xs={2} className={styles.iconBlock}>
                <IconBlock
                  type={INSTAGRAM}
                  label={instagramUrl}
                />
              </Col>
            );
            knownFunctions.push(instagramNode);
          }
          break;
        }
        case 'YOUTUBE_VIDEO': {
          let youtubeUrl = null;
          if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
            functionItem.parameterList.forEach((parameterItem) => {
              if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                youtubeUrl = parameterItem.value;
              }
            });
          }
          if (youtubeUrl !== null) {
            const youtubeNode = (
              <Col key="YOUTUBE_VIDEO" xs={2} className={styles.iconBlock}>
                <IconBlock
                  type={YOUTUBE}
                  label={youtubeUrl}
                />
              </Col>
            );
            knownFunctions.push(youtubeNode);
          }
          break;
        }
        case 'SOUNDCLOUD': {
          let soundcloudUrl = null;
          if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
            functionItem.parameterList.forEach((parameterItem) => {
              if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                soundcloudUrl = parameterItem.value;
              }
            });
          }
          if (soundcloudUrl !== null) {
            const soundcloudNode = (
              <Col key="SOUNDCLOUD" xs={2} className={styles.iconBlock}>
                <IconBlock
                  type={SOUNDCLOUD}
                  label={soundcloudUrl}
                />
              </Col>
            );
            knownFunctions.push(soundcloudNode);
          }
          break;
        }
        case 'SOUNDCLOUD_PLAYLIST': {
          let soundcloudUrl = null;
          if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
            functionItem.parameterList.forEach((parameterItem) => {
              if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                soundcloudUrl = parameterItem.value;
              }
            });
          }
          if (soundcloudUrl !== null) {
            const soundcloudNode = (
              <Col key="SOUNDCLOUD_PLAYLIST" xs={2} className={styles.iconBlock}>
                <IconBlock
                  type={SOUNDCLOUD}
                  label={soundcloudUrl}
                />
              </Col>
            );
            knownFunctions.push(soundcloudNode);
          }
          break;
        }
        case 'LINKEDIN_PROFILE': {
          let linkedInUrl = null;
          if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
            functionItem.parameterList.forEach((parameterItem) => {
              if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                linkedInUrl = parameterItem.value;
              }
            });
          }
          if (linkedInUrl !== null) {
            const linkedInNode = (
              <Col key="LINKEDIN_PROFILE" xs={2} className={styles.iconBlock}>
                <IconBlock
                  type={LINKEDIN}
                  label={linkedInUrl}
                />
              </Col>
            );
            knownFunctions.push(linkedInNode);
          }
          break;
        }
        case 'LINKEDIN_PAGE': {
          let linkedInUrl = null;
          if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
            functionItem.parameterList.forEach((parameterItem) => {
              if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                linkedInUrl = parameterItem.value;
              }
            });
          }
          if (linkedInUrl !== null) {
            const linkedInNode = (
              <Col key="LINKEDIN_PAGE" xs={2} className={styles.iconBlock}>
                <IconBlock
                  type={LINKEDIN}
                  label={linkedInUrl}
                />
              </Col>
            );
            knownFunctions.push(linkedInNode);
          }
          break;
        }
        case 'YOUTUBE_CHANNEL': {
          let youtubeUrl = null;
          if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
            functionItem.parameterList.forEach((parameterItem) => {
              if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                youtubeUrl = parameterItem.value;
              }
            });
          }
          if (youtubeUrl !== null) {
            const youtubeNode = (
              <Col key="YOUTUBE_CHANNEL" xs={2} className={styles.iconBlock}>
                <IconBlock
                  type={YOUTUBE}
                  label={youtubeUrl}
                />
              </Col>
            );
            knownFunctions.push(youtubeNode);
          }
          break;
        }
        case 'WEB': {
          let webUrl = null;
          if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
            functionItem.parameterList.forEach((parameterItem) => {
              if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                webUrl = parameterItem.value;
              }
            });
          }
          if (webUrl !== null) {
            const webNode = (
              <Col key="WEB" xs={2} className={styles.iconBlock}>
                <IconBlock
                  type={WEBSITE}
                  label={webUrl}
                />
              </Col>
            );
            knownFunctions.push(webNode);
          }
          break;
        }
        case 'SNAPCHAT' : {
          let snapchatUrl = null;
          if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
            functionItem.parameterList.forEach((parameterItem) => {
              if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                snapchatUrl = parameterItem.value;
              }
            });
          }
          if (snapchatUrl !== null) {
            const snapchatNode = (
              <Col key="SNAPCHAT" xs={2} className={styles.iconBlock}>
                <IconBlock
                  type={SNAPCHAT}
                  label={snapchatUrl}
                />
              </Col>
            );
            knownFunctions.push(snapchatNode);
          }
          break;
        }
        case 'SPOTIFY_USER':
        case 'SPOTIFY_PLAYLIST':
        case 'SPOTIFY_ALBUM': {
          let spotifyUrl = null;
          if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
            functionItem.parameterList.forEach((parameterItem) => {
              if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                spotifyUrl = parameterItem.value;
              }
            });
          }
          if (spotifyUrl !== null) {
            const snapchatNode = (
              <Col key={functionItem.identifier} xs={2} className={styles.iconBlock}>
                <IconBlock
                  type={SPOTIFY}
                  label={spotifyUrl}
                />
              </Col>
            );
            knownFunctions.push(snapchatNode);
          }
          break;
        }
        default: {
          if (typeof (functionItem.pictureUrl) !== 'undefined' && functionItem.pictureUrl.trim() !== '') {
            let otherUrl = null;
            if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
              functionItem.parameterList.forEach((parameterItem) => {
                if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                  otherUrl = parameterItem.value;
                }
              });
            }
            if (otherUrl !== null) {
              const otherNode = (
                <Col key={functionItem.identifier} xs={2} className={styles.iconBlock}>
                  <IconBlock
                    type={OTHER}
                    label={otherUrl}
                    typePicture={functionItem.pictureUrl}
                  />
                </Col>
              );
              otherFunctions.push(otherNode);
            }
          }
        }
      }
    });
  }

  return (
    <div>
      <Row center="xs">
        { knownFunctions }
      </Row>
      <Row center="xs">
        { otherFunctions }
      </Row>
    </div>
  );
};

SocialIcons.propTypes = {
  functionList: PropTypes.arrayOf(PropTypes.any).isRequired,
};

export default SocialIcons;
