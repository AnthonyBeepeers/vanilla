import React, { PropTypes } from 'react/lib/React';
import { connect } from 'react-redux';

import { withRouter, Redirect } from 'react-router-dom';

import FlatButton from 'material-ui/FlatButton';

import Row from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Row/Row';
import Col from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Col/Col';

import styles from './FrEnUrlSwitcher.css';

const FrEnUrlSwitcher = ({ locales, className = '', history, location, verticalMode = false }) => {
  const { lang } = locales;
  const langWithoutRegionCode = lang.toLowerCase().split(/[_-]+/)[0];
  let flagEN = false;
  const redirectToLang = (redirectLang) => {
    const purePathname = location.pathname.substring(3, location.pathname.length);
    history.push(`/${redirectLang}${purePathname}`);
  };

  let colSize = 6;
  if (verticalMode) {
    colSize = 12;
  }

  return (
    <div className={className} >
      <Row center="xs">
        <Col xs={colSize}>
          <FlatButton
            label="Fr"
            onTouchTap={() => redirectToLang('fr')}
            className={`${styles.button} ${langWithoutRegionCode === 'fr' ? styles.selected : styles.notSelected}`}
          />
        </Col>
        <Col xs={colSize}>
          <FlatButton
            label="En"
            onTouchTap={() => redirectToLang('en')}
            className={`${styles.button} ${langWithoutRegionCode === 'en' ? styles.selected : styles.notSelected}`}
          />
          { flagEN && <Redirect to={`/en/${location.pathname.substring(3, location.pathname.length)}`} />}
        </Col>
      </Row>
    </div>
  );
};

FrEnUrlSwitcher.propTypes = {
  locales: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ])).isRequired,
  className: PropTypes.string,
  location: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ])).isRequired,
  history: PropTypes.objectOf(PropTypes.any).isRequired,
  verticalMode: PropTypes.bool,
};

const mapStateToProps = state => ({
  locales: state.locales,
});

const FrEnUrlSwitcherConnected = connect(
  mapStateToProps
)(FrEnUrlSwitcher);

const FrEnUrlSwitcherConnectedWithrouter = withRouter(FrEnUrlSwitcherConnected);

export default FrEnUrlSwitcherConnectedWithrouter;
