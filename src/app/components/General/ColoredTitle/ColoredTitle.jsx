import React, { PropTypes } from 'react/lib/React';

import BlockWithTitle from 'reactjs-beepeers-framework/components/General/Web/BlockWithTitle/BlockWithTitle';

import styles from './ColoredTitle.css';

const ColoredTitle = ({ title, children, intlActivated = true, className = '' }) => (
  <BlockWithTitle
    title={title}
    className={`${styles.blockTitle} ${className}`}
    intlActivated={intlActivated}
  >
    { children }
  </BlockWithTitle>
);

ColoredTitle.propTypes = {
  className: PropTypes.string,
  title: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ]).isRequired,
  children: PropTypes.node.isRequired,
  intlActivated: PropTypes.bool,
};

export default ColoredTitle;
