import React, { PropTypes } from 'react/lib/React';
import { connect } from 'react-redux';

import Grid from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Grid/Grid';
import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
import ActorsListFetcher from 'reactjs-beepeers-framework/components/Actors/Fetchers/ActorsListFetcher/ActorsListFetcher';
import FullLineTitle from 'reactjs-beepeers-framework/components/General/Web/FullLineTitle/FullLineTitle';
import ResponsiveContainer from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/ResponsiveContainer/ResponsiveContainer';
import ActorsSlider from 'reactjs-beepeers-framework/components/Actors/Renderers/Web/ActorsSlider/ActorsSlider';

import LinkedPartner from '../../General/LinkedPartner/LinkedPartner';

import { PARTNERS_LIST_KEY } from '../../../constants/actors';

import styles from './Partners.css';
import HoloColoredTitle from '../HoloColoredTitle/HoloColoredTitle';

const Partners = ({ className = '' }) => {
  const renderFunc = (partnersSections) => {
    const renderPartnerFunc = (partner) => {
      if (typeof (partner.originalUrl) !== 'undefined' && typeof (partner.website) !== 'undefined') {
        return (<LinkedPartner partner={partner} pictureUrl={`${partner.originalUrl}&type=png&width=250&height=250`} />);
      }

      if (typeof (partner.thumbnailUrl) !== 'undefined' && typeof (partner.website) !== 'undefined') {
        return (
          <LinkedPartner partner={partner} pictureUrl={partner.thumbnailUrl} />
        );
      }

      if (typeof (partner.coverUrl) !== 'undefined' && typeof (partner.website) !== 'undefined') {
        return (
          <LinkedPartner partner={partner} pictureUrl={`${partner.coverUrl}&type=png&width=250&height=250`} />
        );
      }

      return null;
    };

    const responsivePartnersSliderSettings = {
      xs: {
        slidesToShow: 4,
        slidesToScroll: 4,
        displayArrows: false,
        autoplay: true,
        displayDots: false,
        infinite: true,
        variableWidth: false,
        autoplaySpeed: 7000,
      },
      sm: {
        slidesToShow: 5,
        slidesToScroll: 5,
        displayArrows: false,
        autoplay: true,
        displayDots: false,
        infinite: true,
        variableWidth: false,
        autoplaySpeed: 7000,
      },
      md: {
        slidesToShow: 6,
        slidesToScroll: 6,
        displayArrows: false,
        autoplay: true,
        displayDots: false,
        infinite: true,
        variableWidth: false,
        autoplaySpeed: 7000,
      },
      lg: {
        slidesToShow: 7,
        slidesToScroll: 7,
        displayArrows: false,
        autoplay: true,
        displayDots: false,
        infinite: true,
        variableWidth: false,
        autoplaySpeed: 7000,
      },
    };

    const partnersNodes = partnersSections.map((partnersSection, i) => (
      <div key={i}>
        <FullLineTitle title={partnersSection.name} intlActivated={false} className={styles.sectionTitle} />
        <ResponsiveContainer
          xs={{ height: '25vw' }}
          sm={{ height: '141px' }}
          md={{ height: '155px' }}
          lg={{ height: '162px' }}
        >
          <ActorsSlider
            actors={partnersSection.items}
            renderFunc={renderPartnerFunc}
            responsiveSettings={responsivePartnersSliderSettings}
          />
        </ResponsiveContainer>
      </div>
    ));

    return (
      <FullRow>
        <div className={className}>
          {partnersNodes}
        </div>
      </FullRow>
    );
  };

  return (
    <Grid className={styles.wrapper}>
      <HoloColoredTitle
        title="PARTNERS.TITLE"
        enableGrid={false}
        className={styles.wrapper}
      >
        <ActorsListFetcher
          actorsListKey={PARTNERS_LIST_KEY}
          renderFunc={renderFunc}
        />
      </HoloColoredTitle>
    </Grid>
  );
};

Partners.propTypes = {
  className: PropTypes.string,
};

const mapStateToProps = state => ({
  locales: state.locales,
});

const PartnersConnected = connect(
  mapStateToProps
)(Partners);

export default PartnersConnected;
