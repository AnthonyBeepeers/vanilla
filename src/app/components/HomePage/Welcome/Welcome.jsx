import React from 'react/lib/React';
import ActorFetcherByKey from 'reactjs-beepeers-framework/components/Actors/Fetchers/ActorFetcherByKey/ActorFetcherByKey';
import ReactHtmlParser from 'react-html-parser';

import arrow from '../../../assets/images/navigation.png';
import bulb from '../../../assets/images/bulb.png';
import calendar from '../../../assets/images/calendar.png';
import clock from '../../../assets/images/clock.png';
import styles from './welcome.css';
/* eslint-disable */
import { HOME_SCREEN } from '../../../constants/actors';

let renderFuncWelcome = (actor) => {
 console.log('welcome');
 console.log(actor);
 const { displayName, description } = actor;
 return (
  <div className={styles.welcome} >
   <div className={styles.welcomeTitle}>
    { ReactHtmlParser(displayName)}
   </div>
   <div className={styles.welcomeText}>
    { ReactHtmlParser(description) }
   </div>
  </div>
 );
};
let renderFuncInfos = ( actor) => {
 let {name2: where, name3: day, description2: hours, description3: why} = actor;
 console.log(location);
 return (
  <div className={styles.infosContainer}>

   <div className={styles.row1}>
    <div className={styles.icon}> <img className={styles.img} src={arrow}/> </div>
    <div className={styles.infos}> {ReactHtmlParser(where)} </div>
   </div>
   <div className={styles.row2}>
    <div className={styles.icon}> <img className={styles.img} src={calendar}/> </div>
    <div className={styles.infos}> {ReactHtmlParser(day)} </div>
   </div>
   <div className={styles.row3}>
    <div className={styles.icon}> <img className={styles.img} src={clock}/> </div>
    <div className={styles.infos}> {ReactHtmlParser(hours)} </div>
   </div>
   <div className={styles.row4}>
    <div className={styles.icon}> <img className={styles.img} src={bulb}/> </div>
    <div className={styles.infos}>
     {ReactHtmlParser(why)}
    </div>
   </div>

  </div>
 );
};


class Welcome extends React.Component {
constructor(props){
  super(props);
  this.state = { clicked: false}
}

render() {
 let {context} = this.props;

 return (
  <div className={styles.Row}>
   <div className={styles.ColL}>
    <ActorFetcherByKey actorKey={HOME_SCREEN} renderFunc={renderFuncWelcome} />
   </div>
   <div className={styles.ColR}>
    <ActorFetcherByKey actorKey={HOME_SCREEN} renderFunc={renderFuncInfos} />
   </div>
  </div>
 );}
}

export default Welcome;
