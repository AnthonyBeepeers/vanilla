import React, { PropTypes } from 'react/lib/React';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import Grid from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Grid/Grid';

// import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
import Row from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Row/Row';
import Col from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Col/Col';
import CoverPictureLink from '../../Shared/CoverPictureLink/CoverPictureLink';

// import DefaultPicture from '../../../../assets/images/Background.png';
import programme from '../../../assets/images/MegaMenu/programme.jpg';
import hotels from '../../../assets/images/MegaMenu/hotel.jpg';
import infos from '../../../assets/images/MegaMenu/infos_pratiques.jpg';


// import MobileAppsPicture from '../../../../assets/images/Festival/Home/mobile_apps.png';
// import GalleryPicture from '../../../../assets/images/Festival/Home/gallery.png';

import styles from './HomeMegaMenu.css';


const HomeMegaMenu = ({ locales }) => {
  const { lang } = locales;
  const langWithoutRegionCode = lang.toLowerCase().split(/[_-]+/)[0];

  const ProgramNode = (
    <div className={styles.menuItem}>
      <CoverPictureLink
        className={styles.menuPictureTextCenter}
        style={{ border: '1px solid rgb(255, 255, 255)' }}
        pictureUrl={programme}
        linkPath={`/${langWithoutRegionCode}/program`}
        pictoUrl="#"
        linkName=""
      />
    </div>
 );

  const HotelsNode = (
    <div className={styles.menuItem}>
      <CoverPictureLink
        className={styles.menuPictureTextCenter}
        style={{ border: '1px solid rgb(255, 255, 255)' }}
        pictureUrl={hotels}
        linkPath={`/${langWithoutRegionCode}/hotels`}
        pictoUrl="#"
        linkName=""
      />
    </div>
 );
  const InfosNode = (
    <div className={styles.menuItem}>
      <CoverPictureLink
        className={styles.menuPictureTextCenter}
        style={{ border: '1px solid rgb(255, 255, 255)' }}
        pictureUrl={infos}
        linkPath={`/${langWithoutRegionCode}/infos`}
        pictoUrl="#"
        linkName=""
      />
    </div>
 );


  return (
    <Grid className={styles.gridWrapper}>
      <Row>
        <Col xs={12} sm={4}>
          { ProgramNode }
        </Col>
        <Col xs={12} sm={4}>
          { HotelsNode }
        </Col>
        <Col xs={12} sm={4}>
          { InfosNode }
        </Col>
      </Row>
    </Grid>
  );
};

HomeMegaMenu.propTypes = {
  /* appConfig: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.string,
    PropTypes.array,
    PropTypes.object,
    PropTypes.number,
  ])).isRequired, */
  /* eslint-disable react/no-unused-prop-types */
  intl: PropTypes.shape({
    formatMessage: PropTypes.func,
  }).isRequired,
  /* eslint-enable react/no-unused-prop-types */
  locales: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ])).isRequired,
};

const mapStateToProps = state => ({
  // appConfig: state.appConfig,
  locales: state.locales,
});

const HomeMegaMenuIntl = injectIntl(HomeMegaMenu);

const HomeMegaMenuConnected = connect(
  mapStateToProps
)(HomeMegaMenuIntl);

export default HomeMegaMenuConnected;
