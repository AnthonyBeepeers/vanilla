import React from 'react/lib/React';
import { withRouter } from 'react-router-dom';
import { PropTypes } from 'react';

import Grid from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Grid/Grid';

import styles from './HomeContent.css';
import Welcome from '../Welcome/Welcome';

const HomeContent = () =>
   (
     <Grid className={styles.wrapper}>
       <Welcome />
     </Grid>
  )
;

HomeContent.propTypes = {
  location: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ])).isRequired,
};

const HomeContentConected = withRouter(HomeContent);

export default HomeContentConected;
