import React from 'react/lib/React';

import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
// import PopUpVideo from 'reactjs-beepeers-framework/components/General/Web/PopUpVideo/PopUpVideo';

import HomeContent from '../HomeContent/HomeContent';
// import HomeCarousel from '../HomeCarousel/HomeCarousel';
// import { PURPLE, ORANGE, WHITE } from '../../../constants/colors';
import video from '../../../assets/video/Altrad_Showcase.mp4';
import styles from './Home.css';

const Home = () => (
  <div className={styles.wrapper} >
    <video src={video} className={styles.video} controls loop autoPlay muted>
      <source src={video} type="video/mp4" />
    </video>
    <FullRow className={styles.content}>
      <HomeContent />
    </FullRow>
    { /* <PopUpVideo video={video} btnBackgroundColor={PURPLE} btnHoverColor={ORANGE} btnColor={WHITE} key={'crossover'} /> */ }
  </div>
);

export default Home;
