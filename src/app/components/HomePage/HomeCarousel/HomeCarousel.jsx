import React from 'react/lib/React';

import ActorCarouselByKey from '../../General/SliderWithLinks/ActorCarouselByKey/ActorCarouselByKey';

import { HOME_HEADERS } from '../../../constants/actors';

const HomeCarousel = () => (
  <ActorCarouselByKey actorKey={HOME_HEADERS} />
);

export default HomeCarousel;
