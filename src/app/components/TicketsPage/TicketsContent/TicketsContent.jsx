import React from 'react/lib/React';
import Grid from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Grid/Grid';
import WeezeventTicketing from 'reactjs-beepeers-framework/components/Vendor/WeezeventTicketing/WeezeventTicketing';

import styles from './TicketsContent.css';

const TicketsContent = () => (
  <Grid className={styles.wrapper}>
    <WeezeventTicketing
      dataId={'291422'}
      url={'https://www.weezevent.com/widget_billeterie.php?id_evenement=291422&lg_billetterie=1&code=54425&resize=1&width_auto=1&color_primary=00AEEF'}
    />
  </Grid>
);

export default TicketsContent;
