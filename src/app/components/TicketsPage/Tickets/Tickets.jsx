import React from 'react/lib/React';

import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';

import TicketsHeader from '../TicketsHeader/TicketsHeader';
import TicketsContent from '../TicketsContent/TicketsContent';
import TicketsContent2 from '../TicketsContent/TicketsContent2';
import TicketsInfos from '../TicketsInfos/TicketsInfos';

import styles from './Tickets.css';

const Tickets = () => {
  let content = (
    <TicketsContent />
  );

  const params = new URLSearchParams(window.location.search);
  if (params.get('t2') != null) {
    content = (
      <TicketsContent2 />
    );
  }
  return (
    <div className={styles.wrapper}>
      <FullRow className={styles.content}>
        <TicketsHeader />
        { content }
      </FullRow>
      <TicketsInfos />
    </div>
  );
};

export default Tickets;
