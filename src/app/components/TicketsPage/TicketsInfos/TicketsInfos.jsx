import React from 'react/lib/React';

import Grid from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Grid/Grid';
import ActorFetcherByKey from 'reactjs-beepeers-framework/components/Actors/Fetchers/ActorFetcherByKey/ActorFetcherByKey';
import ResponsiveMediaGallery from 'reactjs-beepeers-framework/components/General/Web/ResponsiveMediaGallery/ResponsiveMediaGallery';

import HoloColoredTitle from '../../General/HoloColoredTitle/HoloColoredTitle';
import StaticContentVertical from '../../Shared/StaticContentVertical/StaticContentVertical';

import { TICKETS_INFOS_KEY } from '../../../constants/actors';

import styles from './TicketsInfos.css';

const TicketsInfos = () => {
  const renderFunc = (actor) => {
    if (typeof (actor.profileId) !== 'undefined') {
      let mediasNode = null;
      if (typeof (actor.medias) !== 'undefined' && actor.medias.length > 0) {
        mediasNode = (
          <ResponsiveMediaGallery
            className={styles.mediaGallery}
            modalClassName={styles.modalMediaGallery}
            galleryId={`TicketsInfosMediaGallery_${actor.profileId}`}
            medias={actor.medias}
            nbPerRows={1}
            xs={{ height: '50vw' }}
            sm={{ height: '200px' }}
            md={{ height: '265px' }}
            lg={{ height: '325px' }}
            rowAttributes={{ center: 'xs' }}
            useContainPicture
          />
        );
      }

      return (
        <Grid className={styles.wrapper}>
          <HoloColoredTitle
            title={actor.displayName}
            intlActivated={false}
            enableGrid={false}
          >
            <StaticContentVertical
              className={styles.staticContent}
              contentColWidth={12}
              actor={actor}
              disableGrid
              disableTitle
              disableCover
              disableMedias
            />
            { mediasNode }
          </HoloColoredTitle>
        </Grid>
      );
    }
    return null;
  };

  return (
    <ActorFetcherByKey actorKey={TICKETS_INFOS_KEY} renderFunc={renderFunc} />
  );
};

export default TicketsInfos;
