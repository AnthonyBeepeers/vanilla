import React from 'react/lib/React';
import { PropTypes } from 'react';
import { injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import Grid from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Grid/Grid';
import { Tab, Tabs } from 'material-ui';

import styles from './TicketsHeader.css';
import { ORANGE, PURPLE, WHITE } from '../../../constants/colors';

const TicketsHeader = ({ history, intl }) => {
  let selectedTab = 't1';
  const params = new URLSearchParams(window.location.search);
  if (params.get('t2') != null) {
    selectedTab = 't2';
  }

  const navigateTo = (page) => {
    if (page === 2) {
      history.push('/ticketing?t2');
    } else {
      history.push('/ticketing');
    }
  };


  const { formatMessage } = intl;

  const PlagesShopLabel = formatMessage({ id: 'TICKETS.PLAGES_SHOP' });
  const WaitingPlagesShopLabel = formatMessage({ id: 'TICKETS.WAITING_SHOP' });

  return (
    <Grid className={styles.wrapper}>
      <Tabs
        value={selectedTab}
        inkBarStyle={{ backgroundColor: ORANGE, height: '6px', marginTop: '-6px' }}
        tabItemContainerStyle={{ backgroundColor: PURPLE }}
      >
        <Tab
          value={'t1'}
          style={{ color: WHITE }}
          label={PlagesShopLabel}
          className={styles.tabBtn}
          onActive={() => navigateTo(1)}
        />
        <Tab
          value={'t2'}
          style={{ color: WHITE }}
          label={WaitingPlagesShopLabel}
          className={styles.tabBtn}
          onActive={() => navigateTo(2)}
        />
      </Tabs>
    </Grid>
  );
};

TicketsHeader.propTypes = {
  history: PropTypes.objectOf(PropTypes.any).isRequired,
  intl: PropTypes.objectOf(PropTypes.any).isRequired,
};

const TicketsHeaderIntl = injectIntl(TicketsHeader);

const mapStateToProps = state => ({
  history: state.history,
});

const TicketsHeaderConnected = connect(
  mapStateToProps
)(TicketsHeaderIntl);

export default TicketsHeaderConnected;
