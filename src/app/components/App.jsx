import React, { Component, PropTypes } from 'react/lib/React';
import { connect } from 'react-redux';

import Root from 'reactjs-beepeers-framework/components/Routing/Root';
import RouteList from 'reactjs-beepeers-framework/components/Routing/RouteList';
import NotificationCenter from 'reactjs-beepeers-framework/components/General/Web/NotificationCenter/NotificationCenter';
import CookiePanel from 'reactjs-beepeers-framework/components/Account/Renderers/Web/AccountCookies/CookiePanel/CookiePanel';

import { fetchGuestAuth } from 'reactjs-beepeers-framework/actions/auth';
import { fetchDateFromServer } from 'reactjs-beepeers-framework/actions/serverDate';
import { emptyPending } from 'reactjs-beepeers-framework/actions/pending';
import { resetAllDates } from 'reactjs-beepeers-framework/actions/helper';
import { fetchAppContext } from 'reactjs-beepeers-framework/actions/context';

import Background from '../assets/images/Shared/background.jpg';
import { ORANGE } from '../constants/colors';

import styles from './App.css';
import Header from './General/Header/Header/Header';
import Footer from './General/Footer/Footer';

import NotFound from './General/NotFound/NotFound';


class App extends Component {
  componentWillMount() {
    this.fetchData();
  }

  componentDidUpdate() {
    const { pending, dispatch, context } = this.props;
    const { isFetching, authKey, logOutIsFetching = false } = this.props.auth;
    const contextIsFetching = context.isFetching;
    const { description } = context;

    if (
      typeof (isFetching) === 'undefined'
      || (!isFetching && typeof (authKey) === 'undefined')
    ) {
      dispatch(fetchGuestAuth());
    } else if (
      !logOutIsFetching
      && !isFetching
      && authKey !== null && authKey !== ''
      && typeof (contextIsFetching) !== 'undefined' && !contextIsFetching
      && typeof (description) !== 'undefined' && description !== null
    ) {
      if (Object.keys(pending).length > 0) {
        Object.keys(pending).forEach(pendingId => (
          pending[pendingId]()
        ));

        dispatch(emptyPending());
      }
    }
  }

  fetchData() {
    const { dispatch } = this.props;
    const { authKey } = this.props.auth;

    dispatch(fetchGuestAuth());
    dispatch(fetchDateFromServer());
    dispatch(resetAllDates());
    if (typeof (authKey) !== 'undefined' && authKey !== null && authKey !== '') {
      dispatch(fetchAppContext());
    }
  }

  render() {
    const { store, history } = this.props;

    return (
      <Root store={store} history={history} className={styles.app} style={{ backgroundImage: `url(${Background})` }}>
        <link rel="stylesheet" type="text/css" charSet="UTF-8" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />
        <div id="top" />
        <NotificationCenter />
        <Header />
        <RouteList className={styles.mainWrapper}>
          <NotFound />
        </RouteList>
        <Footer />
        <CookiePanel backgroundColor={ORANGE} hovColor={'firebrick'} />
      </Root>
    );
  }
}

App.propTypes = {
  store: PropTypes.objectOf(PropTypes.func).isRequired,
  history: PropTypes.objectOf(PropTypes.any).isRequired,
  dispatch: PropTypes.func.isRequired,
  pending: PropTypes.objectOf(PropTypes.func).isRequired,
  auth: PropTypes.shape({
    authKey: PropTypes.string,
    isFetching: PropTypes.bool,
    logOutIsFetching: PropTypes.bool,
  }).isRequired,
  context: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.bool,
    PropTypes.number,
    PropTypes.object,
    PropTypes.string,
  ])).isRequired,
};

const mapStateToProps = state => ({
  pending: state.pending,
  auth: state.auth,
  context: state.context,
  history: state.history,
});

const AppConnected = connect(
  mapStateToProps
)(App);

export default AppConnected;
