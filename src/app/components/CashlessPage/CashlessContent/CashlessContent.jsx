import React from 'react/lib/React';

import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
import NemopayCashless from 'reactjs-beepeers-framework/components/Vendor/NemopayCashless/NemopayCashless';
import ActorFetcherByKey from 'reactjs-beepeers-framework/components/Actors/Fetchers/ActorFetcherByKey/ActorFetcherByKey';

import { FAQ_CASHLESS_LIST_KEY } from '../../../constants/actors';

const CashlessContent = () => {
  const renderFunc = (actor) => {
    if (typeof (actor.website) !== 'undefined') {
      return (
        <FullRow>
          <NemopayCashless
            url={actor.website}
          />
        </FullRow>
      );
    }
    return null;
  };

  return (
    <ActorFetcherByKey actorKey={FAQ_CASHLESS_LIST_KEY} renderFunc={renderFunc} />
  );
};

export default CashlessContent;
