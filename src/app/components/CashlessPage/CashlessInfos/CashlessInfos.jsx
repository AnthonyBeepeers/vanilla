import React from 'react/lib/React';

import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
import ActorFetcherByKey from 'reactjs-beepeers-framework/components/Actors/Fetchers/ActorFetcherByKey/ActorFetcherByKey';
import StaticContentVertical from '../../Shared/StaticContentVertical/StaticContentVertical';

import { CASHLESS_KEY } from '../../../constants/actors';

import styles from './CashlessInfos.css';
// import HoloColoredTitle from '../../General/HoloColoredTitle/HoloColoredTitle';

const TicketsInfos = () => {
  const renderFunc = (actor) => {
    if (typeof (actor.profileId) !== 'undefined') {
      return (
        <FullRow className={styles.wrapper}>
          <StaticContentVertical
            className={styles.staticContent}
            actor={actor}
            disableGrid
            disableTitle
            disableCover
            disableMedias
            disableDetails
          />
        </FullRow>
      );
    }
    return null;
  };

  return (
    <ActorFetcherByKey actorKey={CASHLESS_KEY} renderFunc={renderFunc} />
  );
};

export default TicketsInfos;
