import React from 'react/lib/React';

import ActorCarouselByKey from 'reactjs-beepeers-framework/components/Actors/Renderers/Web/ActorCarouselByKey/ActorCarouselByKey';

import { CASHLESS_KEY } from '../../../constants/actors';

const CashlessCarousel = () => (
  <ActorCarouselByKey actorKey={CASHLESS_KEY} />
);

export default CashlessCarousel;
