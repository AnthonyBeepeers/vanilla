import React from 'react/lib/React';

import Grid from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Grid/Grid';
import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
import Row from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Row/Row';

import CashlessCarousel from '../CashlessCarousel/CashlessCarousel';

import CashlessInfos from '../CashlessInfos/CashlessInfos';
import CashlessContent from '../CashlessContent/CashlessContent';

import styles from './Cashless.css';

const Cashless = () => (
  <FullRow>
    <Grid className={styles.wrapper}>
      <Row>
        <CashlessCarousel />
      </Row>
      <Row>
        <CashlessContent />
      </Row>
      <Row>
        <CashlessInfos />
      </Row>
    </Grid>
  </FullRow>
);

export default Cashless;
