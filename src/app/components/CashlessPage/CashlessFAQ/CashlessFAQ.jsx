import React from 'react/lib/React';

import FAQContent from 'reactjs-beepeers-framework/components/Actors/Renderers/Web/FAQ/FAQ';

import { FAQ_CASHLESS_LIST_KEY } from '../../../constants/actors';

import styles from './CashlessFAQ.css';
import HoloColoredTitle from '../../General/HoloColoredTitle/HoloColoredTitle';

const FAQ = () => (
  <HoloColoredTitle
    title="FAQ.FAQ_TITLE"
    enableGrid={false}
    className={styles.wrapper}
  >
    <FAQContent
      actorsListKey={FAQ_CASHLESS_LIST_KEY}
      sidenavClassName={styles.sidenav}
      sectionClassName={styles.section}
      faqItemClassName={styles.faqItem}
    />
  </HoloColoredTitle>
);

export default FAQ;
