import React from 'react/lib/React';

import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';

import Grid from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Grid/Grid';
import Row from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Row/Row';

import CashLessCarousel from '../CashlessCarousel/CashlessCarousel';
import CashContent from '../CashlessContent/CashlessContent';
import CashlessFAQ from '../CashlessFAQ/CashlessFAQ';
import styles from './Home.css';

const Home = () => (
  <div >
    <FullRow className={styles.content}>
      <CashLessCarousel />
      <Grid className={styles.wrapper}>
        <Row>
          <CashContent />

        </Row>
        <Row>
          <CashlessFAQ />

        </Row>
      </Grid>
    </FullRow>
  </div>

);

export default Home;
