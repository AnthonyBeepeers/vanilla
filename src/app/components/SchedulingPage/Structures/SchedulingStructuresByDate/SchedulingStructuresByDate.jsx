import React, { PropTypes } from 'react/lib/React';
import { connect } from 'react-redux';

import moment from 'moment';

import { Tabs, Tab } from 'material-ui/Tabs';

import { FormattedMessage, injectIntl } from 'react-intl';

import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
import ResponsiveGallery from 'reactjs-beepeers-framework/components/Layout/Web/ResponsiveGallery/ResponsiveGallery';
import TypedActorsListFetcherByKey from 'reactjs-beepeers-framework/components/Actors/Fetchers/TypedActorsListFetcherByKey/TypedActorsListFetcherByKey';
/* eslint-disable max-len */
import FullLineTypedActorsListByKeyAutoMoreButton from 'reactjs-beepeers-framework/components/Actors/Renderers/Web/FullLineTypedActorsListByKeyAutoMoreButton/FullLineTypedActorsListByKeyAutoMoreButton';
/* eslint-enable max-len */

import { DATE_FORMAT } from 'reactjs-beepeers-framework/constants/beepeersConfig';

import SchedulingContainer from '../../SchedulingContainer/SchedulingContainer';

import LinkedStructure from '../LinkedStructure/LinkedStructure';

import { CONVENTION_STRUCTURE_ACTOR_TYPE } from '../../../../../constants/actors';

import { WHITE, BLUE, PINK } from '../../../../../constants/colors';

import morePictureUrl from '../../../../../assets/images/more_btn.png';
import StructureDefaultPicture from '../../../../../assets/images/structure_default.png';

import styles from './SchedulingStructuresByDate.css';

const SchedulingStructuresByDate = ({ match, appConfig, locales, history }) => {
  const { lang } = locales;
  const langWithoutRegionCode = lang.toLowerCase().split(/[_-]+/)[0];

  const goToAll = () => {
    history.push(`/${langWithoutRegionCode}/convention/scheduling/structures`);
  };

  const goToDate = (date) => {
    const dateURI = encodeURIComponent(date.format(DATE_FORMAT).replace(/\./g, '&#46;'));
    history.push(`/${langWithoutRegionCode}/convention/scheduling/structures/bydate/${dateURI}`);
  };

  const { params = {} } = match;

  const currentDate = moment(decodeURIComponent(params.date).replace(/&#46;/g, '.')).utcOffset(0).format(DATE_FORMAT);

  const renderFunc = (structures, isFetching) => {
    const renderStructureFunc = (structure) => {
      if (typeof (structure.thumbnailUrl) !== 'undefined') {
        return (
          <LinkedStructure
            pictureUrl={`${structure.originalUrl}&width=250&height=250&type=png`}
            structure={structure}
          />
        );
      }

      if (typeof (structure.coverUrl) !== 'undefined') {
        return (
          <LinkedStructure
            pictureUrl={`${structure.coverUrl}&width=250&height=250&type=png`}
            structure={structure}
          />
        );
      }

      return (
        <LinkedStructure
          pictureUrl={StructureDefaultPicture}
          structure={structure}
        />
      );
    };

    let structuresNodes = null;
    if (structures.length > 0) {
      structuresNodes = (
        <ResponsiveGallery
          list={structures}
          renderItemFunc={renderStructureFunc}
          nbPerRows={{ xs: 3, sm: 6 }}
          strict
        />
      );
    }

    if (structuresNodes !== null) {
      return structuresNodes;
    }

    if (!isFetching) {
      return (
        <FullRow className={styles.emptyWrapper}>
          <FormattedMessage id={'CONVENTION.SCHEDULING.STRUCTURES.NO_STRUCTURE'} />
        </FullRow>
      );
    }

    return null;
  };

  const tabsNodes = [];

  const param = {
    nbResult: 50,
    displaySections: false,
    subscribersEventDay: currentDate,
  };

  tabsNodes.push((
    <Tab
      key="A-Z"
      style={{ color: WHITE }}
      label="A - Z"
      className={styles.tabBtn}
      onActive={() => goToAll()}
      value="A-Z"
    >
      <span>&nbsp;</span>
    </Tab>
  ));

  let validDateSelected = false;

  appConfig.schedulingDates.forEach((date) => {
    const dateMoment = moment(date).utcOffset(0);

    const formattedDateMoment = dateMoment.format(DATE_FORMAT);

    let dateFormat = 'DD MMM YYYY';
    if (langWithoutRegionCode === 'en') {
      dateFormat = 'MMM DD YYYY';
    }
    if (moment().year() === dateMoment.year()) {
      dateFormat = 'DD MMM';
      if (langWithoutRegionCode === 'en') {
        dateFormat = 'MMM DD';
      }
    }

    if (formattedDateMoment === currentDate) {
      validDateSelected = true;
      tabsNodes.push((
        <Tab
          key={date}
          style={{ color: WHITE }}
          label={dateMoment.format(dateFormat)}
          className={styles.tabBtn}
          onActive={() => goToDate(dateMoment)}
          value={formattedDateMoment}
        >
          <TypedActorsListFetcherByKey
            actorTypeKey={CONVENTION_STRUCTURE_ACTOR_TYPE}
            params={param}
            flattenSections
            renderFunc={renderFunc}
          />
          <FullLineTypedActorsListByKeyAutoMoreButton
            actorTypeKey={CONVENTION_STRUCTURE_ACTOR_TYPE}
            params={param}
            loadingColor={PINK}
            pictureUrl={morePictureUrl}
            className={styles.moreBtnWrapper}
          />
        </Tab>
      ));
    } else {
      tabsNodes.push((
        <Tab
          key={date}
          style={{ color: WHITE }}
          label={dateMoment.format(dateFormat)}
          className={styles.tabBtn}
          onActive={() => goToDate(dateMoment)}
          value={formattedDateMoment}
        >
          <span>&nbsp;</span>
        </Tab>
      ));
    }
  });

  if (!validDateSelected) {
    goToAll();
  }

  return (
    <SchedulingContainer>
      <Tabs
        inkBarStyle={{ backgroundColor: PINK, height: '6px', marginTop: '-6px' }}
        tabItemContainerStyle={{ backgroundColor: BLUE }}
        value={currentDate}
      >
        { tabsNodes }
      </Tabs>
    </SchedulingContainer>
  );
};

SchedulingStructuresByDate.propTypes = {
  appConfig: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.string,
    PropTypes.array,
    PropTypes.object,
    PropTypes.number,
  ])).isRequired,
  history: PropTypes.objectOf(PropTypes.any).isRequired,
  match: PropTypes.objectOf(PropTypes.any).isRequired,
  locales: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ])).isRequired,
};

const SchedulingStructuresByDateIntl = injectIntl(SchedulingStructuresByDate);

const mapStateToProps = state => ({
  appConfig: state.appConfig,
  history: state.history,
  locales: state.locales,
});

const SchedulingStructuresByDateIntlConnected = connect(
  mapStateToProps
)(SchedulingStructuresByDateIntl);

export default SchedulingStructuresByDateIntlConnected;
