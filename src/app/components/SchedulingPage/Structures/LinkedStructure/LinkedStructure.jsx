import React, { PropTypes } from 'react/lib/React';

import { FormattedMessage } from 'react-intl';

import ContainPicture from 'reactjs-beepeers-framework/components/General/Web/ContainPicture/ContainPicture';

import Link from '../../../../Routing/Link/Link';

import styles from './LinkedStructure.css';

const LinkedStructure = ({ structure, pictureUrl }) => {
  const structureName = encodeURIComponent(structure.displayName.replace(/\./g, '&#46;'));
  return (
    <Link to={`/convention/structure/${structure.profileId}/${structureName}`} className={styles.wrapper}>
      <div className={styles.pictureWrapper}>
        <div className={styles.pictureContent}>
          <ContainPicture pictureUrl={pictureUrl} />
          <div className={styles.moreInfo}>
            <FormattedMessage id={'BUTTON.MORE_INFOS'} />
          </div>
        </div>
      </div>
      <div className={styles.contentWrapper}>
        <div className={styles.structureName}>
          { structure.displayName }
        </div>
        <div className={styles.structureActivity}>
          { structure.name }
        </div>
      </div>
    </Link>
  );
};

LinkedStructure.propTypes = {
  structure: PropTypes.objectOf(PropTypes.any).isRequired,
  pictureUrl: PropTypes.string.isRequired,
};

export default LinkedStructure;
