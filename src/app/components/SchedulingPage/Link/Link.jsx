import React, { PropTypes } from 'react/lib/React';
import { connect } from 'react-redux';

import { Link as RouterLink } from 'react-router-dom';

const Link = ({ locales, to, ...remainProps }) => {
  const { lang } = locales;
  const langWithoutRegionCode = lang.toLowerCase().split(/[_-]+/)[0];

  let finalTo = to;
  if (typeof (to) === 'string') {
    finalTo = `/${langWithoutRegionCode}${to}`;
  } else {
    finalTo = {
      ...to,
      pathname: `/${langWithoutRegionCode}${to.pathname}`,
    };
  }

  const remainPropsWithoutDispatch = {
    ...remainProps,
  };

  delete remainPropsWithoutDispatch.dispatch;

  return (
    <RouterLink to={finalTo} {...remainPropsWithoutDispatch} />
  );
};

Link.propTypes = {
  locales: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ])).isRequired,
  to: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ]).isRequired,
};

const mapStateToProps = state => ({
  locales: state.locales,
});

const LinkConnected = connect(
  mapStateToProps
)(Link);

export default LinkConnected;
