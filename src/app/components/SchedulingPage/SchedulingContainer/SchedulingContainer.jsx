import React, { PropTypes } from 'react/lib/React';
import { connect } from 'react-redux';

import Grid from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Grid/Grid';

import styles from './SchedulingContainer.css';

const SchedulingContainer = ({ children, className = '' }) =>
   (
     <div className={`${styles.wrapper} ${className}`}>
       <div
         className={styles.childrenWrapper}
       >
         <Grid className={styles.grid}>
           { children }
         </Grid>
       </div>
       <img role="presentation" src="https://secure.adnxs.com/seg?add=9716956&t=2" width="1" height="1" style={{ display: 'none' }} />
     </div>
  )
;

SchedulingContainer.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
};

const mapStateToProps = state => ({
  locales: state.locales,
});

const SchedulingContainerConnected = connect(
  mapStateToProps
)(SchedulingContainer);

export default SchedulingContainerConnected;
