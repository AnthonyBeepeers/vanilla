import React, { PropTypes } from 'react/lib/React';
import { connect } from 'react-redux';

import moment from 'moment';

import { Tabs, Tab } from 'material-ui/Tabs';

import { FormattedMessage } from 'react-intl';

import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
import ResponsiveGallery from 'reactjs-beepeers-framework/components/Layout/Web/ResponsiveGallery/ResponsiveGallery';
import TypedActorsListFetcherByKey from 'reactjs-beepeers-framework/components/Actors/Fetchers/TypedActorsListFetcherByKey/TypedActorsListFetcherByKey';
/* eslint-disable max-len */
import FullLineTypedActorsListByKeyAutoMoreButton from 'reactjs-beepeers-framework/components/Actors/Renderers/Web/FullLineTypedActorsListByKeyAutoMoreButton/FullLineTypedActorsListByKeyAutoMoreButton';
/* eslint-enable max-len */

import { DATE_FORMAT } from 'reactjs-beepeers-framework/constants/beepeersConfig';

import morePictureUrl from '../../../../../assets/images/more_btn.png';

import SchedulingContainer from '../../SchedulingContainer/SchedulingContainer';

import LinkedShowcase from '../LinkedShowcase/LinkedShowcase';

import { CONVENTION_SHOWCASE_ACTOR_TYPE } from '../../../../../constants/actors';

import { WHITE, BLUE, PINK } from '../../../../../constants/colors';
import ShowcaseDefaultPicture from '../../../../../assets/images/person_default.png';

import styles from './SchedulingShowcases.css';

const SchedulingShowcases = ({ appConfig, history, locales }) => {
  const { lang } = locales;
  const langWithoutRegionCode = lang.toLowerCase().split(/[_-]+/)[0];

  const goToDate = (date) => {
    const dateURI = encodeURIComponent(date.format(DATE_FORMAT).replace(/\./g, '&#46;'));
    history.push(`/${langWithoutRegionCode}/convention/scheduling/showcases/bydate/${dateURI}`);
  };

  const renderFunc = (showcases, isFetching) => {
    const renderShowcaseFunc = (showcase) => {
      if (typeof (showcase.thumbnailUrl) !== 'undefined') {
        return (
          <LinkedShowcase
            pictureUrl={`${showcase.originalUrl}&width=250&height=250`}
            showcase={showcase}
          />
        );
      }

      if (typeof (showcase.coverUrl) !== 'undefined') {
        return (
          <LinkedShowcase
            pictureUrl={`${showcase.coverUrl}&width=250&height=250`}
            showcase={showcase}
          />
        );
      }

      return (
        <LinkedShowcase
          pictureUrl={ShowcaseDefaultPicture}
          showcase={showcase}
        />
      );
    };

    let showcasesNodes = null;
    if (showcases.length > 0) {
      showcasesNodes = (
        <ResponsiveGallery
          list={showcases}
          renderItemFunc={renderShowcaseFunc}
          nbPerRows={{ xs: 3, sm: 6 }}
          strict
        />
      );
    }

    if (showcasesNodes !== null) {
      return showcasesNodes;
    }

    if (!isFetching) {
      return (
        <FullRow className={styles.emptyWrapper}>
          <FormattedMessage id={'CONVENTION.SCHEDULING.SHOWCASES.NO_SHOWCASE'} />
        </FullRow>
      );
    }

    return null;
  };

  const tabsNodes = [];
  const param = {
    nbResult: 50,
    displaySections: false,
  };
  tabsNodes.push((
    <Tab
      key="A-Z"
      style={{ color: WHITE }}
      label="A - Z"
      className={styles.tabBtn}
    >
      <TypedActorsListFetcherByKey
        actorTypeKey={CONVENTION_SHOWCASE_ACTOR_TYPE}
        params={param}
        flattenSections
        renderFunc={renderFunc}
      />
      <FullLineTypedActorsListByKeyAutoMoreButton
        actorTypeKey={CONVENTION_SHOWCASE_ACTOR_TYPE}
        params={param}
        loadingColor={PINK}
        pictureUrl={morePictureUrl}
        className={styles.moreBtnWrapper}
      />
    </Tab>
  ));

  appConfig.schedulingDates.forEach((date) => {
    const dateMoment = moment(date).utcOffset(0);

    let dateFormat = 'DD MMM YYYY';
    if (langWithoutRegionCode === 'en') {
      dateFormat = 'MMM DD YYYY';
    }
    if (moment().year() === dateMoment.year()) {
      dateFormat = 'DD MMM';
      if (langWithoutRegionCode === 'en') {
        dateFormat = 'MMM DD';
      }
    }
    tabsNodes.push((
      <Tab
        key={date}
        style={{ color: WHITE }}
        label={dateMoment.format(dateFormat)}
        className={styles.tabBtn}
        onActive={() => goToDate(dateMoment)}
      >
        <span>&nbsp;</span>
      </Tab>
    ));
  });
  return (
    <SchedulingContainer>
      <Tabs
        inkBarStyle={{ backgroundColor: PINK, height: '6px', marginTop: '-6px' }}
        tabItemContainerStyle={{ backgroundColor: BLUE }}
      >
        { tabsNodes }
      </Tabs>
    </SchedulingContainer>
  );
};

SchedulingShowcases.propTypes = {
  appConfig: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.string,
    PropTypes.array,
    PropTypes.object,
    PropTypes.number,
  ])).isRequired,
  history: PropTypes.objectOf(PropTypes.any).isRequired,
  locales: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ])).isRequired,
};

const mapStateToProps = state => ({
  appConfig: state.appConfig,
  history: state.history,
  locales: state.locales,
});

const SchedulingShowcasesConnected = connect(
  mapStateToProps
)(SchedulingShowcases);

export default SchedulingShowcasesConnected;
