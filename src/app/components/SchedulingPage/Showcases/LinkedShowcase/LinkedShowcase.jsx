import React, { PropTypes } from 'react/lib/React';

import { FormattedMessage } from 'react-intl';

import ContainPicture from 'reactjs-beepeers-framework/components/General/Web/ContainPicture/ContainPicture';

import Link from '../../../../Routing/Link/Link';

import styles from './LinkedShowcase.css';

const LinkedShowcase = ({ showcase, pictureUrl }) => {
  const showcaseName = encodeURIComponent(showcase.displayName.replace(/\./g, '&#46;'));
  return (
    <Link to={`/convention/showcase/${showcase.profileId}/${showcaseName}`} className={styles.wrapper}>
      <div className={styles.pictureWrapper}>
        <div className={styles.pictureContent}>
          <ContainPicture pictureUrl={pictureUrl} />
          <div className={styles.moreInfo}>
            <FormattedMessage id={'BUTTON.MORE_INFOS'} />
          </div>
        </div>
      </div>
      <div className={styles.contentWrapper}>
        <div className={styles.showcaseName}>
          { showcase.displayName }
        </div>
        <div className={styles.showcaseActivity}>
          { showcase.name }
        </div>
      </div>
    </Link>
  );
};

LinkedShowcase.propTypes = {
  showcase: PropTypes.objectOf(PropTypes.any).isRequired,
  pictureUrl: PropTypes.string.isRequired,
};

export default LinkedShowcase;
