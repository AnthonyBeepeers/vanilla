import React, { PropTypes, Component } from 'react/lib/React';
import { connect } from 'react-redux';

import moment from 'moment';

import $ from 'jquery/dist/jquery.min';

import { FormattedMessage, injectIntl } from 'react-intl';

import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
import EventsFetcher from 'reactjs-beepeers-framework/components/Events/Fetchers/EventsFetcher/EventsFetcher';
// import FullLineEventsListAutoMoreButton from 'reactjs-beepeers-framework/components/Events/Renderers/Web/FullLineEventsListAutoMoreButton/FullLineEventsListAutoMoreButton';
import ActorsListFetcher from 'reactjs-beepeers-framework/components/Actors/Fetchers/ActorsListFetcher/ActorsListFetcher';
import FetchingIndicator from 'reactjs-beepeers-framework/components/General/Web/FetchingIndicator/FetchingIndicator';
// import TopButton from 'reactjs-beepeers-framework/components/General/Web/TopButton/TopButton';
import { DATE_FORMAT } from 'reactjs-beepeers-framework/constants/beepeersConfig';

import SchedulingContainer from '../SchedulingContainer/SchedulingContainer';
import LinkedEvent from '../../General/LinkedEvent/LinkedEvent';

import { CONVENTION_EVENT_ACTOR_TYPE, CONVENTION_CATEGORIES_EVENTS_CONV_LIST_KEY, CONVENTION_LOCATIONS_CONV_LIST_KEY, CONVENTION_ROUTES_LIST_KEY } from '../../../constants/actors';

import { PINK } from '../../../constants/colors';


import styles from './SchedulingByDate.css';
import HomeCaroussel from '../../HomePage/HomeCarousel/HomeCarousel';


class SchedulingByDate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      width: 0,
    };
  }

  componentWillMount() {
    this.updateDimensionsHandler = null;
    this.updateDimensions();
  }

  componentDidMount() {
    this.updateDimensionsHandler = () => this.updateDimensions();
    $(window).resize(this.updateDimensionsHandler);
  }

  componentWillUnmount() {
    if (typeof (this.updateDimensionsHandler) !== 'undefined' && this.updateDimensionsHandler !== null) {
      $(window).off('resize', this.updateDimensionsHandler);
    }
  }

  updateDimensions() {
    this.setState({
      width: $(window).width(),
    });
  }

  render() {
    const { match, appConfig } = this.props;
    // const { lang } = locales;
    // const langWithoutRegionCode = lang.toLowerCase().split(/[_-]+/)[0];

    const { params = {} } = match;

    const filterByCat = 'byCategory';
    const filterByRoute = 'byTheme';
    const { filter = null, filterChoiceId = null, query = null } = params;

    let addedEventsParams = {};
    if (filter !== null && filterChoiceId !== null) {
      if (filter === filterByCat) {
        addedEventsParams = {
          parentId: filterChoiceId,
        };
      }
      if (filter === filterByRoute) {
        addedEventsParams = {
          parent2Id: filterChoiceId,
        };
      }
    }
    if (query !== null) {
      addedEventsParams = {
        query,
      };
    }

    const afterRetrieveCategories = (categories, isFetchingCategories) => {
      const afterRetrieveLocations = (locations, isFetchingLocations) => {
        const afterRetrieveRoutes = (eventRoutes, eventRoutesIsFetching) => {
          if (!eventRoutesIsFetching) {
            const renderEventsItems = items => (
              items.map((item, index) => (
                <LinkedEvent event={item} key={index} className={styles.linkedEvent} />
              ))
            );

            const renderScheduledTime = (startDate = null, endDate = null) => {
              let finalDateString = null;
              if (startDate !== null) {
                finalDateString = moment(startDate).utcOffset(0).format('LT');
              }
              if (endDate !== null) {
                finalDateString = `${finalDateString} - ${moment(endDate).utcOffset(0).format('LT')}`;
              }
              if (finalDateString !== null) {
                return (
                  <span>
                    { finalDateString }
                  </span>
                );
              }
              return null;
            };

            const renderFunc = (eventsSections, isFetching) => {
              if (!isFetching && eventsSections.length > 0) {
                console.log('ALL');
                console.log(eventsSections);
                const eventsNodes = eventsSections.map((eventsSection, i) => {
                  const title = (<div className={styles.sectionTitle}>
                    <i className="fa fa-clock-o" /> {renderScheduledTime(eventsSection.items[0].startDate, eventsSection.items[0].endDate)}
                  </div>);
                  return (
                    <div key={i} className={styles.sectionWrapper}>
                      { title}
                      { renderEventsItems(eventsSection.items) }
                    </div>
                  ); });

                return (
                  <FullRow>
                    { eventsNodes }
                  </FullRow>
                );
              }

              if (!isFetching) {
                return (
                  <FullRow className={styles.emptyWrapper}>
                    <FormattedMessage id={'CONVENTION.SCHEDULING.NO_EVENT'} />
                  </FullRow>
                );
              }

              return null;
            };

            const days = appConfig.schedulingDates.map((date) => {
              const dateMoment = moment(date).utcOffset(0);

              const formattedDateMoment = dateMoment.format(DATE_FORMAT);

              const search = {
                profileTypeKey: CONVENTION_EVENT_ACTOR_TYPE,
                day: formattedDateMoment,
                displaySections: true,
                nbResult: 50,
                sectionMode: 'HOUR',
                ...addedEventsParams,
              };


              return (
                <div>
                  <div className={styles.dayTitle}> { moment(formattedDateMoment).format('LL') } </div>
                  <EventsFetcher
                    renderFunc={renderFunc}
                    search={search}
                  />
                </div>);
            });
            const createLayout = ds =>
               (<div className={styles.container}>
                 <div className={styles.contained}>
                   { ds[0] }
                 </div>
                 <div className={styles.contained}>
                   { ds[1] }
                 </div>
                 <div className={styles.contained}>
                   { ds[2] }
                 </div>
               </div>)
            ;
            return (
              <FullRow>
                { createLayout(days) }
              </FullRow>
            );
          }
          return (
            <FullRow>
              <FetchingIndicator
                loadingColor={PINK}
                className={styles.fetchingIndicator}
              />
            </FullRow>
          );
        };

        if (!isFetchingLocations) {
          console.log('isFetchingLocations');
          return (
            <ActorsListFetcher actorsListKey={CONVENTION_ROUTES_LIST_KEY} renderFunc={afterRetrieveRoutes} flattenSections />
          );
        }
        return (
          <FetchingIndicator
            loadingColor={PINK}
            className={styles.fetchingIndicator}
          />
        );
      };

      if (!isFetchingCategories) {
        console.log('isFetchingCategories');
        return (
          <ActorsListFetcher actorsListKey={CONVENTION_LOCATIONS_CONV_LIST_KEY} renderFunc={afterRetrieveLocations} flattenSections />
        );
      }
      return (
        <FetchingIndicator
          loadingColor={PINK}
          className={styles.fetchingIndicator}
        />
      );
    };

    return (
      <div>
        <HomeCaroussel />
        <SchedulingContainer>
          <ActorsListFetcher actorsListKey={CONVENTION_CATEGORIES_EVENTS_CONV_LIST_KEY} renderFunc={afterRetrieveCategories} flattenSections />
        </SchedulingContainer>
      </div>
    );
  }
}

SchedulingByDate.propTypes = {
  appConfig: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.string,
    PropTypes.array,
    PropTypes.object,
    PropTypes.number,
  ])).isRequired,
  match: PropTypes.objectOf(PropTypes.any).isRequired,
  /* locales: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ])).isRequired, */
};

const SchedulingByDateIntl = injectIntl(SchedulingByDate);

const mapStateToProps = state => ({
  appConfig: state.appConfig,
  history: state.history,
  locales: state.locales,
});

const SchedulingByDateIntlConnected = connect(
  mapStateToProps
)(SchedulingByDateIntl);

export default SchedulingByDateIntlConnected;
