import React, { PropTypes } from 'react/lib/React';

import moment from 'moment';

import { injectIntl } from 'react-intl';

import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
import ActorFetcherById from 'reactjs-beepeers-framework/components/Actors/Fetchers/ActorFetcherById/ActorFetcherById';
import ContainPicture from 'reactjs-beepeers-framework/components/General/Web/ContainPicture/ContainPicture';
import SubscriptionToggleButton from 'reactjs-beepeers-framework/components/Actors/Renderers/Web/SubscriptionToggleButton/SubscriptionToggleButton';
import SubscriptionContainer from 'reactjs-beepeers-framework/components/General/Web/SubscriptionContainer/SubscriptionContainer';

import Link from '../SchedulingPage/Link/Link';

import DefaultEventPicture from '../../assets/images/event_default.png';

import styles from './LinkedEventConv.css';

const LinkedEventConv = ({ event, className = '', intl }) => {
  const { formatMessage } = intl;

  const renderActorDisplayName = (actor) => {
    if (typeof (actor.profileId) !== 'undefined') {
      return (
        <span>
          { actor.displayName }
        </span>
      );
    }
    return null;
  };

  const renderRouteColor = (actor) => {
    if (typeof (actor.profileId) !== 'undefined') {
      const routeTitleString = formatMessage({ id: 'CONVENTION.SCHEDULING.EVENT.ROUTE.TITLE' });
      return (
        <div className={styles.route}>
          {routeTitleString} <div className={styles.routeColor} style={{ backgroundColor: actor.color }} /> {actor.displayName}
        </div>
      );
    }
    return null;
  };

  const renderScheduledTime = (startDate = null, endDate = null) => {
    let finalDateString = null;
    if (startDate !== null) {
      finalDateString = moment(startDate).utcOffset(0).format('LT');
    }
    if (endDate !== null) {
      finalDateString = `${finalDateString} - ${moment(endDate).utcOffset(0).format('LT')}`;
    }
    if (finalDateString !== null) {
      return (
        <span>
          { finalDateString }
        </span>
      );
    }
    return null;
  };

  let pictureNode = (
    <ContainPicture
      pictureUrl={DefaultEventPicture}
    />
  );
  if (typeof (event.originalUrl) !== 'undefined' && event.originalUrl.trim() !== '') {
    pictureNode = (
      <ContainPicture
        pictureUrl={`${event.originalUrl}&size=200&type=png`}
      />
    );
  } else if (typeof (event.thumbnailUrl) !== 'undefined' && event.thumbnailUrl.trim() !== '') {
    const toDelete = '&format=profile&device=web';
    const originalUrl = event.thumbnailUrl.substring(0, event.thumbnailUrl.length - toDelete.length);
    pictureNode = (
      <ContainPicture
        pictureUrl={`${originalUrl}&size=200&type=png`}
      />
    );
  } else if (typeof (event.coverUrl) !== 'undefined' && event.coverUrl.trim() !== '') {
    pictureNode = (
      <ContainPicture
        pictureUrl={`${event.coverUrl}&size=200&type=png`}
      />
    );
  }

  const eventName = encodeURIComponent(event.displayName.replace(/\./g, '&#46;'));

  const linkedEventNode = (
    <Link to={`/convention/event/${event.profileId}/${eventName}`} className={styles.wrapper}>
      <div className={styles.flexContainer}>
        <div className={styles.eventPicture}>
          { pictureNode }
        </div>
        <div className={styles.eventDescription}>
          <div className={styles.mobileEventWrapper}>
            <FullRow className={styles.mobileEventName}>
              {event.displayName}
            </FullRow>
            <FullRow>
              {
                typeof (event.parentId) !== 'undefined' ? (
                  <div className={styles.eventDetail}>
                    <i className="fa fa-tags" /> <ActorFetcherById actorId={event.parentId} renderFunc={renderActorDisplayName} />
                  </div>
                ) : null
              }
            </FullRow>
            <FullRow>
              {
                typeof (event.locationProfileId) !== 'undefined' ? (
                  <div className={styles.eventDetail}>
                    <i className="fa fa-map-marker" /> <ActorFetcherById actorId={event.locationProfileId} renderFunc={renderActorDisplayName} />
                  </div>
                ) : null
              }
            </FullRow>
            <FullRow>
              <i className="fa fa-clock-o" /> { renderScheduledTime(event.startDate, event.endDate) }
            </FullRow>
            <FullRow>
              {
                typeof (event.parent2Id) !== 'undefined' ? (
                  <div className={styles.eventDetail}>
                    <ActorFetcherById actorId={event.parent2Id} renderFunc={renderRouteColor} />
                  </div>
                ) : (<span>&nbsp;</span>)
              }
            </FullRow>
          </div>
        </div>
      </div>
      <div className={styles.hoverContainer}>
        &nbsp;
      </div>
    </Link>
  );

  const subscriptionToggleButton = (
    <SubscriptionToggleButton
      actorId={event.profileId}
      className={styles.subscriptionBtn}
    />
  );

  return (
    <SubscriptionContainer
      subscriptionToggleButton={subscriptionToggleButton}
      className={`${styles.subscriptionContainer} ${className}`}
    >
      { linkedEventNode }
    </SubscriptionContainer>
  );
};

LinkedEventConv.propTypes = {
  event: PropTypes.objectOf(PropTypes.any).isRequired,
  className: PropTypes.string,
  intl: PropTypes.objectOf(PropTypes.any).isRequired,
};

const LinkedEventConvIntl = injectIntl(LinkedEventConv);

export default LinkedEventConvIntl;
