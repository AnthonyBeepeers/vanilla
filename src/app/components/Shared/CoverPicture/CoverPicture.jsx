import React, { PropTypes } from 'react/lib/React';

import Picture from 'reactjs-beepeers-framework/components/General/Web/Picture/Picture';

import styles from './CoverPicture.css';

const CoverPicture = ({ pictureUrl, className = '', wrapperClassName = '', ...remainProps }) => (
  <div className={styles.container}>
    <Picture className={`${styles.cover} ${className}`} wrapperClassName={wrapperClassName} pictureUrl={pictureUrl} {...remainProps} />
    <div className={styles.overlay} />
  </div>
);

CoverPicture.propTypes = {
  pictureUrl: PropTypes.string.isRequired,
  className: PropTypes.string,
  wrapperClassName: PropTypes.string,
};

export default CoverPicture;
