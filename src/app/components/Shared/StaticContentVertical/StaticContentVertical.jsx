import React, { PropTypes } from 'react/lib/React';

import { FormattedMessage, injectIntl } from 'react-intl';

import { COVER_RATIO } from 'reactjs-beepeers-framework/constants/beepeersConfig';
import DefaultVideoPlayerThumbnail from 'reactjs-beepeers-framework/assets/images/video_player_default.jpg';

import Grid from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Grid/Grid';
import CenterContainer from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/CenterContainer/CenterContainer';
import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
import Row from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Row/Row';
import Col from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Col/Col';
import IconLabel from 'reactjs-beepeers-framework/components/General/Web/IconLabel/IconLabel';
import IconBlock from 'reactjs-beepeers-framework/components/General/Web/IconBlock/IconBlock';
// import ResponsiveContainer from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/ResponsiveContainer/ResponsiveContainer';
import CoverPicture from 'reactjs-beepeers-framework/components/General/Web/CoverPicture/CoverPicture';
import FullLineTitle from 'reactjs-beepeers-framework/components/General/Web/FullLineTitle/FullLineTitle';
import ResponsiveMediaGallery from 'reactjs-beepeers-framework/components/General/Web/ResponsiveMediaGallery/ResponsiveMediaGallery';
import ResponsiveMediaSlider from 'reactjs-beepeers-framework/components/General/Web/ResponsiveMediaSlider/ResponsiveMediaSlider';
import ActorShareButton from 'reactjs-beepeers-framework/components/Actors/Renderers/Web/ActorShareButton/ActorShareButton';
import VideoPlayer from 'reactjs-beepeers-framework/components/General/Web/VideoPlayer/VideoPlayer';


import {
 HTML_CONTENT, WEBSITE, PHONE, EMAIL, FAX,
 FACEBOOK, TWITTER, INSTAGRAM, YOUTUBE,
 SOUNDCLOUD,
 LINKEDIN, OTHER,
} from 'reactjs-beepeers-framework/constants/iconLabelTypes';

import { formatDateRange } from 'reactjs-beepeers-framework/constants/helpers';

import styles from './StaticContentVertical.css';

const StaticContentVertical = (
    {
      actor,
      contentColWidth = 10,
      className = '',
      disableGrid = false,
      disableTitle = false,
      disableCover = false,
      disableMedias = false,
      disableDetails = false,
      enableSubscription = false,
      enableVideoPlayer = false,
      videoPlayerThumbnail = DefaultVideoPlayerThumbnail,
      videoPlayerEnablePlayPicture = false,
      subscriptionToggleButton = null,
      enableShare = false,
      enableTicketMode = false,
      useSliderForMedias = false,
      modalSliderClassName = '',
      sliderClassName = '',
      enableTitleOnDetailsLinks = false,
      intl,
      particularCoverDimension = null,
    }
  ) => {
  const {
    coverUrl,
    displayName,
    description = null,
    description2 = null,
    email = null,
    website = null,
    blog = null,
    phone = null,
    fax = null,
    facebookUrl = null,
    twitterUrl = null,
    instagramUrl = null,
    youtubeUrl = null,
    startDate,
    endDate,
    fullDay = false,

    functionList = null,
  } = actor;

  const { formatMessage } = intl;

  let titleNode = null;
  if (!disableTitle && typeof (displayName) !== 'undefined' && displayName !== '') {
    titleNode = (
      <FullLineTitle title={displayName} intlActivated={false} className={styles.title} />
    );
  }

  let subscriptionContainer = null;
  if (enableSubscription) {
    /* // TODO : Missing router...
     let subscriptionToggleButtonNode = (
     <SubscriptionToggleButton
     actorId={actor.profileId}
     router={router}
     />
     );
     */
    let subscriptionToggleButtonNode = null;
    if (subscriptionToggleButton !== null) {
      subscriptionToggleButtonNode = subscriptionToggleButton;
    }

    subscriptionContainer = (
      <div className={styles.subscriptionContainer}>
        { subscriptionToggleButtonNode }
      </div>
    );
  }

  let shareContainer = null;
  if (enableShare && typeof (actor.shareUrl) !== 'undefined' && actor.shareUrl !== '') {
    shareContainer = (
      <div className={styles.shareContainer}>
        <ActorShareButton
          actor={actor}
        />
      </div>
    );
  }

  const headerNode = (
    <div className={styles.header}>
      { titleNode }
      { subscriptionContainer }
      { shareContainer }
    </div>
  );

  let coverNode = null;
  if (!disableCover && typeof (coverUrl) !== 'undefined' && coverUrl !== '') {
    let width = 1920;
    let height = Math.round(width / COVER_RATIO);
    if (particularCoverDimension !== null) {
      width = particularCoverDimension.width;
      height = particularCoverDimension.height;
    }

    coverNode = (
      <div className={styles.coverWrapper}>
        <div className={styles.coverContent}>
          <CoverPicture pictureUrl={`${coverUrl}&width=${width}&height=${height}`} />
        </div>
      </div>
    );
  }

  let datesNode = null;
  const formattedDates = formatDateRange(startDate, endDate, !fullDay);
  if (formattedDates !== null) {
    datesNode = (
      <FullLineTitle title={formattedDates} intlActivated={false} className={styles.dates} />
    );
  }

  let abstractNode = null;
  if (description !== null && description.trim() !== '') {
    abstractNode = (
      <IconLabel
        className={styles.abstract}
        type={HTML_CONTENT}
        label={description}
      />
    );
  }

  let descriptionNode = null;
  if (description2 !== null && description2.trim() !== '') {
    descriptionNode = (
      <IconLabel
        className={styles.description}
        type={HTML_CONTENT}
        label={description2}
      />
    );
  }

  let phoneNode = null;
  if (phone !== null && phone.trim() !== '') {
    let otherParams = {};
    if (enableTitleOnDetailsLinks) {
      otherParams = {
        title: formatMessage({ id: 'STATIC_CONTENT_VERTICAL.PHONE' }),
      };
    }
    phoneNode = (
      <IconLabel
        type={PHONE}
        label={phone}
        {...otherParams}
      />
    );
  }

  let faxNode = null;
  if (fax !== null && fax.trim() !== '') {
    let otherParams = {};
    if (enableTitleOnDetailsLinks) {
      otherParams = {
        title: formatMessage({ id: 'STATIC_CONTENT_VERTICAL.FAX' }),
      };
    }
    faxNode = (
      <IconLabel
        type={FAX}
        label={fax}
        {...otherParams}
      />
    );
  }

  let emailNode = null;
  if (email !== null && email.trim() !== '') {
    let otherParams = {};
    if (enableTitleOnDetailsLinks) {
      otherParams = {
        title: formatMessage({ id: 'STATIC_CONTENT_VERTICAL.EMAIL' }),
      };
    }
    emailNode = (
      <IconLabel
        type={EMAIL}
        label={email}
        {...otherParams}
      />
    );
  }

  let websiteNode = null;
  if (website !== null && website.trim() !== '') {
    let otherParams = {};
    if (enableTitleOnDetailsLinks) {
      otherParams = {
        title: formatMessage({ id: 'STATIC_CONTENT_VERTICAL.WEBSITE' }),
      };
    }
    websiteNode = (
      <IconLabel
        type={WEBSITE}
        label={website}
        {...otherParams}
      />
    );
  }

  let blogNode = null;
  if (blog !== null && blog.trim() !== '') {
    if (enableTicketMode) {
      blogNode = (
        <Row center="xs">
          <Col xs={6} sm={3} className={styles.ticketWrapper}>
            <a href={blog} target="_blank" rel="noopener noreferrer">
              <i className={`fa fa-ticket ${styles.ticketIcon}`} aria-hidden="true" />
              <FormattedMessage
                id="TICKET_BUTTON.BOOK_EVENT"
                defaultMessage="TICKET_BUTTON.BOOK_EVENT"
              />
            </a>
          </Col>
        </Row>
      );
    } else {
      let otherParams = {};
      if (enableTitleOnDetailsLinks) {
        otherParams = {
          title: formatMessage({ id: 'STATIC_CONTENT_VERTICAL.BLOG' }),
        };
      }
      blogNode = (
        <IconLabel
          type={WEBSITE}
          label={blog}
          {...otherParams}
        />
      );
    }
  }

  let facebookNode = null;
  if (facebookUrl !== null && facebookUrl.trim() !== '') {
    facebookNode = (
      <Col xs={2} sm={1} className={styles.iconBlock}>
        <IconBlock
          type={FACEBOOK}
          label={facebookUrl}
        />
      </Col>
    );
  }

  let twitterNode = null;
  if (twitterUrl !== null && twitterUrl.trim() !== '') {
    twitterNode = (
      <Col xs={2} sm={1} className={styles.iconBlock}>
        <IconBlock
          type={TWITTER}
          label={twitterUrl}
        />
      </Col>
    );
  }

  let instagramNode = null;
  if (instagramUrl !== null && instagramUrl.trim() !== '') {
    instagramNode = (
      <Col xs={2} sm={1} className={styles.iconBlock}>
        <IconBlock
          type={INSTAGRAM}
          label={instagramUrl}
        />
      </Col>
    );
  }

  let youtubeNode = null;
  if (!enableVideoPlayer && youtubeUrl !== null && youtubeUrl.trim() !== '') {
    youtubeNode = (
      <Col xs={2} sm={1} className={styles.iconBlock}>
        <IconBlock
          type={YOUTUBE}
          label={youtubeUrl}
        />
      </Col>
    );
  }

  let youtubeVideoPlayerNode = null;
  let youtubeVideoFunction = false;
  let ytChannelNode = null;
  let soundCloudPlaylistPlayerNode = null;
  const knownFunctions = [];
  const otherFunctions = [];
  if (functionList !== null && functionList.length > 0) {
    functionList.forEach((functionItem) => {
      switch (functionItem.identifier) {
        case 'FB_PAGE':
        case 'TWITTER':
        case 'INSTAGRAM':
          break;
        case 'YOUTUBE_VIDEO': {
          youtubeVideoFunction = true;
          if (enableVideoPlayer) {
            let youtubeVideoUrl = null;
            if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
              functionItem.parameterList.forEach((parameterItem) => {
                if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                  youtubeVideoUrl = parameterItem.value;
                }
              });
            }
            if (youtubeVideoUrl !== null) {
              youtubeVideoPlayerNode = (
                <Row center="xs">
                  <Col xs={10} sm={8}>
                    <VideoPlayer
                      videoThumbnailUrl={videoPlayerThumbnail}
                      disablePlayPictureUrl={!videoPlayerEnablePlayPicture}
                      videoUrl={youtubeVideoUrl}
                    />
                  </Col>
                </Row>
              );
            }
          }
          break;
        }
        case 'SOUNDCLOUD': {
          let soundcloudUrl = null;
          if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
            functionItem.parameterList.forEach((parameterItem) => {
              if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                soundcloudUrl = parameterItem.value;
              }
            });
          }
          if (soundcloudUrl !== null) {
            const soundcloudNode = (
              <Col key="SOUNDCLOUD" xs={2} sm={1} className={styles.iconBlock}>
                <IconBlock
                  type={SOUNDCLOUD}
                  label={soundcloudUrl}
                />
              </Col>
            );
            knownFunctions.push(soundcloudNode);
          }
          break;
        }
        case 'SOUNDCLOUD_PLAYLIST': {
          let soundcloudPlaylistUrl = null;
          if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
            functionItem.parameterList.forEach((parameterItem) => {
              if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                soundcloudPlaylistUrl = parameterItem.value;
              }
            });
          }
          if (soundcloudPlaylistUrl !== null) {
            soundCloudPlaylistPlayerNode = (
              <Row center="xs">
                <Col xs={12} sm={9}>
                  <div className={styles.soundcloudPlaylistPlayerWrapper}>
                    <div className={styles.soundcloudPlaylistPlayerContent}>
                      <iframe
                        frameBorder="0"
                        scrolling="no"
                        marginHeight="0"
                        marginWidth="0"
                        src={soundcloudPlaylistUrl}
                        className={styles.soundcloudPlaylistPlayerIFrameWrapper}
                      />
                    </div>
                  </div>
                </Col>
              </Row>
            );
          }
          break;
        }
        case 'LINKEDIN_PROFILE': {
          let linkedInUrl = null;
          if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
            functionItem.parameterList.forEach((parameterItem) => {
              if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                linkedInUrl = parameterItem.value;
              }
            });
          }
          if (linkedInUrl !== null) {
            const linkedInNode = (
              <Col key="LINKEDIN_PROFILE" xs={2} sm={1} className={styles.iconBlock}>
                <IconBlock
                  type={LINKEDIN}
                  label={linkedInUrl}
                />
              </Col>
            );
            knownFunctions.push(linkedInNode);
          }
          break;
        }
        case 'LINKEDIN_PAGE': {
          let linkedInUrl = null;
          if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
            functionItem.parameterList.forEach((parameterItem) => {
              if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                linkedInUrl = parameterItem.value;
              }
            });
          }
          if (linkedInUrl !== null) {
            const linkedInNode = (
              <Col key="LINKEDIN_PAGE" xs={2} sm={1} className={styles.iconBlock}>
                <IconBlock
                  type={LINKEDIN}
                  label={linkedInUrl}
                />
              </Col>
            );
            knownFunctions.push(linkedInNode);
          }
          break;
        }
        case 'YOUTUBE_CHANNEL': {
          let ytChannelUrl = null;
          if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
            functionItem.parameterList.forEach((parameterItem) => {
              if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                ytChannelUrl = parameterItem.value;
              }
            });
          }
          if (ytChannelUrl !== null) {
            ytChannelNode = (
              <Col key="YOUTUBE_CHANNEL" xs={2} sm={1} className={styles.iconBlock}>
                <IconBlock
                  type={YOUTUBE}
                  label={ytChannelUrl}
                />
              </Col>
            );
            knownFunctions.push(ytChannelNode);
          }
          break;
        }
        case 'WEB': {
          let webUrl = null;
          if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
            functionItem.parameterList.forEach((parameterItem) => {
              if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                webUrl = parameterItem.value;
              }
            });
          }
          if (webUrl !== null) {
            const webNode = (
              <Col key="WEB" xs={2} sm={1} className={styles.iconBlock}>
                <IconBlock
                  type={WEBSITE}
                  label={webUrl}
                />
              </Col>
            );
            knownFunctions.push(webNode);
          }
          break;
        }
        default: {
          if (typeof (functionItem.pictureUrl) !== 'undefined' && functionItem.pictureUrl.trim() !== '') {
            let otherUrl = null;
            if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
              functionItem.parameterList.forEach((parameterItem) => {
                if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                  otherUrl = parameterItem.value;
                }
              });
            }
            if (otherUrl !== null) {
              const otherNode = (
                <Col key={functionItem.identifier} xs={2} sm={1} className={styles.iconBlock}>
                  <IconBlock
                    type={OTHER}
                    label={otherUrl}
                    typePicture={functionItem.pictureUrl}
                  />
                </Col>
              );
              otherFunctions.push(otherNode);
            }
          }
        }
      }
    });
  }
  if (youtubeNode !== null && ytChannelNode !== null && !youtubeVideoFunction) {
    youtubeNode = null;
  }

  let detailsLinks = (
    <div className={styles.details}>
      { emailNode }
      { websiteNode }
      { blogNode }
      { phoneNode }
      { faxNode }
    </div>
  );
  if (enableTicketMode) {
    detailsLinks = (
      <div className={styles.details}>
        { emailNode }
        { websiteNode }
        { phoneNode }
        { faxNode }
      </div>
    );
  }

  const detailsBlocks = (
    <FullRow>
      <Row center="xs">
        { facebookNode }
        { twitterNode }
        { instagramNode }
        { youtubeNode }
        { knownFunctions }
      </Row>
      <Row center="xs">
        { otherFunctions }
      </Row>
    </FullRow>
  );

  let ticketButtonNode = null;
  if (enableTicketMode && blogNode !== null) {
    ticketButtonNode = blogNode;
  }

  let detailsNode = null;
  if (!disableDetails) {
    detailsNode = (
      <FullRow>
        { detailsLinks }
        { ticketButtonNode }
        { youtubeVideoPlayerNode }
        { soundCloudPlaylistPlayerNode }
        { detailsBlocks }
      </FullRow>
    );
  }

  let mediasNode = null;
  if (!disableMedias && typeof (actor.medias) !== 'undefined') {
    if (useSliderForMedias) {
      mediasNode = (
        <ResponsiveMediaSlider
          className={styles.contentNode}
          sliderClassName={`${styles.slider} ${sliderClassName}`}
          modalClassName={`${styles.modalSlider} ${modalSliderClassName}`}
          medias={actor.medias}
          nbToShow={4}
          sliderId={`STATIC_CONTENT_MEDIAS_SLIDER_${actor.profileId}`}
          xs={{ height: '18vw' }}
          sm={{ height: '90px' }}
          md={{ height: '124px' }}
          lg={{ height: '155px' }}
        />
      );
    } else {
      mediasNode = (
        <ResponsiveMediaGallery
          className={styles.staticMediaGallery}
          modalClassName={styles.staticModalMediaGallery}
          galleryId={`staticMediaGallery_${actor.profileId}`}
          medias={actor.medias}
          nbPerRows={4}
          xs={{ height: '18vw' }}
          sm={{ height: '90px' }}
          md={{ height: '124px' }}
          lg={{ height: '155px' }}
        />
      );
    }
  }

  const allContentNode = (
    <div className={`${styles.wrapper} ${className}`}>
      { headerNode }
      <CenterContainer width={contentColWidth}>
        { coverNode }
        { datesNode }
        <FullRow className={styles.infos}>
          { abstractNode }
          { descriptionNode }
          { detailsNode }
        </FullRow>
        { mediasNode }
      </CenterContainer>
    </div>
  );
  if (disableGrid) {
    return allContentNode;
  }

  return (
    <Grid>
      { allContentNode }
    </Grid>
  );
};

StaticContentVertical.propTypes = {
  actor: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.bool,
    PropTypes.number,
    PropTypes.object,
    PropTypes.string,
  ])).isRequired,
  contentColWidth: PropTypes.number,
  className: PropTypes.string,
  disableGrid: PropTypes.bool,
  disableTitle: PropTypes.bool,
  disableCover: PropTypes.bool,
  disableMedias: PropTypes.bool,
  disableDetails: PropTypes.bool,
  enableSubscription: PropTypes.bool,
  enableVideoPlayer: PropTypes.bool,
  videoPlayerThumbnail: PropTypes.string,
  videoPlayerEnablePlayPicture: PropTypes.bool,
  subscriptionToggleButton: PropTypes.node,
  enableShare: PropTypes.bool,
  enableTicketMode: PropTypes.bool,
  useSliderForMedias: PropTypes.bool,
  modalSliderClassName: PropTypes.string,
  sliderClassName: PropTypes.string,
  enableTitleOnDetailsLinks: PropTypes.bool,
  intl: PropTypes.objectOf(PropTypes.any).isRequired,
  particularCoverDimension: PropTypes.objectOf(PropTypes.number),
};

const StaticContentVerticalIntl = injectIntl(StaticContentVertical);

export default StaticContentVerticalIntl;
