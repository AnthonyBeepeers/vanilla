import React, { PropTypes } from 'react/lib/React';

import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
import ResponsiveGallery from 'reactjs-beepeers-framework/components/Layout/Web/ResponsiveGallery/ResponsiveGallery';

import LinkedArtist from '../LinkedArtist/LinkedArtist';

const ArtistsGallery = ({ artists }) => {
  const renderItemFunc = artist => (
    <LinkedArtist artist={artist} />
  );

  return (
    <FullRow>
      <div>
        <ResponsiveGallery
          list={artists}
          renderItemFunc={renderItemFunc}
          nbPerRows={{ xs: 2, sm: 3 }}
          xs={{ height: '160px' }}
          sm={{ height: '190px' }}
          md={{ height: '224px' }}
          lg={{ height: '245px' }}
        />
      </div>
    </FullRow>
  );
};

ArtistsGallery.propTypes = {
  artists: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default ArtistsGallery;
