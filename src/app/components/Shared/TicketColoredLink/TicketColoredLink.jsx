import React, { PropTypes } from 'react/lib/React';

import { Link } from 'react-router-dom';
import { injectIntl } from 'react-intl';
import CoverPicture from 'reactjs-beepeers-framework/components/General/Web/CoverPicture/CoverPicture';

import styles from './TicketColoredLink.css';

const TicketColoredLink = ({
                             pictureUrl = null,
                             color = '#ffe000',
                             linkPath,
                             name,
                             toDisplayName = true,
                             price,
                             buttonLabel,
                             intl,
                             intlActivated = true,
                             className = '',
                             ...remainProps }) => {
  const { formatMessage } = intl;
  let finalButtonLabel = '';
  let finalPrice = '';

  if (buttonLabel !== '') {
    finalButtonLabel = buttonLabel;
    if (intlActivated) {
      finalButtonLabel = formatMessage({
        id: buttonLabel,
        defaultMessage: buttonLabel,
      });
    }
  }
  if (price !== '' && price !== 'undefined') {
    finalPrice = price;
  }

  const linkContent = (
    <div className={styles.linkContent}>
      <div className={styles.linkContentText}>
        {(toDisplayName) ? name : ''}
      </div>
      <div className={styles.linkContentPrice}>
        { finalPrice }
      </div>
      <div className={styles.linkContentButton}>
        <div className={styles.link}>
          { finalButtonLabel }
        </div>
      </div>
    </div>
  );

  const fullClassName = `${styles.container} ${className}`;

  let linkNode = null;
  if (pictureUrl !== null) {
    linkNode = (
      <div className={fullClassName} {...remainProps}>
        <CoverPicture className={styles.coverPicture} pictureUrl={pictureUrl} />
        { linkContent }
      </div>
    );
  } else {
    linkNode = (
      <div className={fullClassName} {...remainProps} style={{ backgroundColor: color }}>
        { linkContent }
      </div>
    );
  }

  if (linkPath.trim() !== '') {
    if (linkPath.toLowerCase().startsWith('http') || linkPath.toLowerCase().startsWith('ftp')) {
      return (
        <a href={linkPath} className={fullClassName} target="_blank" rel="noopener noreferrer">
          { linkNode }
        </a>
      );
    }
    return (
      <Link to={linkPath} className={fullClassName}>
        { linkNode }
      </Link>
    );
  }
  return linkNode;
};

TicketColoredLink.propTypes = {
  pictureUrl: PropTypes.string,
  color: PropTypes.string,
  linkPath: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  toDisplayName: PropTypes.bool,
  price: PropTypes.string,
  buttonLabel: PropTypes.string.isRequired,
  intl: PropTypes.shape({
    formatMessage: PropTypes.func,
  }).isRequired,
  pictoUrl: PropTypes.string,
  intlActivated: PropTypes.bool,
  className: PropTypes.string,
};

const CoverPictureLinkIntl = injectIntl(TicketColoredLink);

export default CoverPictureLinkIntl;
