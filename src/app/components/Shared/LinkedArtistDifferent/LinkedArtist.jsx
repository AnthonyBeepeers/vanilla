import React, { PropTypes } from 'react/lib/React';
import { connect } from 'react-redux';

import ActorPortraitTopPicture from 'reactjs-beepeers-framework/components/Actors/Renderers/Web/ActorPortraitTopPictureDifferent/ActorPortraitTopPicture';
import LinkedActor from 'reactjs-beepeers-framework/components/Actors/Renderers/Web/LinkedActor/LinkedActor';
// import SubscriptionToggleButton from 'reactjs-beepeers-framework/components/Actors/Renderers/Web/SubscriptionToggleButton/SubscriptionToggleButton';
// import SubscriptionContainer from 'reactjs-beepeers-framework/components/General/Web/SubscriptionContainer/SubscriptionContainer';
// import FullLineDecoratedTitle from 'reactjs-beepeers-framework/components/General/Web/FullLineDecoratedTitle/FullLineDecoratedTitle';
// import ActorShareButton from 'reactjs-beepeers-framework/components/Actors/Renderers/Web/ActorShareButton/ActorShareButton';
// import ShareContainer from 'reactjs-beepeers-framework/components/General/Web/ShareContainer/ShareContainer';

import ArtistDefaultPicture from '../../../assets/images/artist_default.png';
// import seeMorePictureUrl from '../../../assets/images/see_more.png';

import styles from './LinkedArtist.css';

const LinkedArtist = ({ artist /* , history = null */ }) => {
  const linkedActorNode = (
    <LinkedActor
      baseUrl="/artist/"
      nameInUrl
      actor={artist}
    >
      <div className={styles.artistDescription}>
        <ActorPortraitTopPicture
          actor={artist}
          defaultPictureUrl={ArtistDefaultPicture}
          enableDescription={false}
          enableLabel
        />
      </div>
      {/* <div className={styles.artistSeeMoreWrapper}> */}
      {/* <FullLineDecoratedTitle */}
      {/* className={styles.artistSeeMore} */}
      {/* title="ARTIST.SEE_MORE" */}
      {/* decoratorUrl={seeMorePictureUrl} */}
      {/* /> */}
      {/* </div> */}
    </LinkedActor>
  );

  const finalActorNode = linkedActorNode;
/*
  if (history !== null) {
    const subscriptionToggleButton = (
      <SubscriptionToggleButton
        history={history}
        actorId={artist.profileId}
        className={styles.subscriptionBtn}
      />
    );

    finalActorNode = (
      <SubscriptionContainer
        subscriptionToggleButton={subscriptionToggleButton}
      >
        { finalActorNode }
      </SubscriptionContainer>
    );
  }
*/
/*
  if (typeof (artist.shareUrl) !== 'undefined') {
    const shareButton = (
      <ActorShareButton
        actor={artist}
      />
    );

    finalActorNode = (
      <ShareContainer
        shareButton={shareButton}
      >
        { finalActorNode }
      </ShareContainer>
    );
  }
*/
  return (
    <div className={styles.artistWrapper}>
      { finalActorNode }
    </div>
  );
};

LinkedArtist.propTypes = {
  // history: PropTypes.objectOf(PropTypes.any),
  artist: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.bool,
    PropTypes.number,
    PropTypes.object,
    PropTypes.string,
  ])).isRequired,
};

const mapStateToProps = state => ({
  history: state.history,
});

const LinkedArtistConnected = connect(
  mapStateToProps
)(LinkedArtist);

export default LinkedArtistConnected;
