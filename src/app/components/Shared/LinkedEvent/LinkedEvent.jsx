import React, { PropTypes } from 'react/lib/React';
import { connect } from 'react-redux';

// import LinkedActor from 'reactjs-beepeers-framework/components/Actors/Renderers/Web/LinkedActor/LinkedActor';
import LinkedActorVertical from 'reactjs-beepeers-framework/components/Actors/Renderers/Web/LinkedActorVertical/LinkedActorVertical';
import SubscriptionToggleButton from 'reactjs-beepeers-framework/components/Actors/Renderers/Web/SubscriptionToggleButton/SubscriptionToggleButton';
import SubscriptionContainer from 'reactjs-beepeers-framework/components/General/Web/SubscriptionContainer/SubscriptionContainer';
// import ExternalLinkContainer from 'reactjs-beepeers-framework/components/General/Web/ExternalLinkContainer/ExternalLinkContainer';
// import ActorShareButton from 'reactjs-beepeers-framework/components/Actors/Renderers/Web/ActorShareButton/ActorShareButton';
// import ShareContainer from 'reactjs-beepeers-framework/components/General/Web/ShareContainer/ShareContainer';

import EventDefaultPicture from '../../../assets/images/news_default.png';

import styles from './LinkedEvent.css';

const LinkedEvent = ({ event, history = null, displayExternalLink = true, enableSubscription = false }) => {
  const linkedEventNode = (
    <LinkedActorVertical
      actor={event}
      defaultPictureUrl={EventDefaultPicture}
      className={styles.event}
      isEvent
      displayEndDate={false}
      displayExternalLink={displayExternalLink}
      linkIconClassName="fa fa-ticket"
      baseUrl="/scheduling/event/"
      nameInUrl
    />
  );

  let finalEventNode = linkedEventNode;

  if (enableSubscription && history !== null) {
    const subscriptionToggleButton = (
      <SubscriptionToggleButton
        history={history}
        actorId={event.profileId}
        className={styles.subscriptionBtn}
      />
    );

    finalEventNode = (
      <SubscriptionContainer
        subscriptionToggleButton={subscriptionToggleButton}
      >
        { finalEventNode }
      </SubscriptionContainer>
    );
  }

/*
  if (typeof (event.shareUrl) !== 'undefined' && event.shareUrl !== '') {
    const shareButton = (
      <ActorShareButton
        actor={event}
      />
    );

    finalEventNode = (
      <ShareContainer
        shareButton={shareButton}
      >
        { finalEventNode }
      </ShareContainer>
    );
  }
*/

  // if (typeof (event.blog) !== 'undefined' && event.blog !== '') {
  //   finalEventNode = (
  //     <ExternalLinkContainer linkPath={event.blog} linkIconClassName="fa fa-ticket">
  //       { finalEventNode }
  //     </ExternalLinkContainer>
  //   );
  // }

  return (
    <div className={styles.eventWrapper}>
      { finalEventNode }
    </div>
  );
};

LinkedEvent.propTypes = {
  history: PropTypes.objectOf(PropTypes.any),
  event: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.bool,
    PropTypes.number,
    PropTypes.object,
    PropTypes.string,
  ])).isRequired,
  displayExternalLink: PropTypes.bool,
  enableSubscription: PropTypes.bool,
};

const mapStateToProps = state => ({
  history: state.history,
});

const LinkedEventConnected = connect(
  mapStateToProps
)(LinkedEvent);

export default LinkedEventConnected;
