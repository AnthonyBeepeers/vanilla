import React, { PropTypes } from 'react/lib/React';

import Link from 'react-router-dom/Link';

import { injectIntl } from 'react-intl';

import ContainPicture from 'reactjs-beepeers-framework/components/General/Web/ContainPicture/ContainPicture';

import CoverPicture from '../CoverPicture/CoverPicture';

import styles from './CoverPictureLink.css';

const CoverPictureLink = ({ pictureUrl, linkPath, linkName, intl, intlActivated = true, pictoUrl = '', className = '', ...remainProps }) => {
  const { formatMessage } = intl;
  let finalLinkName = '';
  if (linkName !== '') {
    finalLinkName = linkName;
    if (intlActivated) {
      finalLinkName = formatMessage({
        id: linkName,
        defaultMessage: linkName,
      });
    }
  }

  let linkContent = (
    <div className={styles.linkContent}>
      { finalLinkName }
    </div>
  );

  if (pictoUrl !== '') {
    linkContent = (
      <div className={styles.linkContent}>
        <div className={styles.pictoPicture}>
          <ContainPicture pictureUrl={pictoUrl} />
        </div>
        <div className={styles.linkUnderPicto}>
          <div className={styles.linkUnderPictoText}>
            { finalLinkName }
          </div>
        </div>
      </div>
    );
  }

  let linkNode = linkContent;
  if (linkPath.trim() !== '') {
    linkNode = (
      <Link to={linkPath} className={styles.link}>
        { linkContent }
        <div className={styles.overlay} />
      </Link>
    );

    if (linkPath.toLowerCase().startsWith('http') || linkPath.toLowerCase().startsWith('ftp')) {
      linkNode = (
        <a href={linkPath} className={styles.link} target="_blank" rel="noopener noreferrer">
          { linkContent }
        </a>
      );
    }
  }

  const fullClassName = `${styles.container} ${className} ${styles.link}`;

  return (
    <a href={linkPath} className={fullClassName} {...remainProps}>
      <CoverPicture className={styles.coverPicture} pictureUrl={pictureUrl} />
      { linkNode }
    </a>
  );
};

CoverPictureLink.propTypes = {
  pictureUrl: PropTypes.string.isRequired,
  linkPath: PropTypes.string.isRequired,
  linkName: PropTypes.string.isRequired,
  intl: PropTypes.shape({
    formatMessage: PropTypes.func,
  }).isRequired,
  pictoUrl: PropTypes.string,
  intlActivated: PropTypes.bool,
  className: PropTypes.string,
};

const CoverPictureLinkIntl = injectIntl(CoverPictureLink);

export default CoverPictureLinkIntl;
