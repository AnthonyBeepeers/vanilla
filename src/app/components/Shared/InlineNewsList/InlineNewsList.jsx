import React, { PropTypes } from 'react/lib/React';

import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
import ResponsiveContainer from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/ResponsiveContainer/ResponsiveContainer';
import Row from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Row/Row';
import Col from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Col/Col';

import NewsListFetcher from 'reactjs-beepeers-framework/components/Feed/Fetchers/NewsListFetcher/NewsListFetcher';
import ActorNewsListFetcherById from 'reactjs-beepeers-framework/components/Feed/Fetchers/ActorNewsListFetcherById/ActorNewsListFetcherById';

import { NEWS_FEED_ITEM_TYPE } from 'reactjs-beepeers-framework/constants/feed';

import styles from './InlineNewsList.css';

const InlineNewsList = ({ renderFunc, nbNews = 20, overflow = true, className = '', actorId = null }) => {
  const renderNewsListFunc = (items) => {
    let finalItems = items;
    if (!overflow) {
      finalItems = items.slice(0, nbNews);
    }

    const itemsCols = finalItems.map((item, i) => (
      <Col key={i} xs={12} sm={3}>
        <ResponsiveContainer
          xs={{ height: '100%', width: '100%' }}
          sm={{ height: '176px', width: '100%' }}
          md={{ height: '232px', width: '100%' }}
          lg={{ height: '284px', width: '100%' }}
        >
          {renderFunc(item)}
        </ResponsiveContainer>
      </Col>
    ));

    return (
      <FullRow>
        <div className={`${styles.list} ${className}`}>
          <Row className={styles.feed_row} between="xs">
            {itemsCols}
          </Row>
        </div>
      </FullRow>
    );
  };

  if (actorId !== null) {
    return (
      <ActorNewsListFetcherById
        nbNews={nbNews}
        actorId={actorId}
        renderFunc={renderNewsListFunc}
        overflow={overflow}
      />
    );
  }

  const addedParams = {
    postKeys: [
      NEWS_FEED_ITEM_TYPE,
    ],
  };

  return (
    <NewsListFetcher
      nbNews={nbNews}
      addedParams={addedParams}
      renderFunc={renderNewsListFunc}
    />
  );
};

InlineNewsList.propTypes = {
  renderFunc: PropTypes.func.isRequired,
  className: PropTypes.string,
  nbNews: PropTypes.number,
  overflow: PropTypes.bool,
  actorId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

export default InlineNewsList;
