import React, { PropTypes } from 'react/lib/React';

import styles from './SocialIcon.css';

const SocialIcon = ({ iconClassName, url }) => (
  <div className={styles.wrapper}>
    <div className={styles.content}>
      <a className={styles.socialIconLink} href={url} target="_blank" rel="noopener noreferrer">
        <i className={`${iconClassName} ${styles.socialIcon}`} />
      </a>
    </div>
  </div>
);

SocialIcon.propTypes = {
  iconClassName: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
};

export default SocialIcon;
