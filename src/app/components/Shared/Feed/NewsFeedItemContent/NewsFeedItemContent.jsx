import React, { PropTypes } from 'react/lib/React';

import { withRouter } from 'react-router-dom';

import { FormattedMessage } from 'react-intl';

import Grid from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Grid/Grid';
import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
import LinkedNewsItem from 'reactjs-beepeers-framework/components/Feed/Renderers/Web/LinkedNewsItem/LinkedNewsItem';
import NewsItemPortraitTopPicture from 'reactjs-beepeers-framework/components/Feed/Renderers/Web/NewsItemPortraitTopPicture/NewsItemPortraitTopPicture';
import FullLineDecoratedTitle from 'reactjs-beepeers-framework/components/General/Web/FullLineDecoratedTitle/FullLineDecoratedTitle';
import GoBackButton from 'reactjs-beepeers-framework/components/General/Web/GoBackButton/GoBackButton';

import CenterContainer from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/CenterContainer/CenterContainer';
import NewsItemComplete from 'reactjs-beepeers-framework/components/Feed/Renderers/Web/NewsItemComplete/NewsItemComplete';
import NewsItemFetcher from 'reactjs-beepeers-framework/components/Feed/Fetchers/NewsItemFetcher/NewsItemFetcher';
import InlineNewsList from 'reactjs-beepeers-framework/components/Feed/Renderers/Web/InlineNewsList/InlineNewsList';
import FullLineMoreNewsButton from 'reactjs-beepeers-framework/components/Feed/Renderers/Web/FullLineMoreNewsButton/FullLineMoreNewsButton';

import { STATIC_CONTENT_WIDTH } from 'reactjs-beepeers-framework/constants/contentPages';

import HoloColoredTitle from '../../../General/HoloColoredTitle/HoloColoredTitle';

import NewsDefaultPicture from '../../../../assets/images/news_default.png';
import seeMorePictureUrl from '../../../../assets/images/see_more.png';
import MoreBtnPicture from '../../../../assets/images/more_btn.png';
import DefaultVideoPlayerThumbnail from '../../../../assets/images/video_player_default.png';

import { ORANGE } from '../../../../constants/colors';

import styles from './NewsFeedItemContent.css';

const NewsFeedItem = ({ match }) => {
  const { params = {} } = match;

  const renderNewsInlineItemFunc = item => (
    <div className={styles.newsItemWrapper}>
      <div className={styles.newsItemContent}>
        <LinkedNewsItem baseUrl="/feed/" item={item}>
          <div className={styles.newsInlineItemDescription}>
            <NewsItemPortraitTopPicture
              item={item}
              playPictureUrl={DefaultVideoPlayerThumbnail}
              defaultPictureUrl={NewsDefaultPicture}
              disableCoverLink
            />
          </div>
          <div className={styles.newsItemSeeMoreWrapper}>
            <FullLineDecoratedTitle
              className={styles.newsItemSeeMore}
              title="SHARED.NEWS.SEE_MORE"
              decoratorUrl={seeMorePictureUrl}
            />
          </div>
        </LinkedNewsItem>
      </div>
    </div>
  );

  const renderNewsItemFunc = (item) => {
    if (item !== null) {
      return (
        <FullRow>
          <CenterContainer width={STATIC_CONTENT_WIDTH}>
            <div className={styles.backLink}>
              <GoBackButton>
                <i className="fa fa-arrow-circle-left" /> <FormattedMessage id={'SHARED.NEWS.BACK'} />
              </GoBackButton>
            </div>
            <NewsItemComplete
              item={item}
              playPictureUrl={DefaultVideoPlayerThumbnail}
              className={styles.newsItemDescription}
              disableComments
            />
          </CenterContainer>
        </FullRow>
      );
    }
    return null;
  };

  let newsItemFetcherNode = null;
  if (typeof (params.itemId) !== 'undefined') {
    newsItemFetcherNode = (
      <NewsItemFetcher feedItemId={params.itemId} renderFunc={renderNewsItemFunc} />
    );
  }

  return (
    <Grid className={styles.wrapper}>
      { newsItemFetcherNode }
      <HoloColoredTitle
        title="SHARED.OTHER_NEWS_TITLE"
        className={styles.otherNewsTitle}
        enableGrid={false}
      >
        <InlineNewsList
          nbNews={8}
          renderFunc={renderNewsInlineItemFunc}
        />
        <FullLineMoreNewsButton
          className={styles.moreBtn}
          nbNews={8}
          pictureUrl={MoreBtnPicture}
          loadingColor={ORANGE}
        />
      </HoloColoredTitle>
    </Grid>
  );
};

NewsFeedItem.propTypes = {
  match: PropTypes.objectOf(PropTypes.any),
};

const NewsFeedItemWithRouter = withRouter(NewsFeedItem);

export default NewsFeedItemWithRouter;
