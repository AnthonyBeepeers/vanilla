import React, { Component } from 'react/lib/React';

import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';

import NewsFeedItemContent from '../NewsFeedItemContent/NewsFeedItemContent';

import styles from './NewsFeedItem.css';

class NewsFeedItem extends Component {
  componentDidUpdate() {
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <div className={styles.wrapper} >
        <FullRow className={styles.content}>
          <NewsFeedItemContent />
        </FullRow>
      </div>
    );
  }
}

export default NewsFeedItem;
