import React from 'react/lib/React';

// import { FormattedMessage } from 'react-intl';

// import { Tabs, Tab } from 'material-ui/Tabs';

import Grid from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Grid/Grid';
// import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
// import Row from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Row/Row';
// import Col from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Col/Col';
import ActorsListFetcher from 'reactjs-beepeers-framework/components/Actors/Fetchers/ActorsListFetcher/ActorsListFetcher';
// import LeafletMap from 'reactjs-beepeers-framework/components/General/Web/LeafletMap/LeafletMap';
// import IconLabel from 'reactjs-beepeers-framework/components/General/Web/IconLabel/IconLabel';
import ResponsiveMediaSlider from 'reactjs-beepeers-framework/components/General/Web/ResponsiveMediaSlider/ResponsiveMediaSlider';
import ResponsiveContainer from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/ResponsiveContainer/ResponsiveContainer';
import ContainPicture from 'reactjs-beepeers-framework/components/General/Web/ContainPicture/ContainPicture';

// import { HTML_CONTENT } from 'reactjs-beepeers-framework/constants/iconLabelTypes';

import { INFOS_LIST_KEY } from '../../../constants/actors';

// import { WHITE } from '../../../constants/colors';

import styles from './InfosImage.css';

const InfosImage = () => {
  const renderFunc = (infos) => {
    console.log(infos);
    const page = infos[0];
    let mediasSliderNode = null;
    if (typeof (page.medias) !== 'undefined' && page.medias.length > 0) {
      mediasSliderNode = (
        <ResponsiveMediaSlider
          className={styles.contentNode}
          sliderClassName={styles.slider}
          modalClassName={styles.modalSlider}
          medias={page.medias}
          nbToShow={2}
          sliderId={`INFOS_DETAILS_MEDIAS_SLIDER_${page.profileId}`}
          xs={{ height: '25vw' }}
          sm={{ height: '203px' }}
          md={{ height: '268px' }}
          lg={{ height: '328px' }}
        />
        );
    } else if (typeof (page.coverUrl) !== 'undefined' && page.coverUrl !== '') {
      mediasSliderNode = (
        <ResponsiveContainer
          className={styles.coverWrapper}
          xs={{ height: '25vw' }}
          sm={{ height: '203px' }}
          md={{ height: '268px' }}
          lg={{ height: '328px' }}
        >
          <ContainPicture pictureUrl={page.coverUrl} />
        </ResponsiveContainer>
        );
    }

    return (
      <div>
        { mediasSliderNode }
      </div>
    );
  };

  return (
    <Grid className={styles.wrapper}>
      <ActorsListFetcher
        actorsListKey={INFOS_LIST_KEY}
        flattenSections
        renderFunc={renderFunc}
      />
    </Grid>
  );
};


export default InfosImage;
