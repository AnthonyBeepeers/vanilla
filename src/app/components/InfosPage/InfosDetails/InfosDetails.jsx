import React, { PropTypes } from 'react/lib/React';
import { connect } from 'react-redux';

import { FormattedMessage } from 'react-intl';

import { Tabs, Tab } from 'material-ui/Tabs';

import Grid from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Grid/Grid';
import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
import Row from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Row/Row';
import Col from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Col/Col';
import ActorsListFetcher from 'reactjs-beepeers-framework/components/Actors/Fetchers/ActorsListFetcher/ActorsListFetcher';
import LeafletMap from 'reactjs-beepeers-framework/components/General/Web/LeafletMap/LeafletMap';
import IconLabel from 'reactjs-beepeers-framework/components/General/Web/IconLabel/IconLabel';

import { HTML_CONTENT } from 'reactjs-beepeers-framework/constants/iconLabelTypes';

import { INFOS_LIST_KEY } from '../../../constants/actors';

import { WHITE } from '../../../constants/colors';

import styles from './InfosDetails.css';

const InfosDetails = ({ locales }) => {
  const { lang } = locales;
  const langWithoutRegionCode = lang.toLowerCase().split(/[_-]+/)[0];

  const renderFunc = (infos) => {
    const infosNodes = infos.map((page) => {
      const {
        location = null,
        phone = null,
        // email = null,
        description = null,
        description2 = null,
        website = null,
        fax = null,
      } = page;

      let addressNode = null;
      let mapNode = null;
      if (location !== null) {
        const {
          address = null,
          postalCode = null,
          city = null,
          latitude = null,
          longitude = null,
        } = location;

        if (address !== null && city !== null) {
          addressNode = (
            <Col xs={8} sm={4} className={styles.detailsBlockWrapper}>
              <div>
                <i className={`fa fa-map-marker ${styles.icon}`} aria-hidden="true" />
                <FormattedMessage
                  id="ACTOR.ADDRESS"
                  defaultMessage="ACTOR.ADDRESS"
                />
              </div>
              <div>
                { address } - {postalCode} { city }
              </div>
            </Col>
          );
        }

        if (latitude !== null && longitude !== null) {
          mapNode = (
            <FullRow>
              <LeafletMap
                latitude={latitude}
                longitude={longitude}
                markerClassName={styles.marker}
                className={styles.mapBlock}
                zoom={17}
              />
            </FullRow>
          );
        }
      }

      let phoneNode = null;
      if (phone !== null && phone.trim() !== '') {
        phoneNode = (
          <Col xs={8} sm={4} className={styles.detailsBlockWrapper}>
            <div>
              <i className={`fa fa-phone ${styles.icon}`} aria-hidden="true" />
              <FormattedMessage
                id="ACTOR.PHONE"
                defaultMessage="ACTOR.PHONE"
              />
            </div>
            <div>
              <a href={`tel:${page.phone}`} target="_blank" rel="noopener noreferrer">{ page.phone }</a>
            </div>
          </Col>
        );
      }

      let faxNode = null;
      if (fax !== null && fax.trim() !== '') {
        faxNode = (
          <Col xs={8} sm={4} className={styles.detailsBlockWrapper}>
            <div>
              <i className={`fa fa-fax ${styles.icon}`} aria-hidden="true" />
              <FormattedMessage
                id="ACTOR.FAX"
                defaultMessage="ACTOR.FAX"
              />
            </div>
            <div>
              <a href={`tel:${page.fax}`} target="_blank" rel="noopener noreferrer">{ page.fax }</a>
            </div>
          </Col>
        );
      }
/*
      let emailNode = null;
      if (email !== null && email.trim() !== '') {
        emailNode = (
          <Col xs={8} sm={4} className={styles.detailsBlockWrapper}>
            <div>
              <i className={`fa fa-envelope ${styles.icon}`} aria-hidden="true" />
              <FormattedMessage
                id="ACTOR.EMAIL"
                defaultMessage="ACTOR.EMAIL"
              />
            </div>
            <div>
              <a href={`mailto:${page.email}`} target="_blank" rel="noopener noreferrer">{ page.email }</a>
            </div>
          </Col>
        );
      } */

      let websiteNode = null;
      if (website !== null && website.trim() !== '') {
        websiteNode = (
          <Col xs={8} sm={4} className={styles.detailsBlockWrapper}>
            <div>
              <i className={`fa fa-globe ${styles.icon}`} aria-hidden="true" />
              <FormattedMessage
                id="ACTOR.WEBSITE"
                defaultMessage="ACTOR.WEBSITE"
              />
            </div>
            <div>
              <a href={page.website} target="_blank" rel="noopener noreferrer">{ page.website }</a>
            </div>
          </Col>
        );
      }

      let summaryNode = null;
      if (description !== null && description.trim() !== '') {
        summaryNode = (
          <FullRow className={styles.summary}>
            <IconLabel
              type={HTML_CONTENT}
              label={description}
            />
          </FullRow>
        );
      }

      let contentNode = null;
      contentNode = null;
      if (description2 !== null && description2.trim() !== '' && langWithoutRegionCode === 'en') {
        summaryNode = (
          <FullRow className={styles.content}>
            <IconLabel
              type={HTML_CONTENT}
              label={description2}
            />
          </FullRow>
        );
      }

      let detailsNodes = null;
      if (
        addressNode !== null ||
        phoneNode !== null ||
        faxNode !== null ||
        // emailNode !== null ||
        websiteNode !== null
      ) {
        detailsNodes = (
          <Row center="xs">
            { addressNode }
            { phoneNode }
            { faxNode }
            { /* emailNode */ }
            { websiteNode }
          </Row>
        );
      }

     /* eslint-disable no-else-return */
      if (langWithoutRegionCode === 'en') {
        return (
          <Tab
            key={page.profileId}
            style={{ color: WHITE }}
            label={page.email.split('@')[0]}
            className={styles.tabBtn}
          >
            { summaryNode }
            { contentNode }
            { detailsNodes }
            { mapNode }
          </Tab>
        ); } else {
        return (
          <Tab
            key={page.profileId}
            style={{ color: WHITE }}
            label={page.displayName}
            className={styles.tabBtn}
          >
            { summaryNode }
            { contentNode }
            { detailsNodes }
            { mapNode }
          </Tab>
        ); }
    });

    return (
      <Tabs
        inkBarStyle={{ backgroundColor: 'white', height: '6px', marginTop: '-6px' }}
        tabItemContainerStyle={{ backgroundColor: 'red' }}
      >
        { infosNodes }
      </Tabs>
    );
  };

  return (
    <Grid className={styles.wrapper}>
      <ActorsListFetcher
        actorsListKey={INFOS_LIST_KEY}
        flattenSections
        renderFunc={renderFunc}
      />
    </Grid>
  );
};

InfosDetails.propTypes = {
  locales: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ])).isRequired,
};

const mapStateToProps = state => ({
  locales: state.locales,
});

const InfosDetailsConnected = connect(
  mapStateToProps
)(InfosDetails);

export default InfosDetailsConnected;
