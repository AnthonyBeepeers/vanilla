import React from 'react/lib/React';

import ActorCarouselByKey from 'reactjs-beepeers-framework/components/Actors/Renderers/Web/ActorCarouselByKey/ActorCarouselByKey';

import { INFOS_HEADER_KEY } from '../../../constants/actors';

const NewsHeader = () => (
  <ActorCarouselByKey actorKey={INFOS_HEADER_KEY} />
);

export default NewsHeader;
