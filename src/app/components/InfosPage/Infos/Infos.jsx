import React from 'react/lib/React';

import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';

import InfosDetails from '../InfosDetails/InfosDetails';
import HomeHeader from '../../HomePage/HomeCarousel/HomeCarousel';

import styles from './Infos.css';

const Infos = () => (
  <FullRow className={styles.wrapper}>
    <HomeHeader />
    <InfosDetails />
  </FullRow>
);

export default Infos;
