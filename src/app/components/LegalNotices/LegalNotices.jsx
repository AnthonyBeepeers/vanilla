import React from 'react/lib/React';
import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
import Grid from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Grid/Grid';
import ReactHtmlParser from 'react-html-parser';
import ActorFetcherByKey from 'reactjs-beepeers-framework/components/Actors/Fetchers/ActorFetcherByKey/ActorFetcherByKey';
import FullLineTitle from 'reactjs-beepeers-framework/components/General/Web/FullLineTitle/FullLineTitle';
import styles from './LegalNotices.css';

/* eslint-disable new-cap*/
import { LEGAL_NOTICES } from '../../constants/actors';

const LegalNotices = () => {
  const renderFunc = actor =>
     (
       <div style={{ margin: '2%' }} >
         <FullLineTitle title={actor.displayName} className={styles.sectionTitle} />
         { ReactHtmlParser(actor.description) }
       </div>
    )
  ;

  return (
    <Grid>
      <FullRow className={styles.wrapper}>
        <ActorFetcherByKey
          actorKey={LEGAL_NOTICES}
          renderFunc={renderFunc}
        />
      </FullRow>
    </Grid>
  );
};

export default LegalNotices;
