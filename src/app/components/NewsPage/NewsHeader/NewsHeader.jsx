import React from 'react/lib/React';

import ActorCarouselByKey from 'reactjs-beepeers-framework/components/Actors/Renderers/Web/ActorCarouselByKey/ActorCarouselByKey';

import { NEWS_HEADER_KEY } from '../../../constants/actors';

const NewsHeader = () => (
  <ActorCarouselByKey actorKey={NEWS_HEADER_KEY} />
);

export default NewsHeader;
