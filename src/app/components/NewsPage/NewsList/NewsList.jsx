import React, { PropTypes } from 'react/lib/React';

import { withRouter } from 'react-router-dom';

import Grid from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Grid/Grid';
import LinkedNewsItem from 'reactjs-beepeers-framework/components/Feed/Renderers/Web/LinkedNewsItem/LinkedNewsItem';
import NewsItemPortraitTopPicture from 'reactjs-beepeers-framework/components/Feed/Renderers/Web/NewsItemPortraitTopPicture/NewsItemPortraitTopPicture';
import FullLineDecoratedTitle from 'reactjs-beepeers-framework/components/General/Web/FullLineDecoratedTitle/FullLineDecoratedTitle';
import FullLineMoreNewsButton from 'reactjs-beepeers-framework/components/Feed/Renderers/Web/FullLineMoreNewsButton/FullLineMoreNewsButton';

import InlineNewsList from '../../Shared/InlineNewsList/InlineNewsList';

import NewsDefaultPicture from '../../../assets/images/news_default.png';
import seeMorePictureUrl from '../../../assets/images/see_more.png';
import MoreBtnPicture from '../../../assets/images/more_btn.png';

import { ORANGE } from '../../../constants/colors';

import styles from './NewsList.css';

const NewsList = () => {
  const renderNewsInlineItemFunc = item => (
    <div className={styles.newsItemWrapper}>
      <div className={styles.newsItemContent}>
        <LinkedNewsItem baseUrl="/feed/" item={item}>
          <div className={styles.newsInlineItemDescription}>
            <NewsItemPortraitTopPicture
              item={item}
              className={styles.itemPortrait}
              defaultPictureUrl={NewsDefaultPicture}
              disableCoverLink
            />
          </div>
          <div className={styles.newsItemSeeMoreWrapper}>
            <FullLineDecoratedTitle
              className={styles.newsItemSeeMore}
              title="SHARED.NEWS.SEE_MORE"
              decoratorUrl={seeMorePictureUrl}
            />
          </div>
        </LinkedNewsItem>
      </div>
    </div>
  );

  return (
    <Grid className={styles.wrapper}>
      <InlineNewsList
        nbNews={8}
        renderFunc={renderNewsInlineItemFunc}
      />
      <FullLineMoreNewsButton
        className={styles.moreBtn}
        nbNews={8}
        pictureUrl={MoreBtnPicture}
        loadingColor={ORANGE}
      />
    </Grid>
  );
};

NewsList.propTypes = {
  location: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ])).isRequired,
};

const NewsListWithRouter = withRouter(NewsList);

export default NewsListWithRouter;
