import React from 'react/lib/React';

import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';

import NewsHeader from '../NewsHeader/NewsHeader';
import NewsList from '../NewsList/NewsList';

import styles from './News.css';

const Home = () => (
  <div className={styles.wrapper} >
    <FullRow className={styles.content}>
      <NewsHeader />
      <NewsList />
    </FullRow>
  </div>
);

export default Home;
