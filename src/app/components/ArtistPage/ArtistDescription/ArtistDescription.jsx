import React, { PropTypes } from 'react/lib/React';

import moment from 'moment';

import { FormattedMessage, injectIntl } from 'react-intl';

import BeeMetadata from 'reactjs-beepeers-framework/components/General/Web/BeeMetadata/BeeMetadata';
import FullRow from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/FullRow/FullRow';
import Row from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Row/Row';
import Col from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Col/Col';
import CenterContainer from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/CenterContainer/CenterContainer';
import IconLabel from 'reactjs-beepeers-framework/components/General/Web/IconLabel/IconLabel';
import IconBlock from 'reactjs-beepeers-framework/components/General/Web/IconBlock/IconBlock';
import CoverPicture from 'reactjs-beepeers-framework/components/General/Web/CoverPicture/CoverPicture';
import ContainPicture from 'reactjs-beepeers-framework/components/General/Web/ContainPicture/ContainPicture';
import ResponsiveMediaSlider from 'reactjs-beepeers-framework/components/General/Web/ResponsiveMediaSlider/ResponsiveMediaSlider';
import ResponsiveMediaGallery from 'reactjs-beepeers-framework/components/General/Web/ResponsiveMediaGallery/ResponsiveMediaGallery';
import VideoPlayer from 'reactjs-beepeers-framework/components/General/Web/VideoPlayer/VideoPlayer';
import ActorFetcherById from 'reactjs-beepeers-framework/components/Actors/Fetchers/ActorFetcherById/ActorFetcherById';
import GoBackButton from 'reactjs-beepeers-framework/components/General/Web/GoBackButton/GoBackButton';
import SubscriptionToggleButton from 'reactjs-beepeers-framework/components/Actors/Renderers/Web/SubscriptionToggleButton/SubscriptionToggleButton';

import {
HTML_CONTENT,
PHONE, FAX, EMAIL, WEBSITE,
FACEBOOK, TWITTER, INSTAGRAM, YOUTUBE,
SOUNDCLOUD, SNAPCHAT,
LINKEDIN, OTHER,
} from 'reactjs-beepeers-framework/constants/iconLabelTypes';

import HoloColoredTitle from '../../General/HoloColoredTitle/HoloColoredTitle';

import DefaultVideoPlayerThumbnail from '../../../assets/images/video_player_default.png';

import styles from './ArtistDescription.css';

const ArtistDescription = (
    {
      actor,
      defaultPeoplePictureUrl,
      galleryRenderForMedias = false,
      className = '',
      modalClassNameForMedias = '',
      enableTitleOnDetailsLinks = false,
      intl,
      enableVideoPlayer = false,
      videoPlayerThumbnail = DefaultVideoPlayerThumbnail,
      videoPlayerEnablePlayPicture = false,
    }
  ) => {
  const {
    profileId,
    originalUrl,
    thumbnailUrl,
    coverUrl,
    displayName,
    description = null,
    description2 = null,
    startDate = null,
    endDate = null,
    locationProfileId = null,
    parentId = null,
    parentName = null,
    email = null,
    website = null,
    blog = null,
    phone = null,
    fax = null,
    facebookUrl = null,
    twitterUrl = null,
    instagramUrl = null,
    youtubeUrl = null,
    functionList = null,
  } = actor;
  const { formatMessage } = intl;
  let coverPictureNode = null;
  if (typeof (coverUrl) !== 'undefined' && coverUrl !== '') {
    coverPictureNode = (
      <div className={styles.coverWrapper}>
        <div className={styles.coverContent}>
          <CoverPicture pictureUrl={`${coverUrl}&width=1920&height=750`} />
        </div>
      </div>
    );
  }

  const renderLocation = (loc) => {
    if (typeof (loc.profileId) !== 'undefined') {
      return (
        <span>
          { loc.displayName }
        </span>
      );
    }
    return null;
  };

  const renderScheduledTime = () => {
    let finalDateString = null;
    if (startDate !== null) {
      const startDateMoment = moment(startDate).utcOffset(0);
      finalDateString = `${startDateMoment.format('ll')} ${startDateMoment.format('LT')}`;
    }
    if (endDate !== null) {
      finalDateString = `${finalDateString} - ${moment(endDate).utcOffset(0).format('LT')}`;
    }
    if (finalDateString !== null) {
      return (
        <span>
          { finalDateString }
        </span>
      );
    }
    return null;
  };

  const nameNode = (
    <div className={styles.profileName}>
      {displayName}
    </div>
  );

  const datesNode = (
    <div className={styles.dates}>
      <i className="fa fa-clock-o" /> { renderScheduledTime() }
    </div>
  );

  let locationNode = null;
  if (locationProfileId !== null) {
    locationNode = (
      <div className={styles.location}>
        <i className="fa fa-map-marker" /> <ActorFetcherById actorId={locationProfileId} renderFunc={renderLocation} />
      </div>
    );
  }
  let subNameNode = null;
  if (parentName !== null) {
    subNameNode = (
      <div className={styles.parentName}>
        {parentName}
      </div>
    );
  }

  let profilePictureNode = (
    <CoverPicture pictureUrl={defaultPeoplePictureUrl} className={styles.profilePicture} />
  );
  if ((typeof (originalUrl) !== 'undefined' && originalUrl !== '')) {
    profilePictureNode = (
      <ContainPicture pictureUrl={`${originalUrl}&size=200&type=png`} className={styles.profilePicture} />
    );
  } else if ((typeof (thumbnailUrl) !== 'undefined' && thumbnailUrl !== '')) {
    profilePictureNode = (
      <ContainPicture pictureUrl={thumbnailUrl} className={styles.profilePicture} />
    );
  }

  const subscriptionToggleButton = (
    <SubscriptionToggleButton
      actorId={profileId}
      className={styles.subscriptionBtn}
    />
  );

  const header = (
    <div className={styles.profileWrapper}>
      <div className={styles.profilePictureWrapper}>
        { profilePictureNode }
      </div>
      <div className={styles.profileNameWrapper}>
        { nameNode }
        { subNameNode }
        { datesNode }
        { locationNode }
      </div>
      { subscriptionToggleButton }
    </div>
  );

  let abstractNode = null;
  if (description !== null && description.trim() !== '') {
    abstractNode = (
      <IconLabel
        className={styles.abstract}
        type={HTML_CONTENT}
        label={description}
      />
    );
  }

  let descriptionNode = null;
  if (description2 !== null && description2.trim() !== '') {
    descriptionNode = (
      <IconLabel
        type={HTML_CONTENT}
        label={description2}
      />
    );
  }

  let phoneNode = null;
  if (phone !== null && phone.trim() !== '') {
    let otherParams = {};
    if (enableTitleOnDetailsLinks) {
      otherParams = {
        title: formatMessage({ id: 'STATIC_CONTENT_VERTICAL.PHONE' }),
      };
    }
    phoneNode = (
      <IconLabel
        type={PHONE}
        label={phone}
        {...otherParams}
      />
    );
  }

  let faxNode = null;
  if (fax !== null && fax.trim() !== '') {
    let otherParams = {};
    if (enableTitleOnDetailsLinks) {
      otherParams = {
        title: formatMessage({ id: 'STATIC_CONTENT_VERTICAL.FAX' }),
      };
    }
    faxNode = (
      <IconLabel
        type={FAX}
        label={fax}
        {...otherParams}
      />
    );
  }

  let emailNode = null;
  if (email !== null && email.trim() !== '') {
    let otherParams = {};
    if (enableTitleOnDetailsLinks) {
      otherParams = {
        title: formatMessage({ id: 'STATIC_CONTENT_VERTICAL.EMAIL' }),
      };
    }
    emailNode = (
      <IconLabel
        type={EMAIL}
        label={email}
        {...otherParams}
      />
    );
  }

  let websiteNode = null;
  if (website !== null && website.trim() !== '') {
    let otherParams = {};
    if (enableTitleOnDetailsLinks) {
      otherParams = {
        title: formatMessage({ id: 'STATIC_CONTENT_VERTICAL.WEBSITE' }),
      };
    }
    websiteNode = (
      <IconLabel
        type={WEBSITE}
        label={website}
        {...otherParams}
      />
    );
  }

  let blogNode = null;
  if (blog !== null && blog.trim() !== '') {
    let otherParams = {};
    if (enableTitleOnDetailsLinks) {
      otherParams = {
        title: formatMessage({ id: 'STATIC_CONTENT_VERTICAL.BLOG' }),
      };
    }
    blogNode = (
      <IconLabel
        type={WEBSITE}
        label={blog}
        {...otherParams}
      />
    );
  }

  let facebookNode = null;
  if (facebookUrl !== null && facebookUrl.trim() !== '') {
    facebookNode = (
      <Col xs={2} sm={1} className={styles.iconBlock}>
        <IconBlock
          type={FACEBOOK}
          label={facebookUrl}
        />
      </Col>
    );
  }

  let twitterNode = null;
  if (twitterUrl !== null && twitterUrl.trim() !== '') {
    twitterNode = (
      <Col xs={2} sm={1} className={styles.iconBlock}>
        <IconBlock
          type={TWITTER}
          label={twitterUrl}
        />
      </Col>
    );
  }

  let instagramNode = null;
  if (instagramUrl !== null && instagramUrl.trim() !== '') {
    instagramNode = (
      <Col xs={2} sm={1} className={styles.iconBlock}>
        <IconBlock
          type={INSTAGRAM}
          label={instagramUrl}
        />
      </Col>
    );
  }

  let youtubeNode = null;
  if (!enableVideoPlayer && youtubeUrl !== null && youtubeUrl.trim() !== '') {
    youtubeNode = (
      <Col xs={2} sm={1} className={styles.iconBlock}>
        <IconBlock
          type={YOUTUBE}
          label={youtubeUrl}
        />
      </Col>
    );
  }

  let youtubeVideoPlayerNode = null;
  let youtubeVideoFunction = false;
  let ytChannelNode = null;
  const knownFunctions = [];
  const otherFunctions = [];
  if (functionList !== null && functionList.length > 0) {
    functionList.forEach((functionItem) => {
      switch (functionItem.identifier) {
        case 'FB_PAGE':
        case 'TWITTER':
        case 'INSTAGRAM':
          break;
        case 'YOUTUBE_VIDEO': {
          youtubeVideoFunction = true;
          if (enableVideoPlayer) {
            let youtubeVideoUrl = null;
            if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
              functionItem.parameterList.forEach((parameterItem) => {
                if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                  youtubeVideoUrl = parameterItem.value;
                }
              });
            }
            if (youtubeVideoUrl !== null) {
              youtubeVideoPlayerNode = (
                <Row center="xs">
                  <Col xs={10} sm={8}>
                    <VideoPlayer
                      videoThumbnailUrl={videoPlayerThumbnail}
                      disablePlayPictureUrl={!videoPlayerEnablePlayPicture}
                      videoUrl={youtubeVideoUrl}
                    />
                  </Col>
                </Row>
              );
            }
          }
          break;
        }
        case 'SOUNDCLOUD':
        case 'SOUNDCLOUD_PLAYLIST': {
          let soundcloudId = null;
          if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
            functionItem.parameterList.forEach((parameterItem) => {
              if (parameterItem.name === 'app_url' && parameterItem.value.trim() !== '') {
                soundcloudId = parameterItem.value.replace('soundcloud://playlists/', '');
              }
            });
          }
          if (soundcloudId !== null) {
            const soundcloudUrl = `https://soundcloud.com/${soundcloudId}`;
            const soundcloudNode = (
              <Col key="SOUNDCLOUD" xs={2} sm={1} className={styles.iconBlock}>
                <IconBlock
                  type={SOUNDCLOUD}
                  label={soundcloudUrl}
                />
              </Col>
            );
            knownFunctions.push(soundcloudNode);
          }
          break;
        }
        case 'LINKEDIN_PROFILE': {
          let linkedInUrl = null;
          if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
            functionItem.parameterList.forEach((parameterItem) => {
              if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                linkedInUrl = parameterItem.value;
              }
            });
          }
          if (linkedInUrl !== null) {
            const linkedInNode = (
              <Col key="LINKEDIN_PROFILE" xs={2} sm={1} className={styles.iconBlock}>
                <IconBlock
                  type={LINKEDIN}
                  label={linkedInUrl}
                />
              </Col>
            );
            knownFunctions.push(linkedInNode);
          }
          break;
        }
        case 'LINKEDIN_PAGE': {
          let linkedInUrl = null;
          if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
            functionItem.parameterList.forEach((parameterItem) => {
              if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                linkedInUrl = parameterItem.value;
              }
            });
          }
          if (linkedInUrl !== null) {
            const linkedInNode = (
              <Col key="LINKEDIN_PAGE" xs={2} sm={1} className={styles.iconBlock}>
                <IconBlock
                  type={LINKEDIN}
                  label={linkedInUrl}
                />
              </Col>
            );
            knownFunctions.push(linkedInNode);
          }
          break;
        }
        case 'YOUTUBE_CHANNEL': {
          let ytChannelUrl = null;
          if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
            functionItem.parameterList.forEach((parameterItem) => {
              if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                ytChannelUrl = parameterItem.value;
              }
            });
          }
          if (ytChannelUrl !== null) {
            ytChannelNode = (
              <Col key="YOUTUBE_CHANNEL" xs={2} sm={1} className={styles.iconBlock}>
                <IconBlock
                  type={YOUTUBE}
                  label={ytChannelUrl}
                />
              </Col>
            );
            knownFunctions.push(ytChannelNode);
          }
          break;
        }
        case 'WEB': {
          let webUrl = null;
          if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
            functionItem.parameterList.forEach((parameterItem) => {
              if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                webUrl = parameterItem.value;
              }
            });
          }
          if (webUrl !== null) {
            const webNode = (
              <Col key="WEB" xs={2} sm={1} className={styles.iconBlock}>
                <IconBlock
                  type={WEBSITE}
                  label={webUrl}
                />
              </Col>
            );
            knownFunctions.push(webNode);
          }
          break;
        }
        case 'SNAPCHAT': {
          let snapchatUrl = null;
          if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
            functionItem.parameterList.forEach((parameterItem) => {
              if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                snapchatUrl = parameterItem.value;
              }
            });
          }
          if (snapchatUrl !== null) {
            const snapchatNode = (
              <Col key="SNAPCHAT" xs={2} sm={1} className={styles.iconBlock}>
                <IconBlock
                  type={SNAPCHAT}
                  label={snapchatUrl}
                />
              </Col>
            );
            knownFunctions.push(snapchatNode);
          }
          break;
        }
        default: {
          if (typeof (functionItem.pictureUrl) !== 'undefined' && functionItem.pictureUrl.trim() !== '') {
            let otherUrl = null;
            if (typeof (functionItem.parameterList) !== 'undefined' && functionItem.parameterList.length > 0) {
              functionItem.parameterList.forEach((parameterItem) => {
                if (parameterItem.name === 'url' && parameterItem.value.trim() !== '') {
                  otherUrl = parameterItem.value;
                }
              });
            }
            if (otherUrl !== null) {
              const otherNode = (
                <Col key={functionItem.identifier} xs={2} sm={1} className={styles.iconBlock}>
                  <IconBlock
                    type={OTHER}
                    label={otherUrl}
                    typePicture={functionItem.pictureUrl}
                  />
                </Col>
              );
              otherFunctions.push(otherNode);
            }
          }
        }
      }
    });
  }
  if (youtubeNode !== null && ytChannelNode !== null && !youtubeVideoFunction) {
    youtubeNode = null;
  }

  let mediasNode = null;
  if (typeof (actor.medias) !== 'undefined') {
    mediasNode = (
      <ResponsiveMediaSlider
        className={styles.sliderWrapper}
        sliderClassName={styles.slider}
        modalClassName={`${styles.modalMedia} ${modalClassNameForMedias}`}
        medias={actor.medias}
        nbToShow={4}
        sliderId={`peopleMediaSlider_${actor.profileId}`}
        xs={{ height: '25vw' }}
        sm={{ height: '89px' }}
        md={{ height: '124px' }}
        lg={{ height: '157px' }}
      />
    );
    if (galleryRenderForMedias) {
      mediasNode = (
        <ResponsiveMediaGallery
          className={styles.mediaGallery}
          modalClassName={`${styles.modalMedia} ${modalClassNameForMedias}`}
          galleryId={`peopleMediaGallery_${actor.profileId}`}
          medias={actor.medias}
          nbPerRows={4}
          xs={{ height: '25vw' }}
          sm={{ height: '89px' }}
          md={{ height: '124px' }}
          lg={{ height: '157px' }}
        />
      );
    }
  }

  let peopleImgUrl = null;
  if ((typeof (coverUrl) !== 'undefined' && coverUrl !== '')) {
    peopleImgUrl = coverUrl;
  } else if ((typeof (thumbnailUrl) !== 'undefined' && thumbnailUrl !== '')) {
    peopleImgUrl = thumbnailUrl;
  }

  const detailsLinks = (
    <div className={styles.details}>
      { emailNode }
      { websiteNode }
      { blogNode }
      { phoneNode }
      { faxNode }
    </div>
  );

  const detailsBlocks = (
    <FullRow>
      <Row center="xs">
        { facebookNode }
        { twitterNode }
        { instagramNode }
        { youtubeNode }
        { knownFunctions }
      </Row>
      <Row center="xs">
        { otherFunctions }
      </Row>
    </FullRow>
  );

  const detailsNode = (
    <FullRow>
      { detailsLinks }
      { youtubeVideoPlayerNode }
      { detailsBlocks }
    </FullRow>
  );

  let partnerNode = null;
  if (parentId !== null) {
    const renderPartner = (part, isFetching) => {
      if (!isFetching) {
        return (
          <HoloColoredTitle
            title="SCHEDULING.ARTIST.PARTNER.TITLE"
            enableGrid={false}
          />
        );
      }
      return null;
    };

    partnerNode = (
      <ActorFetcherById actorId={parentId} renderFunc={renderPartner} />
    );
  }

  /* TODO: To Keep : Remove this lines to add partner on artist page. */
  partnerNode = null;

  return (
    <Row className={`${styles.wrapper} ${className}`}>
      <div className={styles.backLink}>
        <GoBackButton>
          <i className="fa fa-arrow-circle-left" /> <FormattedMessage id={'SHARED.NEWS.BACK'} />
        </GoBackButton>
      </div>
      <BeeMetadata title={displayName} path={`/profile/${actor.profileId}`} imgUrl={peopleImgUrl} />
      { coverPictureNode }
      { header }
      <div className={styles.infos}>
        { abstractNode }
        { descriptionNode }
      </div>
      { partnerNode }
      <CenterContainer className={styles.detailsContainer} width={10}>
        { detailsNode }
        { mediasNode }
      </CenterContainer>
    </Row>
  );
};

ArtistDescription.propTypes = {
  actor: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.bool,
    PropTypes.number,
    PropTypes.object,
    PropTypes.string,
  ])).isRequired,
  defaultPeoplePictureUrl: PropTypes.string.isRequired,
  className: PropTypes.string,
  galleryRenderForMedias: PropTypes.bool,
  modalClassNameForMedias: PropTypes.string,
  enableTitleOnDetailsLinks: PropTypes.bool,
  intl: PropTypes.objectOf(PropTypes.any).isRequired,
  enableVideoPlayer: PropTypes.bool,
  videoPlayerThumbnail: PropTypes.string,
  videoPlayerEnablePlayPicture: PropTypes.bool,
};

const PeopleDescriptionIntl = injectIntl(ArtistDescription);

export default PeopleDescriptionIntl;
