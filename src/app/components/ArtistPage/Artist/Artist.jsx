import React, { PropTypes } from 'react/lib/React';

import Grid from 'reactjs-beepeers-framework/components/Layout/Web/FlexboxGrid/Grid/Grid';
import ActorFetcherById from 'reactjs-beepeers-framework/components/Actors/Fetchers/ActorFetcherById/ActorFetcherById';

import ArtistDescription from '../ArtistDescription/ArtistDescription';

import ArtistDefaultPicture from '../../../assets/images/artist_default.png';
import VideoPlayerDefault from '../../../assets/images/video_player_default.png';

import styles from './Artist.css';

const Artist = ({ match }) => {
  const { params = {} } = match;
  const renderFunc = (actor) => {
    if (typeof (actor.profileId) !== 'undefined') {
      return (
        <ArtistDescription
          actor={actor}
          defaultPeoplePictureUrl={ArtistDefaultPicture}
          className={styles.artistDesc}
          modalClassNameForMedias={styles.artistModal}
          enableVideoPlayer
          videoPlayerThumbnail={VideoPlayerDefault}
          videoPlayerEnablePlayPicture={false}
        />
      );
    }

    return null;
  };

  return (
    <div
      className={styles.wrapper}
    >
      <Grid className={styles.grid}>
        <ActorFetcherById
          actorId={params.artistId}
          renderFunc={renderFunc}
        />
      </Grid>
    </div>
  );
};

Artist.propTypes = {
  match: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default Artist;
