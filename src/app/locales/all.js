import en from './en';
import fr from './fr';

const all = {
  en,
  'en-US': en,
  'en-GB': en,
  fr,
  'fr-FR': fr,
};

export default all;
