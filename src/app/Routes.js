
import Home from './components/HomePage/Home/Home';
import EventConv from './components/General/EventConvPage/EventConv/EventConv';
import Infos from './components/InfosPage/Infos/Infos';
import FAQ from './components/FAQPage/FAQ/FAQ';
// import NewsFeedItem from './components/Shared/Feed/NewsFeedItem/NewsFeedItem';
// import Tickets from './components/TicketsPage/Tickets/Tickets';
// import PartnersPage from './components/PartnersPage/PartnersPage/ParntersPage';
// import Cashless from './components/CashlessPage/Home/Home';
import AccountManageForgotPwd from './components/Account/AccountManageForgotPwd/AccountManageForgotPwd';
import LegalNotices from './components/LegalNotices/LegalNotices';
// import Tmp from './components/General/Tmp/Tmp';
// import SchedulingByDate from '../../../vanilla/src/app/components/SchedulingByDatePage/SchedulingByDate/SchedulingByDate';
// import SchedulingArtistsList from '../../../vanilla/src/app/components/SchedulingByDatePage/SchedulingArtistsList/SchedulingArtistsList';
import SchedulingByDate from './components/SchedulingPage/SchedulingByDate/SchedulingByDate';
import Hotels from './components/Hotels/Hotels';

const Routes = [
  {
    pattern: '/',
    component: Home,
  },
  {
    pattern: '/event/scheduling/bydate',
    name: 'MENU.SCHEDULE',
    component: SchedulingByDate,
    disableScrollOnTop: true,
    menuSelectedPatterns: [
      '/event/contributor',
      '/event/event',
      '/event/structure',
      '/event/scheduling/structures',
      '/event/scheduling/contributors',
      '/event/scheduling/bylocation',
    ],
  },
  {
    pattern: '/hotels/',
    name: 'MENU.LOG',
    component: Hotels,
  },
 /*
  {
    pattern: '/scheduling/bydate/:date',
    component: SchedulingByDate,
    disableScrollOnTop: true,
  }, */
  {
    pattern: '/infos',
    name: 'MENU.USEFUL.INFORMATION',
    component: Infos,
  },
  {
    moreMenu: true,
    routes: [
      {
        pattern: '/faq',
        component: FAQ,
        name: 'MENU.FAQ',
      },
      {
        pattern: '/legalnotices',
        name: 'MENU.LEGAL',
        component: LegalNotices,
      },
    ],
  },
  {
    pattern: '/event/:eventId/:eventName',
    component: EventConv,
    exactMatch: false,
  },
  {
    pattern: '/event/:eventId',
    component: EventConv,
  },
  {
    pattern: '/myaccount/forgotpwd',
    component: AccountManageForgotPwd,
    onlyLogIn: false,
  },
];

export default Routes;
