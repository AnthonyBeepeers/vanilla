import createStore from 'redux/lib/createStore';
import moment from 'moment';

import throttle from 'lodash/throttle';

import applyMiddleware from 'redux/lib/applyMiddleware';
import createLogger from 'redux-logger';
import thunk from 'redux-thunk';

import combineReducers from 'redux/lib/combineReducers';

import createBrowserHistory from 'history/createBrowserHistory';

import allBeepeersReducers from 'reactjs-beepeers-framework/reducers';

import {
  initLocales,
  mergeLocalesData,
  getMessages,
} from 'reactjs-beepeers-framework/manageLocales';

import { loadState, saveState } from 'reactjs-beepeers-framework/localStorage';

import allAppReducers from './app/reducers';

import Routes from './app/Routes';
import Config from './app/Config.json';
import localeAppData from './app/locales/all';

const configureStore = () => {
  const history = createBrowserHistory();

  const middlewares = [thunk];

  if (Config.debug) {
    middlewares.push(createLogger());
  }

  initLocales();
  mergeLocalesData(localeAppData);

  const allReducers = combineReducers({
    ...allBeepeersReducers,
    ...allAppReducers,
  });

  const initLocalesState = (lang = 'fr') => {
    moment.locale(lang);
    return {
      locales: {
        lang,
        messages: getMessages(lang),
      },
    };
  };

  let initialState = {
    history,
    routes: Routes,
    appConfig: Config,
    ...initLocalesState(),
  };

  const persistedState = loadState(Config.env, Config.applicationKey);

  initialState = {
    ...initialState,
    ...persistedState,
  };

  if (typeof (persistedState.locales) !== 'undefined' && typeof (persistedState.locales.lang) !== 'undefined') {
    initialState = {
      ...initialState,
      ...initLocalesState(persistedState.locales.lang),
    };
  }
 /* eslint-disable no-underscore-dangle */
  const store = createStore(
    allReducers,
    initialState,
    applyMiddleware(...middlewares),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  );
 /* eslint-enable */
  store.subscribe(throttle(() => {
    const { accountType, authKey, account } = store.getState().auth;
    const authLastFetchDate = store.getState().auth.lastFetchDate;
    const { description } = store.getState().context;
    const contextLastFetchDate = store.getState().context.lastFetchDate;
    const { lang } = store.getState().locales;

    saveState(
      Config.env,
      Config.applicationKey,
      {
        locales: {
          lang,
        },
        account: store.getState().account,
        auth: {
          accountType,
          lastFetchDate: authLastFetchDate,
          authKey,
          account,
        },
        feed: store.getState().feed,
        actors: store.getState().actors,
        context: {
          lastFetchDate: contextLastFetchDate,
          description,
        },
        shopify: store.getState().shopify,
        cookiePopup: store.getState().cookiePopup,
      }
    );
  }, 1000));

  return store;
};

export default configureStore;
