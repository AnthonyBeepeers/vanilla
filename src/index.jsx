import 'babel-polyfill';

import React from 'react/lib/React';
import { render } from 'react-dom';

/* eslint-disable no-unused-vars */
import * as fontAwesome from 'font-awesome/css/font-awesome.min.css';
import * as slick from 'slick-carousel/slick/slick.css';
import * as slickTheme from 'slick-carousel/slick/slick-theme.css';
/* eslint-enable no-unused-vars */

import App from './app/components/App';
import configureStore from './configureStore';

const store = configureStore();

const renderApp = () => {
  render(
    <App store={store} />,
    document.getElementById('app')
  );
};

if (!window.intl) {
  require.ensure([
    'intl',
    'intl/locale-data/jsonp/en.js',
    'intl/locale-data/jsonp/fr.js',
  ], (require) => {
    require('intl');
    require('intl/locale-data/jsonp/en.js');
    require('intl/locale-data/jsonp/fr.js');

    renderApp();
  });
} else {
  renderApp();
}
