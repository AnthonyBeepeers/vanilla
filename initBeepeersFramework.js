// const exec = require('child_process').exec;
const fs = require('fs');

const nodeModulesDir = './node_modules';

if (!fs.existsSync(nodeModulesDir)) {
  fs.mkdirSync(nodeModulesDir);
}
/*
const puts = function(error, stdout, stderr) {
    if(error) {
        console.error(error);
    }
};

exec('sudo npm link reactjs-beepeers-framework', puts);
*/

fs.symlinkSync('/usr/local/lib/node_modules/reactjs-beepeers-framework', './node_modules/reactjs-beepeers-framework');

fs.symlinkSync('./node_modules/reactjs-beepeers-framework', './reactjs-beepeers-framework');
